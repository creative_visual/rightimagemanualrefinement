#include "stdafx.h"
#include <opencv/cxcore.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
/////////////////////////////////////////////////////////////////////////////////////////////////////////    全背景下右图工具第一步的功能所需要的对应头文件
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/legacy/legacy.hpp>
#include <opencv2/legacy/compat.hpp>
#include <opencv2/flann/flann.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/nonfree/nonfree.hpp>
//////////////////////////////////////////////////////////////////////////////////////////////////////////




#define DEBUG_SIMILARITY_IMPROVE
#define C_THRESH 0.7 //threshold value,
//#define BK_MIN
//#define SHOW_MATCHES
//the smaller, the fewer initial matches (but the fewer ones are more reliable)
#define MIN2(x, y) (((x)<(y))?(x):(y))
#define MAX2(x, y) (((x)>(y))?(x):(y))
#define CLIP(x,a,b) (MIN2(MAX2(x,a),b))

using namespace std;
int bk_label;

static CvScalar colors[] =
{
	{{0,0,255}},
	{{0,128,255}},
	{{0,255,255}},
	{{0,255,0}},
	{{255,128,0}},
	{{255,255,0}},
	{{255,0,0}},
	{{255,0,255}},
	{{255,255,255}}
};

double  compareSURFDescriptors( const float* d1, const float* d2, double best, int length )
{
	double total_cost = 0;
	assert( length % 4 == 0 );
	for( int i = 0; i < length; i += 4 )
	{
		double t0 = d1[i] - d2[i];
		double t1 = d1[i+1] - d2[i+1];
		double t2 = d1[i+2] - d2[i+2];
		double t3 = d1[i+3] - d2[i+3];
		total_cost += t0*t0 + t1*t1 + t2*t2 + t3*t3;
		if( total_cost > best )
			break; 	
	}
	return total_cost;
}

/*
*  in-place convert a given mask (created by manual segmentation)
* into a mask of 1 (background) and 0 (foreground)
*/
int  create_detection_mask(IplImage* target_mask,int x, bool is_first_run = true)
{
	if (is_first_run)
	{
#if 1               //#if 0还有一个重要的用途就是用来当成注释，如果你想要注释的程序很长，这个时候#if 0是最好的，保证不会犯错误。#if 1可以让其间的变量成为局部变量。
 		bk_label = 255;
 		
// 		if (x==0)
// 		{
// 			bk_label = 1;
// 		}
// 		else
// 		{
// 			bk_label = x;
// 		}
		

#else
#ifdef BK_MIN
		// find min
		unsigned char* p_mask;
		bk_label = 255;
		for(int i=0; i<target_mask->height; ++i)
		{
			unsigned char* p_maskrow = (unsigned char*)(target_mask->imageData + i* target_mask->widthStep);
			for(int j=0; j<target_mask->width; ++j)
			{
				if(int(p_maskrow[j]) < bk_label)
				{
					bk_label = int(p_maskrow[j]);
				}
			}
		}
#else
		// find max
		unsigned char* p_mask;
		bk_label = 0;
		for(int i=0; i<target_mask->height; ++i)
		{
			unsigned char* p_maskrow = (unsigned char*)(target_mask->imageData + i* target_mask->widthStep);
			for(int j=0; j<target_mask->width; ++j)
			{
				if(int(p_maskrow[j]) > bk_label)
				{
					bk_label = int(p_maskrow[j]);
				}
			}
		}
#endif
#endif
	}

	if(!target_mask)
	{
//		cout << "mask image loading error" << endl;
		AfxMessageBox("mask image loading error!\n\n确保mask文件存在！");
	}

	for(int i=0; i<target_mask->height; ++i)
	{
		unsigned char* p_maskrow = (unsigned char*)(target_mask->imageData + i* target_mask->widthStep);
		for(int j=0; j<target_mask->width; ++j)
		{
			
			if(int(p_maskrow[j]) ==x)
				p_maskrow[j] = (unsigned char) 255;
 			else
				p_maskrow[j] = (unsigned char) 0;	

// 			if(int(p_maskrow[j]) ==bk_label)
// 				p_maskrow[j] = (unsigned char) 255;
// 			else
// 				p_maskrow[j] = (unsigned char) 0;

		}

	}
	//cvNamedWindow("frame_mask", 1);
	//cvShowImage( "frame_mask", target_mask );
	//cvWaitKey(0);
	
	
	return 0;
}


int naiveNearestNeighbor( const float* vec, int laplacian,
					 const CvSeq* model_keypoints,
					 const CvSeq* model_descriptors,
					 double& score )
{
	int length = (int)(model_descriptors->elem_size/sizeof(float));
	int i, neighbor = -1;
	double d, dist1 = 1e6, dist2 = 1e6;
	score = 0.0;
	CvSeqReader reader, kreader;
	cvStartReadSeq( model_keypoints, &kreader, 0 );//读序列
	cvStartReadSeq( model_descriptors, &reader, 0 );

	for( i = 0; i < model_descriptors->total; i++ )
	{
		const CvSURFPoint* kp = (const CvSURFPoint*)kreader.ptr;
		const float* mvec = (const float*)reader.ptr;
		CV_NEXT_SEQ_ELEM( kreader.seq->elem_size, kreader );
		CV_NEXT_SEQ_ELEM( reader.seq->elem_size, reader );//读取下一个特征点,等价于 CvSURFPoint *kp = (CvSURFPoint*)cvGetSeqElem(objectKeypoints,i);
		if( laplacian != kp->laplacian )
			continue;
		d = compareSURFDescriptors( vec, mvec, dist2, length );
		if( d < dist1 ){
			dist2 = dist1;
			dist1 = d;
			neighbor = i;
		}
		else if ( d < dist2 )
			dist2 = d;

	}
	if ( dist1 < C_THRESH*dist2 ){
		score = dist1;
		return neighbor;
	}
	return -1;
}




void
findPairs( const CvSeq* frame1Keypoints, const CvSeq* frame1Descriptors,
		  const CvSeq* frame2Keypoints, const CvSeq* frame2Descriptors,
		  vector<int>& ptpairs, double& total_score,
		  const IplImage* scene_mask, const IplImage* object_mask )
{
	int i;
	total_score = 0.0;
	CvSeqReader reader, kreader;
	cvStartReadSeq( frame1Keypoints, &kreader );
	cvStartReadSeq( frame1Descriptors, &reader );
	ptpairs.clear();

	int s = ptpairs.size()/2;
//	std::cout << "In findPairs(), initially we have " << s << " matches." << std::endl;
	for( i = 0; i < frame1Descriptors->total; i++ ){
		double score = 0.0;
		const CvSURFPoint* kp = (const CvSURFPoint*)kreader.ptr;
		const float* descriptor = (const float*)reader.ptr;
		CV_NEXT_SEQ_ELEM( kreader.seq->elem_size, kreader );
		CV_NEXT_SEQ_ELEM( reader.seq->elem_size, reader );
		int nearest_neighbor = naiveNearestNeighbor( descriptor, kp->laplacian, frame2Keypoints, frame2Descriptors, score );
		if( nearest_neighbor >= 0 ){
			total_score = total_score + score;
			ptpairs.push_back(i);
			ptpairs.push_back(nearest_neighbor);
		}




// 		MSG msg;
// 		while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
// 		{
// 			::TranslateMessage(&msg);
// 			::DispatchMessage(&msg);
// 	
	}
	s = ptpairs.size()/2;
//	std::cout << "In findPairs(), # of initial nn-matches: " << s << std::endl;
//	std::cout << "\t average similarity score is: " << total_score/s << std::endl;
}


/*
* do a cross-validating of match results contained in 'ptpairs':
*
* Inputs: *
* checkKeypoints: Keypoints that need to be verified ( nearest-neighbor in both way ).
* 					Only a subset of this (prescribed by 'ptpairs') is checked.
* checkDescriptor: corresponding descriptor
* againstKeypoints:  Keypoints of the other image. All of them will be compared against.
* againstDescriptor: corresponding descriptor
* ptpairs: matches that need to be checked
* Outputs: *
* crossValidatedPairs: matches that are two-way nearest-neighbor
* */
void
validatePairs( const CvSeq* checkKeypoints, const CvSeq* checkDescriptors,
			  const CvSeq* againstKeypoints, const CvSeq* againstDescriptors,
			  vector<int>& ptPairs, vector<int>& crossValidatedPairs, double& total_score )
{

	int i;
	total_score = 0;
	crossValidatedPairs.clear();

	int s = ptPairs.size()/2;
	double score;
	for( i = 0; i < s; i=i+2 ){
		int leftInd = ptPairs[i];
		int rightInd = ptPairs[i+1];
		const CvSURFPoint* kp = (const CvSURFPoint*)cvGetSeqElem( checkKeypoints, rightInd );
		const float* descriptor = (const float*)cvGetSeqElem( checkDescriptors, rightInd );

		int nearest_neighbor = naiveNearestNeighbor( descriptor, kp->laplacian, againstKeypoints, againstDescriptors, score );
		if( nearest_neighbor >= 0 && nearest_neighbor == leftInd ){
	//		assert (score > 0.0000000001);//score is a positive value
	//		assert (!(score <0));
			total_score = score + total_score;
			crossValidatedPairs.push_back(leftInd);
			crossValidatedPairs.push_back(rightInd);
		}



// 		MSG msg;
// 		while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
// 		{
// 			::TranslateMessage(&msg);
// 			::DispatchMessage(&msg);
// 	
	}
	s = crossValidatedPairs.size()/2.0;
//	std::cout << "In validatePairs(), # of matches after cross-validation: " << s << std::endl;
//	std::cout << "\t After cross-validation, average similarity score is: " << total_score/s << std::endl;
}


void
findPairs_withCrossValidation( const CvSeq* frame1Keypoints, const CvSeq* frame1Descriptors,
							  const CvSeq* frame2Keypoints, const CvSeq* frame2Descriptors,
							  vector<int>& ptPairs, double& total_score,
							  const IplImage* scene_mask, const IplImage* object_mask )
{
	total_score = 0.0;
	findPairs( frame1Keypoints, frame1Descriptors,
		frame2Keypoints, frame2Descriptors,
		ptPairs, total_score, scene_mask, object_mask );
//	std::cout << "Using findPairs_withCrossValidation(). \n";
	vector<int> validatedPairs;
	validatePairs( frame2Keypoints, frame2Descriptors,
		frame1Keypoints, frame1Descriptors, ptPairs, validatedPairs, total_score );
	ptPairs = validatedPairs;
}


void
flannFindPairs( const CvSeq*, const CvSeq* objectDescriptors,
			   const CvSeq*, const CvSeq* imageDescriptors, vector<int>& ptpairs )
{
	int length = (int)(objectDescriptors->elem_size/sizeof(float));

	cv::Mat m_object(objectDescriptors->total, length, CV_32F);
	cv::Mat m_image(imageDescriptors->total, length, CV_32F);


	// copy descriptors
	CvSeqReader obj_reader;
	float* obj_ptr = m_object.ptr<float>(0);
	cvStartReadSeq( objectDescriptors, &obj_reader );
	for(int i = 0; i < objectDescriptors->total; i++ ){
		const float* descriptor = (const float*)obj_reader.ptr;
		CV_NEXT_SEQ_ELEM( obj_reader.seq->elem_size, obj_reader );
		memcpy(obj_ptr, descriptor, length*sizeof(float));
		obj_ptr += length;




// 		MSG msg;
// 		while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
// 		{
// 			::TranslateMessage(&msg);
// 			::DispatchMessage(&msg);
// 	
	}
	CvSeqReader img_reader;
	float* img_ptr = m_image.ptr<float>(0);
	cvStartReadSeq( imageDescriptors, &img_reader );
	for(int i = 0; i < imageDescriptors->total; i++ ){
		const float* descriptor = (const float*)img_reader.ptr;
		CV_NEXT_SEQ_ELEM( img_reader.seq->elem_size, img_reader );
		memcpy(img_ptr, descriptor, length*sizeof(float));
		img_ptr += length;



		MSG msg;
		while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			::TranslateMessage(&msg);
			::DispatchMessage(&msg);
		}
	}

	// find nearest neighbors using FLANN
	cv::Mat m_indices(objectDescriptors->total, 2, CV_32S);
	cv::Mat m_dists(objectDescriptors->total, 2, CV_32F);
	cv::flann::Index flann_index(m_image, cv::flann::KDTreeIndexParams(4));  // using 4 randomized kdtrees
	flann_index.knnSearch(m_object, m_indices, m_dists, 2, cv::flann::SearchParams(64) ); // maximum number of leafs checked

	int* indices_ptr = m_indices.ptr<int>(0);
	float* dists_ptr = m_dists.ptr<float>(0);
	for (int i=0;i<m_indices.rows;++i) {
		if (dists_ptr[2*i]<C_THRESH*dists_ptr[2*i+1]) {
			ptpairs.push_back(i);
			ptpairs.push_back(indices_ptr[2*i]);
		}


// 		MSG msg;
// 		while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
// 		{
// 			::TranslateMessage(&msg);
// 			::DispatchMessage(&msg);
// 	
	}
}


#ifdef LOCATE_PLANAR_OBJECT
/* a rough implementation for planar object location */
int
locatePlanarObject( const IplImage* image, const IplImage* object,
				   const CvSeq* objectKeypoints, const CvSeq* objectDescriptors,
				   const CvSeq* imageKeypoints, const CvSeq* imageDescriptors,
				   double* h, vector<int>& ptpairs )
{
#ifdef SHOW_MATCHES
	cvNamedWindow("Object", 1);
	cvNamedWindow("Object correspond", 1);

	IplImage* correspond = cvCreateImage( cvSize(image->width, object->height+image->height), 8, 1 );
	cvSetImageROI( correspond, cvRect( 0, 0, object->width, object->height ) );
	cvCopy( object, correspond );
	cvSetImageROI( correspond, cvRect( 0, object->height, correspond->width, correspond->height ) );
	cvCopy( image, correspond );
	cvResetImageROI( correspond );

	IplImage* object_color = cvCreateImage(cvGetSize(object), 8, 3);
	cvCvtColor( object, object_color, CV_GRAY2BGR );
#endif//SHOW_MATCHES

	CvPoint src_corners[4] = {{0,0}, {object->width,0}, {object->width, object->height}, {0, object->height}};
	CvPoint dst_corners[4];

	CvMat _h = cvMat(3, 3, CV_64F, h);
	vector<CvPoint2D32f> pt1, pt2;
	vector<char> match;
	CvMat _pt1, _pt2, _match;
	int i, n;

#ifdef USE_FLANN
	flannFindPairs( objectKeypoints, objectDescriptors, imageKeypoints, imageDescriptors, ptpairs );
#else
	findPairs( objectKeypoints, objectDescriptors, imageKeypoints, imageDescriptors, ptpairs );
#endif

	n = ptpairs.size()/2;
	if( n < 4 )
		return 0;//too few matches for computing homography

	pt1.resize(n);
	pt2.resize(n);
	match.resize(n);
	for( i = 0; i < n; i++ ){
		pt1[i] = ((CvSURFPoint*)cvGetSeqElem(objectKeypoints,ptpairs[i*2]))->pt;
		pt2[i] = ((CvSURFPoint*)cvGetSeqElem(imageKeypoints,ptpairs[i*2+1]))->pt;
	}

	_pt1 = cvMat(1, n, CV_32FC2, &pt1[0] );
	_pt2 = cvMat(1, n, CV_32FC2, &pt2[0] );
	_match = cvMat(1, n, CV_8UC1, &match[0] );
	if( !cvFindHomography( &_pt1, &_pt2, &_h, CV_RANSAC, 5, &_match ))
		return 0;

	int N_Match = (int)ptpairs.size()/2;
	int N_Mismatch = 0;
	for( i = 0; i < N_Match; i++ ){
		if (match[i]!=0)
			N_Mismatch++;
	}
	std::cout << "Obtained " << N_Match << " matches. \n";
	std::cout << "Amongst them, " << N_Mismatch << " mismatches. \n";

	for( i = 0; i < 4; i++ ){
		double x = src_corners[i].x, y = src_corners[i].y;
		double Z = 1./(h[6]*x + h[7]*y + h[8]);
		double X = (h[0]*x + h[1]*y + h[2])*Z;
		double Y = (h[3]*x + h[4]*y + h[5])*Z;
		dst_corners[i] = cvPoint(cvRound(X), cvRound(Y));
	}

#ifdef SHOW_MATCHES
	//draw a quadrilateral around the plane object
	for( i = 0; i < 4; i++ ){
		CvPoint r1 = dst_corners[i%4];
		CvPoint r2 = dst_corners[(i+1)%4];
		cvLine( correspond, cvPoint(r1.x, r1.y+object->height ),
			cvPoint(r2.x, r2.y+object->height ), colors[8] );
	}
	cvShowImage( "Object correspond", correspond );

	//draw the features detected on object
	for( i = 0; i < objectKeypoints->total; i++ ){
		CvSURFPoint* r = (CvSURFPoint*)cvGetSeqElem( objectKeypoints, i );
		CvPoint center;
		int radius;
		center.x = cvRound(r->pt.x);
		center.y = cvRound(r->pt.y);
		radius = cvRound(r->size*1.2/9.*2);
		cvCircle( object_color, center, radius, colors[0], 1, 8, 0 );
	}
	cvShowImage( "Object", object_color );

	cvWaitKey(0);

	cvDestroyWindow("Object");
	cvDestroyWindow("Object correspond");
	cvReleaseImage(&correspond);
	cvReleaseImage(&object_color);
#endif//SHOW_MATCHES

	return 1;
}
#endif//LOCATE_PLANAR_OBJECT


/* matching features assuming homography (with RANSAC algorithm) */
int
robustFeatureMatching( const IplImage* image, const IplImage* object,
					  const CvSeq* frame1Keypoints, const CvSeq* frame1Descriptors,
					  const CvSeq* frame2Keypoints, const CvSeq* frame2Descriptors,
					  double* h, vector<int>& ptpairs, 
					  const IplImage* scene_mask, const IplImage* object_mask )
{
	//1. set up output displays

#ifdef SHOW_MATCHES
	cvNamedWindow("Object", 1);
	cvNamedWindow("Object correspond", 1);

	IplImage* correspond = cvCreateImage( cvSize(image->width, object->height+image->height), 8, 1 );
	cvSetImageROI( correspond, cvRect( 0, 0, object->width, object->height ) );
	cvCopy( object, correspond );
	cvSetImageROI( correspond, cvRect( 0, object->height, correspond->width, correspond->height ) );
	cvCopy( image, correspond );
	cvResetImageROI( correspond );

	IplImage* object_color = cvCreateImage(cvGetSize(object), 8, 3);
	cvCvtColor( object, object_color, CV_GRAY2BGR );
#endif//SHOW_MATCHES

	CvMat _h = cvMat(3, 3, CV_64F, h);
	vector<CvPoint2D32f> pt1, pt2;
	vector<char> match;
	CvMat _pt1, _pt2, _match;
	int i, n;

	//2. pair up initial matches (nearest-neighbor matching)
	double total_score = 0;
	//findPairs( frame1Keypoints, frame1Descriptors, frame2Keypoints, frame2Descriptors, ptpairs, total_score );
	findPairs_withCrossValidation( frame1Keypoints, frame1Descriptors, frame2Keypoints, frame2Descriptors,
		ptpairs, total_score, scene_mask, object_mask );

//	cvSaveImage("2.jpg",object_mask);

	n = ptpairs.size()/2;
	if( n < 4 )
	{
// 		h[0]=1;
// 		h[1]=0;
// 		h[2]=0;
// 		h[3]=0;
// 		h[4]=1;
// 		h[5]=0;
// 		h[6]=0;
// 		h[7]=0;
// 		h[8]=1;
		return 0;//too few matches
	}
	pt1.resize(n);
	pt2.resize(n);
	match.resize(n);
	for( i = 0; i < n; i++ )
	{
		pt1[i] = ((CvSURFPoint*)cvGetSeqElem(frame1Keypoints,ptpairs[i*2]))->pt;
		pt2[i] = ((CvSURFPoint*)cvGetSeqElem(frame2Keypoints,ptpairs[i*2+1]))->pt;
	}

	//3. find homography
	_pt1 = cvMat(1, n, CV_32FC2, &pt1[0] );
	_pt2 = cvMat(1, n, CV_32FC2, &pt2[0] );
	_match = cvMat(1, n, CV_8UC1, &match[0] );
	// im2 = H * im1 (pt1 in im1, etc.)
	if( !cvFindHomography( &_pt1, &_pt2, &_h, CV_RANSAC, 5, &_match ))
		return 0;

	int N_Match = (int)ptpairs.size()/2;
	int N_Mismatch = 0;
#ifdef DEBUG_SIMILARITY_IMPROVE
	int count_inliers = 0;
	double total_cost = 0;
	double cost1 = 0, maxCost = 1e6;
#endif
//	std::cout << "Obtained " << N_Match << " matches. \n";
	for( i = 0; i < N_Match; i++ ){
		CvSURFPoint* r1 = (CvSURFPoint*)cvGetSeqElem( frame1Keypoints, ptpairs[i*2] );
		CvSURFPoint* r2 = (CvSURFPoint*)cvGetSeqElem( frame2Keypoints, ptpairs[i*2+1] );
// 		std::cout << "Match #" << i
// 			<< ": (" << cvRound(r1->pt.x) << "," << cvRound(r1->pt.y) << ") to ("
// 			<< cvRound(r2->pt.x) << "," << cvRound(r2->pt.y) << ")";
		if (match[i]==0){//need to finish here
//			std::cout << ", fail.";
			N_Mismatch++;
		}
		else{
			/*add up similarity score*/
#ifdef DEBUG_SIMILARITY_IMPROVE
			const float* vec1 = (const float*)cvGetSeqElem( frame1Descriptors, ptpairs[i*2] );
			const float* vec2 = (const float*)cvGetSeqElem( frame2Descriptors, ptpairs[i*2+1] );
			int lengthD = (int)(frame2Descriptors->elem_size/sizeof(float));
			cost1 = compareSURFDescriptors( vec1, vec2, maxCost, lengthD );
			total_cost = total_cost + cost1;
			count_inliers++;
#endif
			/*draw line*/
#ifdef SHOW_MATCHES
			cvLine( correspond, cvPointFrom32f(r1->pt),
				cvPoint(cvRound(r2->pt.x), cvRound(r2->pt.y+object->height)), colors[8] );
#endif//SHOW_MATCHES
		}
//		std::cout << std::endl;


// 		MSG msg;
// 		while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
// 		{
// 			::TranslateMessage(&msg);
// 			::DispatchMessage(&msg);
// 	
	}
// 	std::cout << "Amongst total of " << N_Match << " matches, "
// 		<< N_Mismatch << " are outliers by RANSAC/homography screening.\n";

#ifdef SHOW_MATCHES
	cvShowImage( "Object correspond", correspond );
	for( i = 0; i < frame1Keypoints->total; i++ ){
		CvSURFPoint* r = (CvSURFPoint*)cvGetSeqElem( frame1Keypoints, i );
		CvPoint center;
		int radius;
		center.x = cvRound(r->pt.x);
		center.y = cvRound(r->pt.y);
		radius = cvRound(r->size*1.2/9.*2);
		cvCircle( object_color, center, radius, colors[0], 1, 8, 0 );
	}
	cvShowImage( "Object", object_color );

#ifdef DEBUG_SIMILARITY_IMPROVE
	std::cout << "Average cost of match for inliers is: " << total_cost/count_inliers << std::endl;
#endif

	cvWaitKey(0);

	cvDestroyWindow("Object");
	cvDestroyWindow("Object correspond");
	cvSaveImage("correspondence.bmp", correspond);
	cvSaveImage("object_feat.bmp", object_color);
	cvReleaseImage(&correspond);
	cvReleaseImage(&object_color);
#endif//SHOW_MATCHES

	return 1;
}


/* warp 1st image to the 2nd image, using "h" found.
* visually display results */
int warp_to_reference_frame(const IplImage* target_frame, const IplImage* reference_frame, 
							double* h, const IplImage* target_mask, const char* warped_background_file)
{
	// perspective matrix
	const CvMat _h = cvMat(3, 3, CV_64F, h);

	
#ifdef SHOW_MATCHES
	cvNamedWindow("frame-to-be-warped", 1);
	cvNamedWindow("after warp, side-by-side with reference", 1);
#endif
	//put reference frame on the left
	IplImage* sideBySide = cvCreateImage(cvSize(2 * reference_frame->width, reference_frame->height), 8, 3);
	cvSetImageROI(sideBySide, cvRect(0, 0, reference_frame->width, reference_frame->height));
	cvCopy(reference_frame, sideBySide);

	//warp target_frame with _h
	IplImage * warpedIm = cvCreateImage(cvGetSize(reference_frame), 8, 3);
	IplImage * warpedMask = cvCreateImage(cvGetSize(reference_frame), 8, 1);

	
	//cvWarpPerspective(const CvArr* src, CvArr* dst, const CvMat* mapMatrix, int flags=CV_INTER_LINEAR+CV_WARP_FILL_OUTLIERS, CvScalar fillval=cvScalarAll(0));
	cvWarpPerspective(target_frame, warpedIm, &_h, CV_INTER_CUBIC+CV_WARP_FILL_OUTLIERS);
	cvWarpPerspective(target_mask, warpedMask, &_h, CV_INTER_CUBIC+CV_WARP_FILL_OUTLIERS);

 	

	//put warped on the right
	cvSetImageROI(sideBySide, cvRect( reference_frame->width, 0, sideBySide->width, sideBySide->height));
	cvCopy(warpedIm, sideBySide);
	cvResetImageROI(sideBySide);
#ifdef SHOW_MATCHES
	cvShowImage( "frame-to-be-warped", target_frame );
	cvShowImage( "after warp, side-by-side with reference", sideBySide );

	cvWaitKey(0);
#endif
	// save only background
	unsigned char* p_im;
	for(int i=0; i<warpedMask->height; ++i)
	{
		unsigned char* p_imrow = (unsigned char*)(warpedIm->imageData + i * warpedIm->widthStep);
		unsigned char* p_maskrow = (unsigned char*)(warpedMask->imageData + i* warpedMask->widthStep);
		for(int j=0; j<warpedMask->width; ++j)
		{
#ifdef BK_MIN
			if(p_maskrow[j] > bk_label)
#else
			if(p_maskrow[j] < bk_label)
//				if(p_maskrow[j] ==255)

#endif
			{
				p_imrow[j*3] = unsigned char(0);
				p_imrow[j*3+1] = unsigned char(0);
				p_imrow[j*3+2] = unsigned char(0);
			}
		}


		MSG msg;
		while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			::TranslateMessage(&msg);
			::DispatchMessage(&msg);
		}
	}

	cvSaveImage(warped_background_file, warpedIm);
	//
#ifdef SHOW_MATCHES
	cvDestroyWindow("frame-to-be-warped");
	cvDestroyWindow("after warp, side-by-side with reference");
#endif
	cvReleaseImage (&sideBySide);
	cvReleaseImage (&warpedIm);

	return 1;
}

void lightchange(IplImage* object,IplImage* image)
{

	double pixel_brightness_ref = 0;
  double pixel_brightness_full = 0;
  for(int i=0; i<object->height; ++i)
  {
    unsigned char* p_refrow = (unsigned char*)(image->imageData + i * image->widthStep);
    unsigned char* p_imrow = (unsigned char*)(object->imageData + i * object->widthStep);
    for(int j=0; j<object->width; ++j)
    {
		if (object->nChannels==3)
		{
			int R = p_refrow[j*3+2];
			int G = p_refrow[j*3+1];
			int B = p_refrow[j*3];
			int Proc_Cb,Proc_Cr;

			int Y       = 0.299*R + 0.587*G + 0.114*B;//亮度

			Y = CLIP(Y,0,255);

			pixel_brightness_ref +=  Y;

			// do 'full_bk' image
			R = p_imrow[j*3+2];
			G = p_imrow[j*3+1];
			B = p_imrow[j*3];

			Y = 0.299*R + 0.587*G + 0.114*B;

			Y = CLIP(Y,0,255);
			pixel_brightness_full +=  Y;
		}
		else
		{
			int R = p_refrow[j];
			int Proc_Cb,Proc_Cr;
			int Y       = R;//亮度
			pixel_brightness_ref +=  Y;

			// do 'full_bk' image
			R = p_imrow[j];
			Y = R ;
			pixel_brightness_full +=  Y;
		}
    }
  }

  // step2, compensate luma
  double bright_ratio = pixel_brightness_ref / pixel_brightness_full;//亮度补偿比例
  for(int i=0; i<object->height; ++i)
  {
    unsigned char* p_imrow = (unsigned char*)(object->imageData + i * object->widthStep);
    for(int j=0; j<object->width; ++j)
    {
		if (object->nChannels==3)
		{
		int R = p_imrow[j*3+2];
        int G = p_imrow[j*3+1];
        int B = p_imrow[j*3];

        int Y       = 0.299*R + 0.587*G + 0.114*B;
        int Proc_Cb = -0.168736*R - 0.331264*G + 0.5*B + (256)/2;
        int Proc_Cr = 0.5*R - 0.418688*G - 0.081312*B + (256)/2;

        // compensate for Y
        Y = CLIP(Y * bright_ratio, 0, 255);
        int Cb = CLIP(Proc_Cb,0,255);
        int Cr = CLIP(Proc_Cr,0,255);

        // convert back to RGB
        /*
        sYCbCr2RGB = [1,      0,  1.402,
          1, -0.344,  -0.714,
          1,  1.722,      0];

        */
        Proc_Cb = (int)(Cb) - (256)/2;
        Proc_Cr = (int)(Cr) - (256)/2;
        R = Y                     + 1.402*Proc_Cr;
        G = Y - 0.34414*Proc_Cb   - 0.71414*Proc_Cr;
        B = Y + 1.772*Proc_Cb;

        R = CLIP(R, 0, 255);
        G = CLIP(G, 0, 255);
        B = CLIP(B, 0, 255);

        p_imrow[j*3+2] = R;
        p_imrow[j*3+1] = G;
        p_imrow[j*3] = B;
		}
		else
		{
			int R = p_imrow[j];

			int Y       =R;
			Y = CLIP(Y * bright_ratio, 0, 255);
			R = Y ;

			R = CLIP(R, 0, 255);
			p_imrow[j] = R;

		}
		
    }
  }
}
void backGround(char **argv,int x)
{
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	cv::initModule_nonfree();//THIS LINE IS IMPORTANT   源文件中没有这一句和对应的头文件，导致无法正确执行
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	string obj_filename(argv[0]);  //   other_frame

//	string obj_filename("F:\\s043_RightImageFilled\\ProcImg00033_RightImageFilled.jpg");
	const char* object_filename = obj_filename.c_str();

	string sce_filename(argv[1]);   //  anchor_frame
//	string sce_filename("F:\\s043_RightImageFilled\\ProcImg00034_RightImageFilled.jpg");   //  anchor_frame
	const char* scene_filename = sce_filename.c_str();
	string obj_msk_filename(argv[2]);    //other_mask
	const char* object_mask_filename = obj_msk_filename.c_str();  // mask where is background
	string sce_msk_filename(argv[3]);    // anchor_mask
	const char* scene_mask_filename = sce_msk_filename.c_str();  // mask where is background
	//string warped_bk_filename(sce_filename + string("_bk.bmp"));
	string warped_bk_filename(argv[4]);
	const char* warped_background_file = warped_bk_filename.c_str();

	CvMemStorage* storage = cvCreateMemStorage(0);//CvMemStorage *storage=cvCreateMemStorage(block_size);
	// 用来创建一个内存存储器，来统一管理各种动态对象的内存。
	//	  函数返回一个新创建的内存存储器指针。
	//	  参数block_size对应内存器中每个内存块的大小，为0时内存块默认大小为64k。

	IplImage* object = cvLoadImage( object_filename, CV_LOAD_IMAGE_GRAYSCALE );
	IplImage* image = cvLoadImage( scene_filename, CV_LOAD_IMAGE_GRAYSCALE );
	IplImage* object_org_color = cvLoadImage( object_filename, CV_LOAD_IMAGE_COLOR );
	IplImage* image_org_color = cvLoadImage( scene_filename, CV_LOAD_IMAGE_COLOR );
	IplImage* object_mask = cvLoadImage( object_mask_filename, CV_LOAD_IMAGE_GRAYSCALE );
	IplImage* scene_mask = cvLoadImage( scene_mask_filename, CV_LOAD_IMAGE_GRAYSCALE );

 //  cvSaveImage("2.jpg",object);
	if( !object || !image ){   //打开文件失败
// 		fprintf( stderr, "Can not load %s and/or %s\n"
// 			"Usage: registration_by_featurematching.exe [<left_filename> <right_filename>]\n",
// 			object_filename, scene_filename );
// 		exit(-1);
		AfxMessageBox("请确认目标帧，当前帧存在");
	}

	IplImage* object_color = cvCreateImage(cvGetSize(object), 8, 3);
	cvCvtColor( object, object_color, CV_GRAY2BGR );//cvCvtColor(...),是Opencv里的颜色空间转换函数，可以实现RGB颜色向HSV,HSI等颜色空间的转换，也可以转换为灰度图像。
	//参数CV_BGR2GRAY是RGB到gray,参数 CV_GRAY2BGR是gray到RGB. 处理结果是彩色的则转灰色就是了

	//step 2, SURF feature detection

	CvSeq *objectKeypoints = 0, *objectDescriptors = 0;

	CvSeq *imageKeypoints = 0, *imageDescriptors = 0;// CvSeq 为可动态增长元素序列，是所有OpenCV动态数据结构的基础
	CvSURFParams params = cvSURFParams(500, 1);//SURF参数设置：阈值500，生成128维描述符// 参数2：是否扩展，1 - 生成128维描述符，0 - 64维描述符

	create_detection_mask(object_mask,x);//将mask对应背景区设置为255，其余为0
	create_detection_mask(scene_mask,x, 0);
	double tt = (double)cvGetTickCount();	
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////			
	lightchange(object,image);//改变亮度//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	cvExtractSURF( object, object_mask, &objectKeypoints, &objectDescriptors, storage, params );//提取图像中的特征点
	//	printf("Object Descriptors: %d\n", objectDescriptors->total);
	cvExtractSURF( image, scene_mask, &imageKeypoints, &imageDescriptors, storage, params );
	//	printf("Image Descriptors: %d\n", imageDescriptors->total);
	tt = (double)cvGetTickCount() - tt;
	//	printf( "Extraction time = %gms\n", tt/(cvGetTickFrequency()*1000.));

	//step 3, feature matching


#ifdef USE_FLANN
//	printf("Using approximate nearest neighbor search. \n");
#endif

	double h[9];  //homography matrix, it becomes 'fundamental matrix' once called by 'robustFeatureMatching3D()'
	vector<int> ptpairs;

	// Match two planes in 3D.
//	std::cout << "\nRobustly matching features on a plane, using homography: \n";
	if( robustFeatureMatching( image, object,
		objectKeypoints, objectDescriptors, imageKeypoints,
		imageDescriptors, h, ptpairs, scene_mask, object_mask ))
//		std::cout << "Feature matching completed! (homography)\n";
//	else
//		std::cout << "Too few feature matches! (robustFeatureMatching)";


//	cvSaveImage("2.jpg",object_mask);

	//step 4, apply homography and tranform 'object' to the match image.
	// was warpReferenceImage(...)
	if (warp_to_reference_frame(object_org_color, 	image_org_color, h, object_mask,warped_background_file))
	{
		std::cout << "Done warping with the homography! \n";
	}

	//step 5, release memory and return
	cvReleaseImage(&object);
	cvReleaseImage(&image);
	cvReleaseImage(&object_color);
	cvReleaseMemStorage(&storage);
	//	return 0;
}
