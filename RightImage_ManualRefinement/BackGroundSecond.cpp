#include "stdafx.h"
/*
* merge multiple (incomplete) background images into a single one
*
* Author: Shu Fei Fan
* Created on Oct 17th, 2012
* Modified on 
*
* Sample run: 
*
*/

#include <opencv/cxcore.h>
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <iostream>
#include <vector>
#include <string>

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

#include "RightImage_ManualRefinementDlg.h"
using namespace std;



#define USE_MEDIAN



int merge_ims(IplImage** src_frames, IplImage* result_im, int N,CString &s,CString &s1)
{
 	CProgress *Ppro;
	Ppro=new CProgress;
	Ppro->Create(IDD_DIALOG4);
	Ppro->show(0);
	Ppro->show(1);

	// save only background
	//vector<pair<int, unsigned char> > gray;
	vector<unsigned char> res_val_r;
	vector<unsigned char> res_val_g;
	vector<unsigned char> res_val_b;
	for(int i=0; i<result_im->height; ++i)
	{
		unsigned char* p_res = (unsigned char*)(result_im->imageData + i * result_im->widthStep);
// 		if (!i/100)
// 			cout << "in merge.., 2, i" << endl;
		for(int j=0; j<result_im->width; ++j)
		{
			IplImage* imgp;
			unsigned char* p_value;
			for(int k=0; k<N; ++k)
			{
				imgp = src_frames[k];
				if (imgp)
				{
					p_value = (unsigned char*)imgp->imageData + i *imgp ->widthStep;
					float gray_value = (p_value[j*3] + p_value[j*3+1] + p_value[j*3+2])/3.0;
					if(gray_value > 0)//所有帧
					{
						res_val_b.push_back(p_value[j*3]);
						res_val_g.push_back(p_value[j*3+1]);
						res_val_r.push_back(p_value[j*3+2]);
					}
				}
			}
			if(res_val_b.size() > 0)
			{
				unsigned int res_value[3] = {0, 0, 0};
#ifdef USE_MEDIAN
				sort(res_val_b.begin(), res_val_b.end());
				sort(res_val_g.begin(), res_val_g.end());
				sort(res_val_r.begin(), res_val_r.end());
				int ind = res_val_b.size()/2;
				p_res[j*3] = res_val_b[ind];
				p_res[j*3+1] = res_val_g[ind];
				p_res[j*3+2] = res_val_r[ind];
#else
				for(int i=0; i < res_val_b.size(); i++)
				{
					res_value[0] += res_val_b[i];
					res_value[1] += res_val_g[i];
					res_value[2] += res_val_r[i];
				}
				p_res[j*3] = (unsigned char) (res_value[0] / (float)res_val_b.size());
				p_res[j*3+1] = (unsigned char) (res_value[1] / (float)res_val_b.size());
				p_res[j*3+2] = (unsigned char) (res_value[2] / (float)res_val_b.size());
#endif
				res_val_b.clear();
				res_val_g.clear();
				res_val_r.clear();
			}
			else
			{
				p_res[j*3] = (unsigned char)0;
				p_res[j*3+1] = (unsigned char)0;
				p_res[j*3+2] = (unsigned char)0;
			}
			//// sort 'gray', find which 'k' should be used for background pixel.
			//if(gray.size() > 0)
		}
		MSG msg;
		while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			::TranslateMessage(&msg);
			::DispatchMessage(&msg);
		}
		float t1=result_im->height;
		float t=(i+1)/t1*100;
		if (t<2)
		{
			t=2;
		}

//		Ppro->show(t);

		if (Ppro->CancleFlag==FALSE)
		{
			Ppro->show(t);
		}
		

		if ((Ppro->CancleFlag==TRUE)&&(t<100))//中途关闭进度条，取消本次操作
		{
//			Flag=TRUE;//表示取消，本次不再往下执行
			return -3;//表示取消，本次不再往下执行
//			break;
		}

		if (abs(t-100)<0.00000001)
		{
			Ppro->OnClose();

		}
	}
	
//	cout << "in merge.., 3" << endl;
	s+="\\Background_extended_"+s1+".png";
	cvSaveImage(s, result_im);
	return 1;
	
}

/*
* register target_frame 'onto' source_frame('s coordinate)
* by computing/applying homography transform:
*       source_frame = H * target_frame
*
* usage: registration_by_featurematching.exe target_frame source_frame target_mask source_background
*
*/
int BackGroundSec(char** argv,CString s,CString s1)
{
	//step 1, load images and storages

	string dir(argv[1]);
	string start(argv[2]);
	string end(argv[3]);

	IplImage* image = NULL;
	IplImage* return_im;
	int start_i = atoi(start.c_str());
	int end_i = atoi(end.c_str());
	IplImage** p_ipls = (IplImage**)malloc((end_i-start_i+1)*sizeof(IplImage*));
	char buffer[512];
	for (int i = start_i; i <= end_i; ++i)
	{
		sprintf (buffer, "Background_tmp_%05d.png", i);
		string new_filename = dir + string(buffer);
//		cout << new_filename << endl;
		image = cvLoadImage( new_filename.c_str(), CV_LOAD_IMAGE_COLOR );
		if(!image)
		{
//			cout << "error reading image" << endl;
//			AfxMessageBox("error reading image");
		}	
		p_ipls[i - start_i] = image;
		
		if(i==start_i)
			return_im = cvCreateImage(cvGetSize(image), 8, 3);
		image = NULL;
	}

	int k=merge_ims(p_ipls, return_im, end_i-start_i+1,s,s1);//s是保存路径，s1是保存的当前文件序号

	for (int i = 0; i < end_i-start_i+1; ++i)
	{
		cvReleaseImage(&p_ipls[i]);
	}

	free(p_ipls);
	cvReleaseImage(&return_im);
	if (k==-3)
	{
		return -1;
	}
	else
	{
		return 0;
	}
}