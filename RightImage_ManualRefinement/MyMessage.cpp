// MyMessage.cpp : 实现文件
//

#include "stdafx.h"
#include "RightImage_ManualRefinement.h"
#include "MyMessage.h"


// CMyMessage 对话框

IMPLEMENT_DYNAMIC(CMyMessage, CDialog)

CMyMessage::CMyMessage(CWnd* pParent /*=NULL*/)
	: CDialog(CMyMessage::IDD, pParent)
{
	/*CString text=theApp.pRight->m_pathname;
	SetWindowTextA(text);*/

}

CMyMessage::~CMyMessage()
{
}

void CMyMessage::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CMyMessage, CDialog)
	ON_BN_CLICKED(IDC_BUTTON1, &CMyMessage::OnSave)
	ON_BN_CLICKED(IDC_BUTTON2, &CMyMessage::OnUnSave)
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CMyMessage 消息处理程序


void CMyMessage::showMessage(CString s)
{
	CString text=theApp.pRight->m_pathname;
	SetWindowTextA(text);
	//GetDlgItem(IDC_STATIC)->SetWindowText(s);
}
void CMyMessage::OnSave()
{
	theApp.pRight->OnSave();
	CDialog::OnOK();

}

void CMyMessage::OnUnSave()
{
	theApp.pRight->SaveFlag=TRUE;
	CDialog::OnCancel();

}

void CMyMessage::OnClose()
{
	OnUnSave();

	CDialog::OnClose();
}
