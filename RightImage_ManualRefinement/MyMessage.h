#pragma once


// CMyMessage 对话框

class CMyMessage : public CDialog
{
	DECLARE_DYNAMIC(CMyMessage)

public:
	CMyMessage(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CMyMessage();

// 对话框数据
	enum { IDD = IDD_DIALOG2 };

	void showMessage(CString s);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnSave();
public:
	afx_msg void OnUnSave();
public:
	afx_msg void OnClose();
};
