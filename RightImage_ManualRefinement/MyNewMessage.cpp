// MyNewMessage.cpp : 实现文件
//

#include "stdafx.h"
#include "RightImage_ManualRefinement.h"
#include "MyNewMessage.h"


// CMyNewMessage 对话框

IMPLEMENT_DYNAMIC(CMyNewMessage, CDialog)

CMyNewMessage::CMyNewMessage(CWnd* pParent /*=NULL*/)
	: CDialog(CMyNewMessage::IDD, pParent)
{
	
}

CMyNewMessage::~CMyNewMessage()
{
}

void CMyNewMessage::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CMyNewMessage, CDialog)
	ON_BN_CLICKED(IDC_BUTTON1, &CMyNewMessage::OnSure)
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// CMyNewMessage 消息处理程序

void CMyNewMessage::OnSure()
{
	Flag=FALSE;
	
	GetDlgItemText(IDC_EDIT1, pictureName);
	int i=pictureName.GetLength();
	int j=0;
	for (j=0;j<i;j++)
	{
		char s=pictureName.GetAt(j);
		if((s>='0')&&(s<='9')||(s==',')||(s=='-'))
		{
			
			if (j%6==5)
			{
				if (!((s==',')||(s=='-')))
				{
					Flag=TRUE;
					MessageBox("输入的格式不正确，\n\n请按照标准格式重新输入");//两帧序号之间只能用，和-连接
					CString s="";
					GetDlgItem(IDC_EDIT1)->SetWindowText(s);
					break;
				}
				
			}
			if (i%6==0)
			{
				Flag=TRUE;
				MessageBox("最后一个字符不正确，\n\n请按照标准格式重新输入");//长度是6整数倍，说明最后序号后面加了多余字符
				CString s="";
				GetDlgItem(IDC_EDIT1)->SetWindowText(s);
				break;
			}
			else if (i%6!=5)
			{
				Flag=TRUE;
				MessageBox("每个序号为5位，\n\n请按照标准格式重新输入");//长度是6整数倍，说明最后序号后面加了多余字符
				CString s="";
				GetDlgItem(IDC_EDIT1)->SetWindowText(s);
				break;
			}
			continue;
		}
// 		else if (s==' ')
// 		{
// 			Flag=TRUE;
// 			MessageBox("输入不能为空，\n\n请按照标准格式重新输入");
// 			CString s=" ";
// 			GetDlgItem(IDC_EDIT1)->SetWindowText(s);
// 			break;
// 		}
		else
		{
			MessageBox("输入的字符不正确，\n请确保输入的是数字字符及英文逗号字符和减号字符,\n\n请重新输入");//输入有除数字及，和-之外的符号
			CString s="";
			GetDlgItem(IDC_EDIT1)->SetWindowText(s);
			Flag=TRUE;
			break;
		}
	}
	if ((pictureName==""))
	{
		Flag=TRUE;
		MessageBox("输入不能为空，\n\n请按照标准格式重新输入");
		CString s="";
		GetDlgItem(IDC_EDIT1)->SetWindowText(s);
	}

	if (Flag==FALSE)
	{
		CDialog::OnOK();
	}
	
}

void CMyNewMessage::OnClose()
{
	Flag=TRUE;
	CDialog::OnClose();
}

void CMyNewMessage::OnOK()//解决回车关闭窗口的问题
{
	OnSure();
}