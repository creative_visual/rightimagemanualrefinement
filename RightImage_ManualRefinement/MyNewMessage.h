#pragma once


// CMyNewMessage 对话框

class CMyNewMessage : public CDialog
{
	DECLARE_DYNAMIC(CMyNewMessage)

public:
	CMyNewMessage(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CMyNewMessage();

// 对话框数据
	enum { IDD = IDD_DIALOG3 };

	CString pictureName;
	BOOL Flag;

	void OnOK();
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnSure();
public:
	afx_msg void OnClose();
};
