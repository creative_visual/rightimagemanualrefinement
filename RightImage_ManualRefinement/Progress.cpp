// Progress.cpp : 实现文件
//

#include "stdafx.h"
#include "RightImage_ManualRefinement.h"
#include "Progress.h"


// CProgress 对话框

IMPLEMENT_DYNAMIC(CProgress, CDialog)

CProgress::CProgress(CWnd* pParent /*=NULL*/)
	: CDialog(CProgress::IDD, pParent)
{
	CancleFlag=FALSE;
}

CProgress::~CProgress()
{
}

void CProgress::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROGRESS1, m_progress);
}


BEGIN_MESSAGE_MAP(CProgress, CDialog)
	ON_WM_CLOSE()
	ON_EN_CHANGE(IDC_EDIT1, &CProgress::OnEnChangeEdit1)
END_MESSAGE_MAP()


// CProgress 消息处理程序
void CProgress::show(int i)
{
	m_progress.SetRange(0,100);
	m_progress.SendMessage(PBM_SETBARCOLOR, 0, RGB(0, 255, 0));

	
	

	ShowWindow(SW_SHOW);
	CString s1,s;
	s1.Format("%d",i);
	s=" 完成 "+s1+" %";
	GetDlgItem(IDC_EDIT1)->SetWindowText(s);
	m_progress.SetPos(i);

	
}

void CProgress::OnClose()
{
	CancleFlag=TRUE;
	this->OnOK();

//	CDialog::OnClose();
}

void CProgress::OnEnChangeEdit1()
{
	// TODO:  如果该控件是 RICHEDIT 控件，则它将不会
	// 发送该通知，除非重写 CDialog::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。

	// TODO:  在此添加控件通知处理程序代码
}
