#pragma once
#include "afxcmn.h"


// CProgress 对话框

class CProgress : public CDialog
{
	DECLARE_DYNAMIC(CProgress)

public:
	CProgress(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CProgress();

	BOOL CancleFlag;

// 对话框数据
	enum { IDD = IDD_DIALOG4 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CProgressCtrl m_progress;
	void show(int i);
public:
	afx_msg void OnClose();
public:
	afx_msg void OnEnChangeEdit1();
};
