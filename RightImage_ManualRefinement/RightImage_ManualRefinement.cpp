// new.cpp : 定义应用程序的类行为。
//

#include "stdafx.h"
#include "RightImage_ManualRefinement.h"
//#include "RightImage_ManualRefinementDlg.h"

//#include "newdialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CnewApp

BEGIN_MESSAGE_MAP(CRightImage_ManualRefinementApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
	ON_BN_CLICKED(IDC_BUTTON1, &CRightImage_ManualRefinementApp::OnBnClickedButton1)
END_MESSAGE_MAP()


// CnewApp 构造

CRightImage_ManualRefinementApp::CRightImage_ManualRefinementApp()
{
	// TODO: 在此处添加构造代码，
	// 将所有重要的初始化放置在 InitInstance 中
	pRight=NULL;
	
}


// 唯一的一个 CnewApp 对象

CRightImage_ManualRefinementApp theApp;




// CnewApp 初始化

BOOL CRightImage_ManualRefinementApp::InitInstance()
{//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	if(IsRunning("RightImage_ManualRefinement.exe"))                                 ///////// 解决同时打开两个实例的问题
	{                                                                                                                /////////
		AfxMessageBox("RightImage_ManualRefinement 正在运行中！");        /////////
		return FALSE;                                                                                         /////////
	}                                                                                                                /////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// 如果一个运行在 Windows XP 上的应用程序清单指定要
	// 使用 ComCtl32.dll 版本 6 或更高版本来启用可视化方式，
	//则需要 InitCommonControlsEx()。否则，将无法创建窗口。
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// 将它设置为包括所有要在应用程序中使用的
	// 公共控件类。
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	AfxEnableControlContainer();

	// 标准初始化
	// 如果未使用这些功能并希望减小
	// 最终可执行文件的大小，则应移除下列
	// 不需要的特定初始化例程
	// 更改用于存储设置的注册表项
	// TODO: 应适当修改该字符串，
	// 例如修改为公司或组织名
	SetRegistryKey(_T("应用程序向导生成的本地应用程序"));

	CRightImage_ManualRefinementDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: 在此处放置处理何时用“确定”来关闭
		//  对话框的代码
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: 在此放置处理何时用“取消”来关闭
		//  对话框的代码
	}

	// 由于对话框已关闭，所以将返回 FALSE 以便退出应用程序，
	//  而不是启动应用程序的消息泵。
	return FALSE;
}

BOOL CRightImage_ManualRefinementApp::IsRunning(LPCTSTR lpszMutexName)
{   
	HANDLE hMutex = OpenMutex(MUTEX_ALL_ACCESS,FALSE,lpszMutexName);   
	if(!hMutex)   
	{   
		CreateMutex(NULL,TRUE,lpszMutexName);   
		return   FALSE;   
	}   
	else   
		return   TRUE;   
}

void CRightImage_ManualRefinementApp::OnBnClickedButton1()
{

	// TODO: 在此添加控件通知处理程序代码
}




