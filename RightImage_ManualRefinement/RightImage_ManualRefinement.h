// new.h : PROJECT_NAME 应用程序的主头文件
//
#include "RightImage_ManualRefinementDlg.h"

#include "newdialog.h"
#pragma once

#ifndef __AFXWIN_H__
	#error "在包含此文件之前包含“stdafx.h”以生成 PCH 文件"
#endif

#include "resource.h"		// 主符号





// CnewApp:
// 有关此类的实现，请参阅 new.cpp
//

class CRightImage_ManualRefinementApp : public CWinApp
{
public:
	CRightImage_ManualRefinementApp();

CRightImage_ManualRefinementDlg *pRight;

// 重写
	public:
	virtual BOOL InitInstance();

BOOL IsRunning(LPCTSTR lpszMutexName);
// 实现

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
};

extern CRightImage_ManualRefinementApp theApp;