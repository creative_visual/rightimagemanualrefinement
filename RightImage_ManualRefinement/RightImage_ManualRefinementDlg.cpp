// newDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "RightImage_ManualRefinement.h"
#include "RightImage_ManualRefinementDlg.h"


#include <cv.h>

#include <cxcore.h>

#include <highgui.h>

#include <cmath>


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CnewDlg 对话框
CRightImage_ManualRefinementDlg::CRightImage_ManualRefinementDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRightImage_ManualRefinementDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_dspbuf=NULL;
	m_preNum=1;
	m_image = NULL;
	m_MaskImage=NULL;
	m_NewMask=NULL;
	m_buf=NULL;
	m_square=NULL;
	m_Masksquare=NULL;
	m_t1=NULL;
	m_t2=NULL;
	m_t=NULL;
	m_buf1=NULL;

	Pnewdailog=NULL;
	imagenum=0;

	m_NewPicture=0;

	m_ChangeSize=0;



	memset(m_HoleEdgePoint, 0, 4* sizeof(int));
	
	//m_pathname="E:\\7\\ProcImg00455_RightImageFilled.jpg";
	//m_pathname="E:\\a.jpg";

	m_Tools=0;
	m_scoop=3;

	m_hpos=0;
	m_vpos=0;

	flag=new BYTE[1];
	Brushflag=new BYTE[1];

	BrushRightflag=1;

	m_regionflag=0;

	preBrushRightflag=1;

	/*Pnewdailog=new CnewDialog;
	Pnewdailog->Create(IDD_NEWDIALOG);*/

// 	PTool=new Tools;
// 	PTool->Create(IDD_DIALOG1);

//	PMsg=new CMyMessage;
//	PMsg->Create(IDD_DIALOG2);
	Ppro=new CProgress;
	Ppro->Create(IDD_DIALOG4);

	m_mouseSelect=FALSE;

	SaveFlag=TRUE;//默认以保存，每对图片操作一次就改为FALSE

	OldDirFlag=FALSE;//默认不打开上一次目录

	ScaleFlag=5;//每次打开的尺寸为允许放大和缩小的尺寸

	FullBKFlag=FALSE;

	memset(flag, 0,  sizeof(BYTE));


	GetModuleFileName(NULL,m_CurPath.GetBufferSetLength(MAX_PATH+1),MAX_PATH); 
	m_CurPath.ReleaseBuffer(); 
	int pos = m_CurPath.ReverseFind('\\'); 
	m_CurPath = m_CurPath.Left(pos);

	ImageDataPath=m_CurPath+"\\"+"SaveImageData.txt";
	char *p1;
	p1=ImageDataPath.GetBuffer(ImageDataPath.GetLength());//保存 SaveImageData.txt的路径

	fopen_s(&m_ImageData,p1,"wb+");

	MaskDataPath=m_CurPath+"\\"+"SaveMaskData.txt";
	char *p2;
	p2=MaskDataPath.GetBuffer(MaskDataPath.GetLength());//保存 SaveImageData.txt的路径

	fopen_s(&m_MaskData,p2,"wb+");


	LeftButtonUp=FALSE;
	firstFlag=TRUE;
	BackgroundClour=-1;
	colour=-1;
	BRIGHTNESS_COMPENSATION=0;//全背景默认使用亮度补偿
	continueFlag=FALSE;//默认不使用继续做上一次shot没完成的那一帧
}

void CRightImage_ManualRefinementDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST2, m_lbox);
	DDX_Control(pDX, IDC_SCROLLBAR1, m_SBar);
	DDX_Control(pDX, IDC_SCROLLBAR2, m_SvBar);
}

BEGIN_MESSAGE_MAP(CRightImage_ManualRefinementDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()

//////////////////////////////////////////////////////////////////////////////
	ON_NOTIFY_EX( TTN_NEEDTEXT, 0, OnToolTipNotify)
////////////////////////////////////////////////////////////////////////////////


	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON1, &CRightImage_ManualRefinementDlg::OpenDir)
	ON_BN_CLICKED(IDC_BUTTON2, &CRightImage_ManualRefinementDlg::OnBig)
	ON_BN_CLICKED(IDC_BUTTON3, &CRightImage_ManualRefinementDlg::Reduce)
	ON_LBN_DBLCLK(IDC_LIST2, &CRightImage_ManualRefinementDlg::OpenListPicture)
	ON_BN_CLICKED(IDC_BUTTON4, &CRightImage_ManualRefinementDlg::Cancle)
	ON_WM_LBUTTONDOWN()

	ON_WM_RBUTTONDBLCLK()

	ON_WM_RBUTTONDOWN()
	ON_WM_MOUSEWHEEL()
	ON_WM_HSCROLL()
	ON_WM_VSCROLL()
	ON_BN_CLICKED(IDC_BUTTON5, &CRightImage_ManualRefinementDlg::OnSave)
//	ON_BN_CLICKED(IDC_RADIO3, &CRightImage_ManualRefinementDlg::OnEdge)
	ON_LBN_SELCHANGE(IDC_LIST2, &CRightImage_ManualRefinementDlg::OnLopen)
	ON_BN_CLICKED(IDC_BUTTON6, &CRightImage_ManualRefinementDlg::OnBleft)
	ON_BN_CLICKED(IDC_BUTTON7, &CRightImage_ManualRefinementDlg::Onleft)
	ON_BN_CLICKED(IDC_BUTTON8, &CRightImage_ManualRefinementDlg::OnBoth)
	ON_BN_CLICKED(IDC_BUTTON9, &CRightImage_ManualRefinementDlg::OnBright)
	ON_BN_CLICKED(IDC_BUTTON10, &CRightImage_ManualRefinementDlg::OnRight)
	ON_BN_CLICKED(IDC_BUTTON11, &CRightImage_ManualRefinementDlg::OnCopy)
	ON_WM_MOUSEMOVE()
	ON_WM_RBUTTONUP()

//	ON_BN_CLICKED(IDC_RADIO1, &CRightImage_ManualRefinementDlg::OnBrush)
//	ON_BN_CLICKED(IDC_RADIO5, &CRightImage_ManualRefinementDlg::OnRegion)
//	ON_BN_CLICKED(IDC_RADIO6, &CRightImage_ManualRefinementDlg::OnSelectScoop)
//	ON_BN_CLICKED(IDC_RADIO7, &CRightImage_ManualRefinementDlg::OnNormalPixel)
//	ON_BN_CLICKED(IDC_RADIO8, &CRightImage_ManualRefinementDlg::OnHolePixel)
//	ON_BN_CLICKED(IDC_RADIO9, &CRightImage_ManualRefinementDlg::OnCircle)
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	ON_BN_CLICKED(IDC_CHECK5, &CRightImage_ManualRefinementDlg::OnCircle)
	ON_BN_CLICKED(IDC_CHECK6, &CRightImage_ManualRefinementDlg::OnSelectScoop)
	ON_BN_CLICKED(IDC_CHECK7, &CRightImage_ManualRefinementDlg::OnRegion)
	ON_BN_CLICKED(IDC_CHECK8, &CRightImage_ManualRefinementDlg::OnBrush)
	ON_BN_CLICKED(IDC_CHECK9, &CRightImage_ManualRefinementDlg::OnHolePixel)
	ON_BN_CLICKED(IDC_CHECK10, &CRightImage_ManualRefinementDlg::OnNormalPixel)
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	ON_BN_CLICKED(IDC_BUTTON12, &CRightImage_ManualRefinementDlg::OnBackCopy)
//	ON_BN_CLICKED(IDC_RADI10, &CRightImage_ManualRefinementDlg::OnNew)
	ON_BN_CLICKED(IDC_CHECK1, &CRightImage_ManualRefinementDlg::OnNewPicture)
//	ON_BN_CLICKED(IDC_RADI11, &CRightImage_ManualRefinementDlg::OnBnClickedRadi11)
ON_BN_CLICKED(IDC_CHECK2, &CRightImage_ManualRefinementDlg::OnBigORLittle)
ON_BN_CLICKED(IDC_CHECK3, &CRightImage_ManualRefinementDlg::HoleEdge)
ON_BN_CLICKED(IDC_CHECK4, &CRightImage_ManualRefinementDlg::OnShowEdge)
//ON_WM_KEYDOWN()
ON_COMMAND(ID_OpenDir, &CRightImage_ManualRefinementDlg::OnOpendir)
ON_COMMAND(ID_Save, &CRightImage_ManualRefinementDlg::Save)
ON_COMMAND(ID_Reback, &CRightImage_ManualRefinementDlg::OnReback)
ON_COMMAND(ID_CopyForward, &CRightImage_ManualRefinementDlg::OnCopyforward)
ON_COMMAND(ID_CopyBackward, &CRightImage_ManualRefinementDlg::OnCopybackward)
ON_COMMAND(ID_SCOOP_Square, &CRightImage_ManualRefinementDlg::OnScoopSquare)
ON_COMMAND(ID_SCOOP_Circle, &CRightImage_ManualRefinementDlg::OnScoopCircle)
ON_COMMAND(ID_Region, &CRightImage_ManualRefinementDlg::OnRegionSelect)
ON_COMMAND(ID_BRUSH_BOTH, &CRightImage_ManualRefinementDlg::OnBrushBoth)
ON_COMMAND(ID_BRUSH_HOLEPIXEL, &CRightImage_ManualRefinementDlg::OnBrushHolepixel)
ON_COMMAND(ID_BRUSH_NORMALPIXEL, &CRightImage_ManualRefinementDlg::OnBrushNormalpixel)
ON_COMMAND(ID_right, &CRightImage_ManualRefinementDlg::Onright)
ON_COMMAND(ID_left, &CRightImage_ManualRefinementDlg::OnSelectleft)
ON_COMMAND(ID_both, &CRightImage_ManualRefinementDlg::OnSelectboth)
ON_COMMAND(ID_SRight, &CRightImage_ManualRefinementDlg::OnSright)
ON_COMMAND(ID_Sleft, &CRightImage_ManualRefinementDlg::OnSleft)
ON_COMMAND(ID_IMAGE_Biger, &CRightImage_ManualRefinementDlg::OnImageBiger)
ON_COMMAND(ID_IMAGE_Reduce, &CRightImage_ManualRefinementDlg::OnImageReduce)
ON_COMMAND(ID_Point, &CRightImage_ManualRefinementDlg::OnMousePoint)
ON_COMMAND(ID_Hand, &CRightImage_ManualRefinementDlg::OnMouseHand)
ON_COMMAND(ID_Exit, &CRightImage_ManualRefinementDlg::OnExit)


//ON_COMMAND(ID_TOOLS, &CRightImage_ManualRefinementDlg::OnTools)


ON_COMMAND(ID_UNDO, &CRightImage_ManualRefinementDlg::OnUndo)


//ON_COMMAND(ID_CPAForward, &CRightImage_ManualRefinementDlg::OnCopyAllforward)
//ON_COMMAND(ID_CPABack, &CRightImage_ManualRefinementDlg::OnCopyAllback)


ON_WM_CLOSE()
ON_COMMAND(ID_Dir, &CRightImage_ManualRefinementDlg::OnLastDir)
ON_BN_CLICKED(IDC_BUTTON13, &CRightImage_ManualRefinementDlg::OnBUNDO)
ON_BN_CLICKED(IDC_BUTTON14, &CRightImage_ManualRefinementDlg::OnHandMouse)
ON_BN_CLICKED(IDC_BUTTON15, &CRightImage_ManualRefinementDlg::OnArrowMouse)

//ON_BN_CLICKED(IDC_RADIO2, &CRightImage_ManualRefinementDlg::OnUseLastDir)

//ON_BN_CLICKED(IDC_RADIO1, &CRightImage_ManualRefinementDlg::OnUseNewDir)
ON_COMMAND(ID_MutiBackG, &CRightImage_ManualRefinementDlg::OnMutibackground)

ON_COMMAND(ID_32844, &CRightImage_ManualRefinementDlg::OnFull_bk)//全背景转换

ON_COMMAND(ID_32846, &CRightImage_ManualRefinementDlg::OnOpenMutiBK)
ON_BN_CLICKED(IDC_RADIO2, &CRightImage_ManualRefinementDlg::UseLastDir)
ON_BN_CLICKED(IDC_RADIO1, &CRightImage_ManualRefinementDlg::UseNewDir)
ON_WM_LBUTTONUP()
ON_WM_DESTROY()
ON_COMMAND(ID_32851, &CRightImage_ManualRefinementDlg::FullBKCancleLight)
ON_COMMAND(ID_32852, &CRightImage_ManualRefinementDlg::ContinueThisShot)
ON_COMMAND(ID_32853, &CRightImage_ManualRefinementDlg::LightNoColour)
ON_COMMAND(ID_32854, &CRightImage_ManualRefinementDlg::NoLoseBK)
END_MESSAGE_MAP()


// CnewDlg 消息处理程序

BOOL CRightImage_ManualRefinementDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	theApp.pRight=this;

	((CButton *)GetDlgItem(IDC_RADIO1))->SetCheck(TRUE);

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////工具栏
//	m_toolbar.Create(this);
//	m_toolbar.LoadToolBar(IDR_TOOLBAR1);
	//	m_wndtoolbar.ShowWindow(SW_SHOW);
//	m_toolbar.EnableToolTips(TRUE);

//	RepositionBars(AFX_IDW_CONTROLBAR_FIRST, AFX_IDW_CONTROLBAR_LAST, 0);
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	ReadLastpicture();//自动打开上次未做完的帧
	if(continueNum>=0)
	{
		OnLastDir();
	}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CRightImage_ManualRefinementDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

BOOL CRightImage_ManualRefinementDlg::OnToolTipNotify( UINT id, NMHDR * pNMHDR, LRESULT * pResult )
{
	TOOLTIPTEXT *pTTT = (TOOLTIPTEXT *)pNMHDR;
	UINT nID =(UINT)pNMHDR->idFrom; //获取工具栏按钮ID
	LPSTR text[]={"打开目录","保存","单步撤销","手型指针","标准指针","放大图片","缩小图片","复制前一帧图片所有黑洞区域到当前帧","复制后一帧图片所有黑洞区域到当前帧","UNDO","打开上一次目录"};

	if(nID)
	{
		nID = m_toolbar.CommandToIndex(nID); //根据ID获取按钮索引
		if(nID != -1)
		{
		//	m_toolbar.GetButtonText(nID,str); //获取工具栏文本
		//	pTTT->lpszText = str.GetBuffer(str.GetLength()); //设置提示信息文本
			pTTT->lpszText=text[nID];
			pTTT->hinst = AfxGetResourceHandle();
			return(TRUE);
		}
	}
	return(FALSE);
}

inline void CRightImage_ManualRefinementDlg::GetImageDisplayData(IplImage* dspbuf, IplImage* &imagescale, int hpos, int vpos)
{
	int i,j,i2,j2;
	int linebyte = dspbuf->widthStep;
	int linebytesclae = imagescale->widthStep;
	char *temp_dsp = dspbuf->imageData, *temp_scale = imagescale->imageData;
	UINT *dsp = (UINT *)temp_dsp, *scale = (UINT *)temp_scale;

	if( imagescale->width <= dspbuf->width && imagescale->height <= dspbuf->height )
	{
		int x = (dspbuf->width-imagescale->width)/2, y = (dspbuf->height-imagescale->height)/2;
		for( i = y, i2=0; i < imagescale->height+y, i2<imagescale->height; i++,i2++ )
			for( j = x, j2=0; j < imagescale->width+x, j2<imagescale->width; j++,j2++ )
			{
				//if( i < imagescale->height && j < imagescale->width )
				{
					dsp[i * dspbuf->width + j] = scale[(i2+vpos) * imagescale->width + (j2+hpos)];
				}
			}
	}

	if( imagescale->width <= dspbuf->width && imagescale->height > dspbuf->height )
	{
		int x = (dspbuf->width-imagescale->width)/2, y = 0;
		for( i = 0; i < dspbuf->height; i++)
			for( j = x, j2=0; j < imagescale->width+x, j2<imagescale->width; j++,j2++ )
			{
				//if( i < imagescale->height && j < imagescale->width )
				{
					dsp[i * dspbuf->width + j] = scale[(i+vpos) * imagescale->width + (j2+hpos)];
				}
			}
	}

	if( imagescale->width > dspbuf->width && imagescale->height <= dspbuf->height )
	{
		int x = 0, y = (dspbuf->height-imagescale->height)/2;
		for( i = y, i2=0; i < imagescale->height+y, i2<imagescale->height; i++,i2++ )
			for( j = 0; j < dspbuf->width; j++ )
			{
				//if( i < imagescale->height && j < imagescale->width )
				{
					dsp[i * dspbuf->width + j] = scale[(i2+vpos) * imagescale->width + (j+hpos)];
				}
			}
	}

	if( imagescale->width > dspbuf->width && imagescale->height > dspbuf->height )
	{
		DWORD oldtime = GetTickCount();
		for( i = 0; i < dspbuf->height; i++)
			for( j = 0; j < dspbuf->width; j++ )
			{
				if( i < imagescale->height && j < imagescale->width )
				{
					dsp[i * dspbuf->width + j] = scale[(i+vpos) * imagescale->width + (j+hpos)];
				}
			}
			DWORD time = GetTickCount() - oldtime;
			int kkk = 0;
	}
	temp_dsp = NULL, temp_scale = NULL;
	dsp = NULL,scale = NULL;
}


inline void CRightImage_ManualRefinementDlg::SetImageData(IplImage* &image,int j,int i,IplImage* &temp,int j1,int i1)
{
	if (image==m_image)
	{
		SaveVector(m_image,j,i);
	}
	else if (image==m_MaskImage)
	{
		SaveVector(m_MaskImage,j,i);
	}

	if (temp&&image)
	{
		if (j>=0&&j<image->height&&i>=0&&i<image->width&&j1>=0&&j1<temp->height&&i1>=0&&i1<temp->width)
		{
			image->imageData[ j * image->widthStep + image->nChannels * i + 2 ] = temp->imageData[ j1 * temp->widthStep + temp->nChannels * i1 + 2 ];
			image->imageData[ j* image->widthStep + image->nChannels * i + 1 ] = temp->imageData[ j1 * temp->widthStep + temp->nChannels * i1 + 1 ];
			image->imageData[ j * image->widthStep + image->nChannels * i + 0 ] = temp->imageData[ j1 * temp->widthStep + temp->nChannels * i1 + 0 ];
		}
		
	} 
	else if(!temp)
	{
		CString s="-------------------------该图片无对应的Mask文件！------------------ ";
		GetDlgItem(IDC_EDIT1)->SetWindowText(s);
	}
	
}

inline void CRightImage_ManualRefinementDlg::CopyImage(IplImage* &src,IplImage* &dst,float scale)
{
	CvSize dst_cvsize;
	dst_cvsize.width=(int)(src->width*scale);
	dst_cvsize.height=(int)(src->height*scale);
	cvReleaseImage(&dst);
	dst=cvCreateImage(dst_cvsize,src->depth,src->nChannels);
	cvResize(src,dst,CV_INTER_NN);
}
void CRightImage_ManualRefinementDlg::ReadImage()
{
	
	if (m_new)
	{
		m_Tools=0;//每打开新图工具选择初始化
		m_vpos=0;//防止前一张图片使用滚动条后突然打开一张新图片，对其造成影响
		m_hpos=0;
		ScaleFlag=5;//每次打开的尺寸为允许放大和缩小的尺寸
		m_NewPicture=0;
		m_ChangeSize=0;

		OnMousePoint();

		BackgroundClour=-1;

		((CButton *)GetDlgItem(IDC_CHECK6))->SetCheck(FALSE);
		((CButton *)GetDlgItem(IDC_CHECK5))->SetCheck(FALSE);
		((CButton *)GetDlgItem(IDC_CHECK7))->SetCheck(FALSE);
		((CButton *)GetDlgItem(IDC_CHECK8))->SetCheck(FALSE);
		((CButton *)GetDlgItem(IDC_CHECK9))->SetCheck(FALSE);
		((CButton *)GetDlgItem(IDC_CHECK10))->SetCheck(FALSE);
	}

	IplImage* temp = cvLoadImage(m_pathname,CV_LOAD_IMAGE_COLOR);

	//IplImage* temp = cvLoadImage("E:\\cat.jpg",CV_LOAD_IMAGE_COLOR);
	if (temp)
	{
		GetDlgItem(IDC_STATIC_IMG)->GetWindowRect(m_dsprect);

		CvSize size;

		size.height = temp->height, size.width = temp->width;

		cvReleaseImage(&m_image);
		m_image=  cvCreateImage(size, temp->depth, 4);

		m_h=m_image->height;
		m_w=m_image->width;


		int i,j;
		for(i = 0; i < temp->height; i++)
			for(j = 0; j < temp->width; j++)
			{
//				SetImageData(m_image,i,j,temp,i,j);
				m_image->imageData[ i * m_image->widthStep + m_image->nChannels * j + 2 ] = temp->imageData[ i * temp->widthStep + temp->nChannels * j + 2 ];
				m_image->imageData[ i * m_image->widthStep + m_image->nChannels * j + 1 ] = temp->imageData[ i * temp->widthStep + temp->nChannels * j + 1 ];
				m_image->imageData[ i * m_image->widthStep + m_image->nChannels * j + 0 ] = temp->imageData[ i * temp->widthStep + temp->nChannels * j + 0 ];

			}

		cvReleaseImage(&m_dspbuf);
		size.height = m_dsprect.Height(), size.width = m_dsprect.Width();
		m_dspbuf=  cvCreateImage(size, m_image->depth, 4);

//		m_buf=  cvCreateImage(size, temp->depth, 4);
//		cvResize(m_image,m_buf,CV_INTER_NN);//m_buf在每次打开图片时初始化为m_image

		m_buf=m_image;
		cvReleaseImage(&temp);

		CopyImage(m_image,m_t,1);

	} 
	else
	{
//		MessageBox("您需要的文件不存在！");
	}
}


void CRightImage_ManualRefinementDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}

	if(m_image)
	{
		CPaintDC dc(GetDlgItem(IDC_STATIC_IMG));
		CDC dcMem;
		CBitmap bmpMem;
		dcMem.CreateCompatibleDC(&dc);
		bmpMem.CreateCompatibleBitmap(&dc, m_dsprect.Width(), m_dsprect.Height());
		dcMem.SelectObject(&bmpMem);

		//int i,j,linebyte,linebyte2,linebyte3;
		int linebyte;
		int w = m_dsprect.Width();
		int h = m_dsprect.Height();
		linebyte = m_dspbuf->widthStep;

		bmpMem.SetBitmapBits(linebyte * h, m_dspbuf->imageData);
		dc.BitBlt(0, 0, w,h, &dcMem, 0, 0, SRCCOPY);

		bmpMem.DeleteObject();
		dcMem.DeleteDC(); 
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标显示。

HCURSOR CRightImage_ManualRefinementDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


//打开目录

void CRightImage_ManualRefinementDlg::OpenDir()   
{
//	WriteDir();//保存上一次目录
	 if(DirFlag==FALSE)//根据单选框状态选择调用函数
	{
		if(BST_CHECKED==((CButton*)GetDlgItem(IDC_RADIO1))->GetCheck())
		{
			OnUseNewDir();
		}
		else if (BST_CHECKED==((CButton*)GetDlgItem(IDC_RADIO2))->GetCheck())
		{
			OnUseLastDir();
		}
	}
	
	OnMousePoint();
	ReadDir();
	
	continueFlag=FALSE;//每次打开目录默认不使用继续上次功能

	if((continueNum>=0)&&(continueNum<(int) imagename.size()))
	{
		m_lbox.SetCurSel(continueNum);
	}
	else if(((m_name_begin=="ProcImg")||(m_name_begin=="origin"))&&(m_name_end==".jpg"))//////////////////////////////////////////////////默认打开目录后打开第一张
	{
		int k=(int)imagename.size()/2;
		m_lbox.SetCurSel(k);
	}
	else
	{
		m_lbox.SetCurSel(0);
	}
	
	continueNum=-1;//第一次用完后，恢复，否则打开另一目录不会默认选择第一帧或中间帧
	OnLopen();
}

void CRightImage_ManualRefinementDlg::zoom(float scale)
{
//	CString s=" ";
//	GetDlgItem(IDC_EDIT1)->SetWindowText(s);

	if (m_image)
	{
		m_vpos=0;
		m_hpos=0;
		m_holeedgeFlag=0;//默认下hole edge 只能在原图使用

		

		if ((m_image->width>10000||m_image->height>10000)&&((2-scale)<0.000001))
		{
			CString s="                             最大尺寸 ！！";
			GetDlgItem(IDC_EDIT1)->SetWindowText(s);
			
			ScaleFlag=10;
		}
		else	if ((m_image->height<2*m_h||m_image->width<2*m_w)&&((scale-0.5)<0.000001))
		{
			CString s="                             最小尺寸 ！！";
			GetDlgItem(IDC_EDIT1)->SetWindowText(s);
			ScaleFlag=0;
		}
		else 
 		{
			
			ScaleFlag=5;

			IplImage *dst=NULL;
			CopyImage(m_image,dst,scale);

			if ((scale-0.5)<0.000001)
			{
				cvReleaseImage(&m_dspbuf);
				CvSize size; //缩小后重新分配m_dspbuf，否则m_dspbuf会保留前一张图片的信息，如果这次图片尺寸小于前一张，则这张图片尺寸之外将显示前一张图片的信息
				size.height =(int) m_dsprect.Height(), size.width = (int)m_dsprect.Width();
				m_dspbuf=  cvCreateImage(size, m_image->depth, 4);
			}
			
			CopyImage(dst,m_image,1);
			m_buf=m_image;//放大后保存当前区域

			/////////////////////////////////////////////////放大mask，以便于用scoop处理放大的m_image时同步处理mask
			CopyImage(m_MaskImage,dst,scale);
			//			m_image=dst;//解决放大后小方块点击问题

			CopyImage(dst,m_MaskImage,1);

			cvReleaseImage(&dst);
			
			GetImageDisplayData(m_dspbuf, m_image, 0, 0);//解决放大后小方块点击问题
			Invalidate();
		}

		if ((m_image->width>10000||m_image->height>10000)&&((2-scale)<0.000001))
		{
			ScaleFlag=10;
		}
		else	if ((m_image->height<2*m_h||m_image->width<2*m_w)&&((scale-0.5)<0.000001))
		{
			ScaleFlag=0;
		}

		m_SvBar.SetScrollPos(0);
		m_SBar.SetScrollPos(0);
	}
}
//放大图片
void CRightImage_ManualRefinementDlg::OnBig()
{
	if (m_image)
	{
		if ((m_Tools==SQUARE)||(m_Tools==CIRCLE))
		{
			if (BST_CHECKED==((CButton*)GetDlgItem(IDC_CHECK3))->GetCheck())
			{
				((CButton*)GetDlgItem(IDC_CHECK3))->SetCheck(FALSE);
			}

			if (ScaleFlag<7)//不是最大尺寸是每次缩小都记录
			{
				int tmp_num=-1;

				num.push_back(tmp_num);
				int i=(int)num.size();
				CString s=" ";
				s.Format("%d", i);

				s="       已经操作 "+s+" 步";
				GetDlgItem(IDC_EDIT1)->SetWindowText(s);
				SaveFlag=FALSE;

			}
			float scale=2;
			zoom(scale);
		}
		else 
		{
			MessageBox("先选择scoop工具！！");
		}
	}
	else
	{
		MessageBox("请重新选择图片！！");
	}
	

}

void CRightImage_ManualRefinementDlg::Message(int &i)
{
	if (((i-701)%50)==1)
	{
	//	MessageBox("您已经连续操作超过了700步，请及时保存或者 UNDO 以防止内存不足！！");
	}
}

//缩小图片

void CRightImage_ManualRefinementDlg::Reduce()
{
	if (m_image)
	{
		if (ScaleFlag>0)//不是最小尺寸是每次缩小都记录
		{

			// 		//解决什么都不处理，直接放大2次，scoop处理后放大到最大，直接最小到最小，这期间保存了两次放大后的信息
			// 		int tmp_num=num.back();
			// 		if (tmp_num<0)
			// 		{
			// 			num.pop_back();
			// 		}
			int tmp_num=-2;

			num.push_back(tmp_num);
			int i=(int)num.size();
			CString s=" ";
			s.Format("%d", i);

			s="       已经操作 "+s+" 步";
			GetDlgItem(IDC_EDIT1)->SetWindowText(s);
			SaveFlag=FALSE;

		}
		float scale=0.5;
		zoom(scale);
	}
	else
	{
		MessageBox("请重新选择图片！！");
	}
	

// 	if ((m_image->height<2*m_h||m_image->width<2*m_w))
// 	{
// 		CString s="                             最小尺寸 ！！";
// 		GetDlgItem(IDC_EDIT1)->SetWindowText(s);
// 		ScaleFlag=0;
// 	}
// 	else
// 	{
// 		zoom(scale);
// 	}
	

}

//读取目录下图片

void CRightImage_ManualRefinementDlg::ReadDir()
{
//	WriteDir();//每次打开目录保存本次目录
	/*CFileDialog dlg(TRUE);///TRUE

	if(dlg.DoModal()==IDOK)  
		m_pathname=dlg.GetPathName();*/    //打开一个文件

//	m_image=NULL;
	CString pathname;
	m_lbox.ResetContent( );//每次打开新目录，清除之前目录信息

	imagename.clear();//每次打开新目录，清空容器内容

	m_SBar.SetScrollPos(0);
	m_SvBar.SetScrollPos(0);

	if ((OldDirFlag==FALSE)||((OldDirFlag==TRUE)&&(!m_path)))//不使用上次目录或者上次目录为空
	{
		BROWSEINFO bInfo;
		ZeroMemory(&bInfo, sizeof(bInfo));
		bInfo.hwndOwner =GetSafeHwnd();
		bInfo.lpszTitle = _T("请选择含有原始图像的目录: ");
		bInfo.ulFlags = BIF_RETURNONLYFSDIRS;// BIF_BROWSEINCLUDEFILES

		LPITEMIDLIST lpDlist; //用来保存返回信息的IDList
		lpDlist = SHBrowseForFolder(&bInfo) ; //显示选择对话框
		if(lpDlist != NULL) //用户按了确定按钮
		{
			TCHAR chPath[MAX_PATH]; //用来存储路径的字符串
			SHGetPathFromIDList(lpDlist, chPath);//把项目标识列表转化成字符串
		//*pathname = chPath; //将TCHAR类型的字符串转换为CString类型的字符串

			pathname.Format(_T("%s"),chPath);
		}
	} 
	else if(m_path)
	{
		pathname=m_path;
	}
	
	if((DirFlag==FALSE)||(BST_CHECKED==((CButton*)GetDlgItem(IDC_RADIO1))->GetCheck()))
	{
		CString s=pathname;
		GetDlgItem(IDC_EDIT4)->SetWindowText(s);
	}
	else
	{
		DirFlag=FALSE;
	}
	


	m_path=pathname;
	pathname+="\\*.jpg";

	/*CFileDialog dlg(TRUE);///TRUE

	if(dlg.DoModal()==IDOK)  
		m_pathname=dlg.GetPathName();*/

	WIN32_FIND_DATA FindFileData;

	HANDLE hFind;

	hFind = FindFirstFile(pathname, &FindFileData);

	if (hFind == INVALID_HANDLE_VALUE) 
		return;
	else 
	{
		/*while(FindNextFile(hFind, &FindFileData) != 0)
		{
			imagename.push_back(FindFileData.cFileName);
		}*/
		do 
		{
			imagename.push_back(FindFileData.cFileName);
		} 
		while (FindNextFile(hFind, &FindFileData) != 0);

		imagenum =(int) imagename.size();
	}

	int i;
	CString name;
	for(i=0;i<imagenum;i++)
	{

		int j=0;
		char c=imagename[i].GetAt(j);
		int k=imagename[i].GetLength();

		while (!((c>47)&&(c<58)))
		{
			j++;
			if(j>(k-5))
			{
				break;
			}
			c=imagename[i].GetAt(j);
		}
		m_name_begin=imagename[i].Left(j);
		if((m_name_begin=="ProcImg")||(m_name_begin=="origin"))
		{
			m_name_end=imagename[i].Right(k-j-5);
			name=imagename[i].Left(j+5);
			name=name.Right(5);
		}
		else
		{
			
			m_name_begin="";
			m_name_end="";
			name=imagename[i];
		}

		
		((CListBox* )GetDlgItem(IDC_LIST2))->InsertString(i,name);

//		((CListBox* )GetDlgItem(IDC_LIST2))->InsertString(i,imagename[i]);
	}

}

//打开listbox选中的图片  左双击

void CRightImage_ManualRefinementDlg::OpenListPicture()
{
}

inline void CRightImage_ManualRefinementDlg::Scoop(CPoint point,int &tmp_num)
{
	if (m_flag==0)
	{
		MessageBox("scoop边长已经改变，请重新右击选择区域 ！！");
	} 
	else
	{
		int x,y;//相对于图片的左上角位置
		SetPicturePoint(x,y,point);

		if (m_square&&(x>(-2))&&(x<m_image->width)&&(y>(-2))&&(y<m_image->height))
		{
			if (x<m_scoop)
			{
				x=m_scoop;
			}
			if (x>(m_image->width-m_scoop))
			{
				x=m_image->width-m_scoop;
			}
			if (y<m_scoop)
			{
				y=m_scoop;
			}
			if (y>(m_image->height-m_scoop))
			{
				y=m_image->height-m_scoop;
			}

			int i,j;
			int k=x-m_scoop;
			int l=y-m_scoop;
			for(i = 0; i < 2*m_scoop+1; i++)
				for(j= 0; j<2*m_scoop+1;j++)
				{		
					if (((j +l)>=0)&&((j +l)<m_image->height)&&((i+k)>=0)&&((i+k)<m_image->width))
					{
						if (m_Tools==SQUARE)
						{
							tmp_num++;
							SetImageData(m_image,(j +l),(i+k),m_square,j,i);
							/////////////////////////////////////////////////////////////////////////////////////////处理output mask
							SetImageData(m_MaskImage,(j +l),(i+k),m_Masksquare,j,i);

							if (m_holeedgeFlag==1)//在hole edge下操作m_image的同时对m_t1操作
							{
								BYTE tmp_g=m_t1->imageData[ (j +l) * m_t1->widthStep + m_t1->nChannels * (i+k)+ 1 ];
								BYTE tmp_g1=m_t1->imageData[ (j +l) * m_t1->widthStep + m_t1->nChannels * (i+k)+ 2 ];
								BYTE tmp_g2=m_t1->imageData[ (j +l) * m_t1->widthStep + m_t1->nChannels * (i+k)+ 0 ];

								if (!((tmp_g==255)&&(tmp_g1==0)&&(tmp_g2==0)))
								{
									SetImageData(m_t1,(j +l),(i+k),m_square,j,i);
								}
							}
						}

						if (m_Tools==CIRCLE)
						{
							int i1=abs(i-m_scoop);
							int i2=abs(j-m_scoop);

							int i7=m_scoop*m_scoop;
							int i8=i1*i1+i2*i2;

							if ((i8<i7))
							{
								tmp_num++;
								SetImageData(m_image,(j +l),(i+k),m_square,j,i);
								/////////////////////////////////////////////////////////////////////////////////////////处理output mask
								SetImageData(m_MaskImage,(j +l),(i+k),m_Masksquare,j,i);

								if (m_holeedgeFlag==1)
								{
									BYTE tmp_g=m_t1->imageData[ (j +l) * m_t1->widthStep + m_t1->nChannels * (i+k)+ 1 ];
									BYTE tmp_g1=m_t1->imageData[ (j +l) * m_t1->widthStep + m_t1->nChannels * (i+k)+ 2 ];
									BYTE tmp_g2=m_t1->imageData[ (j +l) * m_t1->widthStep + m_t1->nChannels * (i+k)+ 0 ];

									if (!((tmp_g==255)&&(tmp_g1==0)&&(tmp_g2==0)))
									{
										SetImageData(m_t1,(j +l),(i+k),m_square,j,i);
									}
								}
							}
						}
					}

				}

				
				if (m_holeedgeFlag==1)    //hole edge 下显示的其实是m_t1，但是m_t1与m_image被同步操作
				{
					GetImageDisplayData(m_dspbuf, m_t1, m_hpos, m_vpos);
					Invalidate(FALSE);
				}
				else if (m_holeedgeFlag==0)  //非hole edge 下，显示的是m_image
				{
					GetImageDisplayData(m_dspbuf, m_image, m_hpos, m_vpos);
					Invalidate(FALSE);
				}

				OnRButtonDown(0, m_point1);//显示最后一次小方块的位置

				// 每次右击改变图形，就像水平滚动条改变时那样，更新m_buf的信息

				if (m_buf->height==m_image->height)
				{
					m_buf=m_image;
				} 
				else
				{
					UpdateMbuf();
				}
		}
		else if(!m_square)
		{
			if ((x>-2)&&(x<m_image->width)&&(y>-1)&&(y<m_image->height))
			{
				MessageBox("请先右键选择小方块或者圆！！");
			}
			
		}
	}
}

inline void CRightImage_ManualRefinementDlg::Manual(CPoint point,int &tmp_num)
{
	BYTE *tmp_flag;
	BYTE *tmp_flag1;

	int x,y;//相对于图片的左上角位置
	if (m_NewPicture==0)
	{
		SetPicturePoint(x,y,point);
	} 
	else
	{
		x=point.x;
		y=point.y;
	}

	if (m_regionflag==0)
	{
		if ((x>-2)&&(x<m_image->width)&&(y>-1)&&(y<m_image->height))
		{
			MessageBox("请先右键选定区域！！");
			GetImageDisplayData(m_dspbuf, m_image, m_hpos, m_vpos);

		}
		
		
	} 
	else
	{

		if (m_Tools==BOTH)
		{
			flag=Brushflag;
		}

		if ((m_Tools==HOLE_PIXEL)||(m_Tools==NORMAL_PIXEL))
		{
			tmp_flag=flag;
			tmp_flag1=flag;

			flag=Brushflag;
		}

		/*int x,y;//相对于图片的左上角位置
		if (m_NewPicture==0)
		{
			SetPicturePoint(x,y,point);
		} 
		else
		{
			x=point.x;
			y=point.y;
		}*/
//		SetPicturePoint(x,y,point);

		int i=0,j=0;
		int w2=m_image->width;
		int h2=m_image->height;

		int t5=(m_HoleEdgePoint[2]-m_HoleEdgePoint[1])/2;
		int t6=(m_HoleEdgePoint[3]-m_HoleEdgePoint[0])/2;
		int t7=x-t5-m_HoleEdgePoint[1];
		int t8=y-t6-m_HoleEdgePoint[0];
		int t=0;

		if ((m_HoleEdgePoint[1]+t7)<=0)
		{
			t=m_HoleEdgePoint[1]+t7;
			t7-=t;

			if ((m_HoleEdgePoint[0]+t8)<0)
			{
				t=m_HoleEdgePoint[0]+t8;

				t8+=abs(t);
			} 
			else if((m_HoleEdgePoint[3]+t8)>=h2)
			{
				t=m_HoleEdgePoint[3]+t8-h2+1;

				t8-=abs(t);

			}
		}
		else if((m_HoleEdgePoint[2]+t7)>=w2)
		{
			t=m_HoleEdgePoint[2]+t7;
			t7+=w2-t;

			if ((m_HoleEdgePoint[0]+t8)<0)
			{

				t=m_HoleEdgePoint[0]+t8;

				t8+=abs(t);
			} 
			else if((m_HoleEdgePoint[3]+t8)>=h2)
			{

				t=m_HoleEdgePoint[3]+t8-h2+1;
				t8-=abs(t);

			}
		}
		else
		{
			if ((m_HoleEdgePoint[0]+t8)<0)
			{
				t=m_HoleEdgePoint[0]+t8;

				t8+=abs(t);
			} 
			else if((m_HoleEdgePoint[3]+t8)>=h2)
			{
				t=m_HoleEdgePoint[3]+t8-h2+1;

				t8-=abs(t);
			}
		}

		if (x>0&&x<m_image->width&&y>0&&y<m_image->height)
		{
			for (j=m_HoleEdgePoint[0];j<=m_HoleEdgePoint[3];j++)
				for(i=m_HoleEdgePoint[1];i<=m_HoleEdgePoint[2];i++)
				{
					
					if(flag[j*w2+i]==1)
					{
								
						if (m_NewPicture==0)
						{
							if (((m_Tools==HOLE_PIXEL)&&(tmp_flag[j*m_image->width+i/*+l*/]==1))||((m_Tools==NORMAL_PIXEL)&&(tmp_flag[j*m_image->width+i/*+l*/]==0)))
							{
	//							tmp_num++;
								if (LeftButtonUp==TRUE)
								{
									tmp_num++;

									SetImageData(m_image,j,i,m_image,( j+t8),( i+t7));
								////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////        对 mask 图操作
									SetImageData(m_MaskImage,j,i,m_MaskImage,( j+t8),( i+t7));
								}
								else
								{
									int i1=i,j1=j;
									SetDisplayPoint(i1,j1);
//									SetImageData(m_dspbuf,j1,i1,m_image,( j+t8),( i+t7));
									m_dspbuf->imageData[ j1 * m_dspbuf->widthStep + m_dspbuf->nChannels * i1 + 2 ] = m_image->imageData[ ( j+t8) * m_image->widthStep + m_image->nChannels * ( i+t7) + 2 ];
									m_dspbuf->imageData[ j1* m_dspbuf->widthStep + m_dspbuf->nChannels * i1 + 1 ] = m_image->imageData[ ( j+t8) * m_image->widthStep + m_image->nChannels * ( i+t7) + 1 ];
									m_dspbuf->imageData[ j1 * m_dspbuf->widthStep + m_dspbuf->nChannels * i1 + 0 ] = m_image->imageData[ ( j+t8) * m_image->widthStep + m_image->nChannels * ( i+t7) + 0 ];
								}
//								SetImageData(m_MaskImage,j,i,m_MaskImage,( j+t8),( i+t7));

							}
							else if ((m_Tools==REGION)||(m_Tools==BOTH))
							{
	//							tmp_num++;
								if (LeftButtonUp==TRUE)
								{
									tmp_num++;
								}

								if (m_ShowEdge==1)////////////////////////////对m_t1处理，在勾选显示区域边界下同步操作m_t1是黑洞区域的操作与对M_image的相同
								{
									BYTE tmp_g=m_t1->imageData[ j * m_t1->widthStep + m_t1->nChannels *  i+ 1 ];
									BYTE tmp_g1=m_t1->imageData[ j * m_t1->widthStep + m_t1->nChannels * i+ 2 ];
									BYTE tmp_g2=m_t1->imageData[ j * m_t1->widthStep + m_t1->nChannels * i+ 0 ];

									if (!((tmp_g==255)&&(tmp_g1==0)&&(tmp_g2==0)))
									{
										SetImageData(m_t1,j,i,m_image,( j+t8),( i+t7));
									}
								} 

//								SetImageData(m_image,j,i,m_image,( j+t8),( i+t7));
								////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////        对 mask 图操作
//								SetImageData(m_MaskImage,j,i,m_MaskImage,( j+t8),( i+t7));
								if (LeftButtonUp==TRUE)
								{
									SetImageData(m_image,j,i,m_image,( j+t8),( i+t7));
									SetImageData(m_MaskImage,j,i,m_MaskImage,( j+t8),( i+t7));
								}
								else
								{
									int i1=i,j1=j;
									SetDisplayPoint(i1,j1);
//									SetImageData(m_dspbuf,j1,i1,m_image,( j+t8),( i+t7));
									m_dspbuf->imageData[ j1 * m_dspbuf->widthStep + m_dspbuf->nChannels * i1 + 2 ] = m_image->imageData[ ( j+t8) * m_image->widthStep + m_image->nChannels * ( i+t7) + 2 ];
									m_dspbuf->imageData[ j1* m_dspbuf->widthStep + m_dspbuf->nChannels * i1 + 1 ] = m_image->imageData[ ( j+t8) * m_image->widthStep + m_image->nChannels * ( i+t7) + 1 ];
									m_dspbuf->imageData[ j1 * m_dspbuf->widthStep + m_dspbuf->nChannels * i1 + 0 ] = m_image->imageData[ ( j+t8) * m_image->widthStep + m_image->nChannels * ( i+t7) + 0 ];

								}

// 								int i1=i,j1=j;
// 								SetDisplayPoint(i1,j1);
// 								SetImageData(m_dspbuf,j1,i1,m_image,( j+t8),( i+t7));
							}
						}
						else if (m_NewPicture==1)
						{
							if (((m_Tools==HOLE_PIXEL)&&(tmp_flag[j*m_image->width+i]==1))||((m_Tools==NORMAL_PIXEL)&&(tmp_flag[j*m_image->width+i]==0))||(m_Tools==REGION)||(m_Tools==BOTH))
							{
//								tmp_num++;
								if (LeftButtonUp==TRUE)
								{
									tmp_num++;
								}

//								SetImageData(m_image,j,i,m_t2,( j+t8),( i+t7));

								////////////////////////////////////////////////////////////////////////////    新打开图片时    对 mask 图操作
//								SetImageData(m_MaskImage,j,i,m_NewMask,( j+t8),( i+t7));
								if (LeftButtonUp==TRUE)
								{
									SetImageData(m_image,j,i,m_t2,( j+t8),( i+t7));
									SetImageData(m_MaskImage,j,i,m_MaskImage,( j+t8),( i+t7));
								}
								else
								{
									int i1=i,j1=j;
									SetDisplayPoint(i1,j1);
	//								SetImageData(m_dspbuf,j1,i1,m_t2,( j+t8),( i+t7));
									m_dspbuf->imageData[ j1 * m_dspbuf->widthStep + m_dspbuf->nChannels * i1 + 2 ] = m_t2->imageData[ ( j+t8) * m_t2->widthStep + m_t2->nChannels * ( i+t7) + 2 ];
									m_dspbuf->imageData[ j1* m_dspbuf->widthStep + m_dspbuf->nChannels * i1 + 1 ] = m_t2->imageData[ ( j+t8) * m_t2->widthStep + m_t2->nChannels * ( i+t7) + 1 ];
									m_dspbuf->imageData[ j1 * m_dspbuf->widthStep + m_dspbuf->nChannels * i1 + 0 ] = m_t2->imageData[ ( j+t8) * m_t2->widthStep + m_t2->nChannels * ( i+t7) + 0 ];

								}

								if ((m_ShowEdge==1)&&(m_Tools==REGION))   ////////////////////////////对m_t1处理，在勾选显示区域边界下同步操作m_t1是黑洞区域的操作与对M_image的相同
								{
									BYTE tmp_g=m_t1->imageData[ j * m_t1->widthStep + m_t1->nChannels * i+ 1 ];
									BYTE tmp_g1=m_t1->imageData[ j * m_t1->widthStep + m_t1->nChannels * i+ 2 ];
									BYTE tmp_g2=m_t1->imageData[ j * m_t1->widthStep + m_t1->nChannels * i+ 0 ];

									if (!((tmp_g==255)&&(tmp_g1==0)&&(tmp_g2==0)))
									{
										SetImageData(m_t1,j,i,m_t2,( j+t8),( i+t7));
									}
								}

// 								int i1=i,j1=j;
// 								SetDisplayPoint(i1,j1);
// 								SetImageData(m_dspbuf,j1,i1,m_t2,( j+t8),( i+t7));
							}
						}
						//	}
					}
				}
		}
		
		if ((m_Tools==HOLE_PIXEL)||(m_Tools==NORMAL_PIXEL))
		{
			flag=tmp_flag1;
		}
	}

	if ((m_ShowEdge==1)&&(m_Tools==REGION))
	{
		GetImageDisplayData(m_dspbuf,m_t1,m_hpos,m_vpos);//可以忽略
	}
	else if(LeftButtonUp==TRUE)
	{
		GetImageDisplayData(m_dspbuf,m_image,m_hpos,m_vpos);
	}
//	GetImageDisplayData(m_dspbuf,m_dspbuf,0,0);
	Invalidate(FALSE);

	/*if ((m_Tools==HOLE_PIXEL)||(m_Tools==NORMAL_PIXEL))
	{
		flag=tmp_flag1;
	}*/

	if (m_buf->height==m_image->height)
	{
		m_buf=m_image;
	} 
	else
	{
		UpdateMbuf();
	}

	if (m_ShowEdge==0)
	{
		UpdateMt1();//更新m_t1为m_image  --------------------------------- 在选择区域显示边界的情况下不更新m_t1
	}
}
//左单击
inline void CRightImage_ManualRefinementDlg::OnLButtonDown(UINT nFlags, CPoint point)
{
	CString s=" ";
	GetDlgItem(IDC_EDIT1)->SetWindowText(s);

	if (m_image)
	{
		int x,y;
		SetPicturePoint(x,y,point);

		if (m_image&&x>0&&x<m_image->width&&y>0&&y<m_image->height)
		{
			if (((m_name_begin=="ProcImg")||(m_name_begin=="origin"))&&(m_name_end==".jpg")&&(m_name.Left(11)!="Background_")&&(m_mouseSelect==FALSE)&&(!m_square)&&(!m_regionflag))//非手型，非多帧背景拼接图
			{
				BYTE Clour=m_MaskImage->imageData[ y* m_MaskImage->widthStep + m_MaskImage->nChannels * x];
				BackgroundClour=(int)Clour;
				CString s1,s;
				s1.Format("%d",BackgroundClour);
				s="已选择背景， 背景对应值："+s1;
				GetDlgItem(IDC_EDIT1)->SetWindowText(s);//左键弹起后提示1.5秒后消失
			}
		}
	}
	
	int tmp_num=0;
	m_point=point;

	if (m_image&&(m_mouseSelect==FALSE))
	{
		if ((m_Tools==SQUARE)||(m_Tools==CIRCLE))    //小方块功能
		{
			if (m_NewPicture==0)
			{
				Scoop(point,tmp_num);
			}
			
			
		}
		else if ((m_Tools==REGION)||(m_Tools==BOTH)||(m_Tools==HOLE_PIXEL)||(m_Tools==NORMAL_PIXEL))
		{
			((CButton *)GetDlgItem(IDC_CHECK3))->SetCheck(FALSE);//hole edge
			if (firstFlag==TRUE)
			{
				GetImageDisplayData(m_dspbuf,m_image,m_hpos,m_vpos);
				firstFlag=FALSE;
			}
			Manual(point,tmp_num);
			
		}
		else if ((m_Tools==BOTH)&&(overFlag!=1)/*||(m_Tools==4)*/)
		{
			((CButton *)GetDlgItem(IDC_CHECK3))->SetCheck(FALSE);//hole edge
			BrushRightflag=1;
			memset(Brushflag, 0, m_w * m_h* sizeof(BYTE));
		}
	} 
	else if(!m_image)
	{
//		MessageBox(" 请先选择图片！！！");
	}

	if (tmp_num>0)
	{
		num.push_back(tmp_num);
		int i=(int)num.size();
		CString s=" ";
		s.Format("%d", i);

		s="       已经操作 "+s+" 步";
		GetDlgItem(IDC_EDIT1)->SetWindowText(s);
		SaveFlag=FALSE;
	}
	CDialog::OnLButtonDown(nFlags, point);
}

void CRightImage_ManualRefinementDlg::OnLButtonUp(UINT nFlags, CPoint point)
{
	if (((m_name_begin=="ProcImg")||(m_name_begin=="origin"))&&(m_name_end==".jpg")&&(m_name.Left(11)!="Background_")&&(m_mouseSelect==FALSE)&&(!m_square)&&(!m_regionflag))//非手型，非多帧背景拼接图
	{
		CString s;
		s=" ";
		Sleep(800);
		GetDlgItem(IDC_EDIT1)->SetWindowText(s);
	}

	LeftButtonUp=TRUE;
	int tmp_num=0;
	

	if (m_image&&(m_mouseSelect==FALSE))
	{
		if ((m_Tools==REGION)||(m_Tools==BOTH)||(m_Tools==HOLE_PIXEL)||(m_Tools==NORMAL_PIXEL))
		{
			CString s=" ";
			GetDlgItem(IDC_EDIT1)->SetWindowText(s);
//			((CButton *)GetDlgItem(IDC_CHECK3))->SetCheck(FALSE);//hole edge
			Manual(point,tmp_num);
			firstFlag=TRUE;
		}
	} 

	if (tmp_num>0)
	{
		num.push_back(tmp_num);
		int i=(int)num.size();
		CString s=" ";
		s.Format("%d", i);

		s="       已经操作 "+s+" 步";
		GetDlgItem(IDC_EDIT1)->SetWindowText(s);
		SaveFlag=FALSE;
	}

	LeftButtonUp=FALSE;

	CDialog::OnLButtonUp(nFlags, point);
}

//右键双击

void CRightImage_ManualRefinementDlg::OnRButtonDblClk(UINT nFlags, CPoint point)
{	
	OnRButtonDown(nFlags, point);
	CDialog::OnRButtonDblClk(nFlags, point);
}

//鼠标右键按下  

void CRightImage_ManualRefinementDlg::OnRButtonDown(UINT nFlags, CPoint point)
{
	CString s=" ";
	GetDlgItem(IDC_EDIT1)->SetWindowText(s);

	m_point=point;
	m_point1=point;

	m_flag=1;

	if (m_image)
	{
		if ((m_Tools==SQUARE)||(m_Tools==CIRCLE))
		{

			IplImage *dst1=0;//保存scoop图像信息

			CvSize dst1_cvsize;

			dst1_cvsize.width=2*m_scoop+1;
			dst1_cvsize.height=2*m_scoop+1;
			dst1=cvCreateImage(dst1_cvsize,m_buf->depth,m_buf->nChannels);

			IplImage *dst=0;  //用来保存scoop痕迹的图片

			/*CvSize dst_cvsize;
			dst_cvsize.width=(int)(m_image->width);
			dst_cvsize.height=(int)(m_image->height);*/
			if (m_holeedgeFlag==1)
			{
				CopyImage(m_t1,dst,1);
/*				dst=cvCreateImage(dst_cvsize,m_t1->depth,m_t1->nChannels);
				cvResize(m_t1,dst,CV_INTER_NN);*/
			}
			else if (m_holeedgeFlag==0)
			{
				CopyImage(m_image,dst,1);
				/*dst=cvCreateImage(dst_cvsize,m_image->depth,m_image->nChannels);
				cvResize(m_image,dst,CV_INTER_NN);*/
			}
				
			/*int x=0,y=0;
			if (m_NewPicture==0)
			{
				GetDlgItem(IDC_STATIC_IMG)->GetWindowRect(m_dsprect);
				ScreenToClient(m_dsprect);

				int x=0,y=0;  //相对图片左上角的点坐标，用来绘图小方块

				SetPicturePoint(x,y,point);
			}
			else
			{
				x=point.x;
				y=point.y;
			}*/
			GetDlgItem(IDC_STATIC_IMG)->GetWindowRect(m_dsprect);
			ScreenToClient(m_dsprect);

				int x=0,y=0;  //相对图片左上角的点坐标，用来绘图小方块

				SetPicturePoint(x,y,point);
				if (x<m_scoop)
				{
					x=m_scoop;
				}
				if (x>(m_image->width-m_scoop))
				{
					x=m_image->width-m_scoop-1;
				}
				if (y<m_scoop)
				{
					y=m_scoop;
				}
				if (y>(m_image->height-m_scoop))
				{
					y=m_image->height-m_scoop-1;
				}

// 				if(point.y>(H+m_scoop)&&point.y<(H+h-m_scoop)&&point.x>(W+m_scoop)&&point.x<(W+w-m_scoop))
// 				{
					int i=0,j=0;
					for(i = x-m_scoop; i < x+m_scoop+1; i++)
						for(j= y-m_scoop; j<y+m_scoop+1;j++)
						{
							if (m_Tools==SQUARE)
							{
								if (i==x-m_scoop||i==x+m_scoop||j==y-m_scoop||j==y+m_scoop)
								{
									dst->imageData[ j * dst->widthStep + dst->nChannels * i + 2 ] = (BYTE)255;
									dst->imageData[ j * dst->widthStep + dst->nChannels * i+ 1 ] = 0;
									dst->imageData[ j * dst->widthStep + dst->nChannels * i + 0 ] = 0;
								}
							}
							
							if (m_Tools==CIRCLE)
							{
								int i1=abs(i-x);
								int i2=abs(j-y);
								int i3=(i1-1)*(i1-1)+i2*i2;
								int i4=i1*i1+(i2-1)*(i2-1);
								int i5=(i1+1)*(i1+1)+i2*i2;
								int i6=i1*i1+(i2+1)*(i2+1);
								int i7=m_scoop*m_scoop;
								int i8=i1*i1+i2*i2;

								if (((i8>i7)||(i8==i7))&&((i3<i7)||(i4<i7)))
								{
									dst->imageData[ j * dst->widthStep + dst->nChannels * i + 2 ] = (BYTE)255;
									dst->imageData[ j * dst->widthStep + dst->nChannels * i+ 1 ] = 0;
									dst->imageData[ j * dst->widthStep + dst->nChannels * i + 0 ] = 0;
								}
							}
						}
					GetImageDisplayData(m_dspbuf, dst, m_hpos, m_vpos);
					Invalidate(FALSE);

		//			if(point.y>=(H+m_scoop)&&point.y<=(H+h-m_scoop)&&point.x>=(W+m_scoop)&&point.x<=(W+w-m_scoop))
		//			{
		//				int i=0,j=0;
						int k=x-m_scoop;
						int l=y-m_scoop;

						for(i =0; i < 2*m_scoop+1; i++)
							for(j=0; j<2*m_scoop+1;j++)
							{
								SetImageData(dst1,j,i,m_buf,(j +l),(i+k));
								if (m_holeedgeFlag==1)
								{
									SetImageData(dst1,j,i,m_buf1,(j +l),(i+k));
								}
							}
						CopyImage(dst1,m_square,1);
						/*cvReleaseImage(&m_square);
						m_square=cvCreateImage(dst1_cvsize,m_buf->depth,m_buf->nChannels);
						cvResize(dst1,m_square,CV_INTER_NN);*/
			//	}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////保存 output 文件对应的小方块信息mask
					cvReleaseImage(&dst1);	
					dst1=cvCreateImage(dst1_cvsize,m_MaskImage->depth,m_MaskImage->nChannels);//对dst重新分配，因为mask图只有3个通道

	//					if(point.y>=(H+m_scoop)&&point.y<=(H+h-m_scoop)&&point.x>=(W+m_scoop)&&point.x<=(W+w-m_scoop))
	//					{
	//						int i=0,j=0;
	//						int k=x-m_scoop;
	//						int l=y-m_scoop;

							for(i =0; i < 2*m_scoop+1; i++)
								for(j=0; j<2*m_scoop+1;j++)
								{
									SetImageData(dst1,j,i,m_MaskImage,(j +l),(i+k));
								}
								CopyImage(dst1,m_Masksquare,1);//m_Masksquare再次被分配到3个通道，如果之前不重新给dst1分配，执行下一句出错：将4通道图片复制给3通道变量
								/*cvReleaseImage(&m_Masksquare);
								m_Masksquare=cvCreateImage(dst1_cvsize,m_MaskImage->depth,m_MaskImage->nChannels);//m_Masksquare再次被分配到3个通道，如果之前不重新给dst1分配，执行下一句出错：将4通道图片复制给3通道变量
								cvResize(dst1,m_Masksquare,CV_INTER_NN);*/
	//					}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					

					if (m_holeedgeFlag==1)
					{
						CopyImage(m_buf,m_buf1,1);
/*						cvReleaseImage(&m_buf1);
						CvSize dst_cvsize;
						//保存hole edge 前的dspimage，为在hole edge 下取小方块而不会取中边界
						dst_cvsize.width=(int)(m_buf->width);

						dst_cvsize.height=(int)(m_buf->height);

						m_buf1=cvCreateImage(dst_cvsize,m_buf->depth,m_buf->nChannels);
						cvResize(m_buf,m_buf1,CV_INTER_NN);*/
					}
//				}

			cvReleaseImage(&dst);
			
			cvReleaseImage(&dst1);	
		}
		else if (m_Tools==REGION)
		{
			((CButton *)GetDlgItem(IDC_CHECK3))->SetCheck(FALSE);//hole edge

//			m_regionflag=0; //每次选区域都将区域标记置为0，不适用于选多个区域
			CString p,text;
			int i7 = m_lbox.GetCurSel();
			m_lbox.GetText(i7, text);
			if ((m_name_begin=="ProcImg")&&(m_name_end=="_RightImageFilled.jpg"))
			{
				p=m_path+"\\"+m_name_begin+text+"_RightMask.bmp";
			}
			else if ((m_name_begin=="origin")||((m_name_begin=="ProcImg")&&(m_name_end==".jpg")))
			{
				p=m_path+"\\"+"refinement"+text+".bmp";
			}
			else
			{
				p=m_path+"\\"+text.Left(text.GetLength()-3)+"bmp";
			}
// 			CString p,p1;
// 			int l=m_pathname.GetLength();
// 			l-=15;
// 			p=m_pathname.Left(l);
// 			p+="Mask.bmp";

			CString name=m_name.Left(11);
			if (name=="Background_")
			{
				p=m_path+"\\"+"Background_"+text+"_Filled.bmp";
			}

			IplImage* t1 = cvLoadImage(p,CV_LOAD_IMAGE_COLOR);

			CopyImage(m_image,m_t1,1);
/*			CvSize dst_cvsize;
			cvReleaseImage(&m_t1);
			dst_cvsize.width=(int)(m_image->width);
			dst_cvsize.height=(int)(m_image->height);
			m_t1=cvCreateImage(dst_cvsize,m_image->depth,m_image->nChannels);//m_t1专门保存hole edge 图
			cvResize(m_image,m_t1,CV_INTER_NN);*/

			int x,y;//相对于图片的左上角位置
			int w1=m_t1->width;
			int h1=m_t1->height;

			SetPicturePoint(x,y,point);

			if (x>=0&&x<m_image->width&&y>=0&&y<m_image->height)
			{
				int tmp_num=0;
				if (t1)
				{
					SetRegionFlag(x,y,t1);

					int i=0,j=0;
					int g=0;
					int i1=0,i2=0,i3=0,i4=0;
					for(i=1;i<t1->width-1;i++)
						for (j=1;j<t1->height-1;j++)
						{
							g=flag[j*w1+i];

							if (!t1->imageData[y*t1->widthStep+t1->nChannels*x])
							{
								i1=flag[(j-1)*w1+i];
								i2=flag[(j+1)*w1+i];
								i3=flag[j*w1+(i-1)];
								i4=flag[j*w1+(i+1)];
							}

							if (!g&& ((i1==1)||(i2==1)||(i3==1)||(i4==1)) )
							{
								m_t1->imageData[ j* m_t1->widthStep + m_t1->nChannels * i+ 2 ] = 0;
								m_t1->imageData[ j* m_t1->widthStep + m_t1->nChannels * i+ 1 ] = (BYTE)255;
								m_t1->imageData[ j* m_t1->widthStep + m_t1->nChannels * i+ 0 ] = 0;

								tmp_num=1;

							} 
						}
				} 
				else
				{
					CString s="--------------------- 该图片无对应的Mask文件！------------------- ";
					GetDlgItem(IDC_EDIT1)->SetWindowText(s);
				}

				if (tmp_num>0)
				{
					m_regionflag=1;//只有选中黑洞区域才标记

				}
				else
				{
					m_regionflag=0;
				}
			}
			
				GetImageDisplayData(m_dspbuf,m_t1,m_hpos,m_vpos);
				Invalidate(FALSE);

//				m_regionflag=1;

				/*int g=flag[y*w1+x];//左键选中非黑洞区域就将区域标志置0；可以顺带清空flag
				if (g==0)
				{
					m_regionflag=0;  //这一段没起作用，原因不明，2013.7.4
				}*/

				if (m_regionflag==0)
				{
					memset(flag, 0, m_w * m_h* sizeof(BYTE));
					
				}

				cvReleaseImage(&t1);

				UpdateMbuf();
		} 
		else if ((m_Tools==BOTH)||(m_Tools==HOLE_PIXEL)||(m_Tools==NORMAL_PIXEL))
		{
			((CButton *)GetDlgItem(IDC_CHECK3))->SetCheck(FALSE);//hole edge

			UpdateMt1();

			if (m_pnt.size()==0)
			{
				m_pnt.push_back(point);
				m_ButtonDown=point;
				m_point=point;

				if (BrushRightflag==0)
				{
					memset(Brushflag, 0, m_w * m_h* sizeof(BYTE));//每连续两次以上画图却没有填充，每两次之间清空Brushflag
				}

				m_HoleEdgePoint[0]=m_h;
				m_HoleEdgePoint[1]=m_w;
				m_HoleEdgePoint[2]=0;
				m_HoleEdgePoint[3]=0;
			}

			BrushPoint(point);

		}
		else
		{
			MessageBox("请先选工具！！");     //  关闭该窗口会自动调用OnEdge（），原因不明 2013.7.4

		}
	}
	else
	{
//		MessageBox("请先选择图片！！");
	}

	CDialog::OnRButtonDown(nFlags, point);

}

// 中键滚动

BOOL CRightImage_ManualRefinementDlg::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	//ScreenToClient();
	if (m_image)
	{

		if ((m_Tools==SQUARE)||(m_Tools==CIRCLE))
		{
			m_flag=0;// 控制放大小方块时因为边长以改变，却没有及时点击左键取样

			if (zDelta>0)
			{
				if ((m_ChangeSize==0)/*&&(m_NewPicture==0)*/)
				{
					if (m_scoop<90)
					{
						m_scoop++;
					}
					else
					{
						MessageBox("最大尺寸！！");
					}
				} 
				else if(m_ChangeSize==1)
				{
					OnBig();
				}
					
			} 
			else
			{
				if ((m_ChangeSize==0)&&(m_NewPicture==0))
				{
					if (m_scoop>1)
					{
						m_scoop--;
					}
				} 
				else if(m_ChangeSize==1)
				{
					Reduce();
				}
				
			}

			if (m_ChangeSize==0)
			{
				OnRButtonDown(0, m_point);
			}
		} 
		
	} 
	
	
	return CDialog::OnMouseWheel(nFlags, zDelta, pt);
}



// 水平滚动条
void CRightImage_ManualRefinementDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	if (m_image)
	{
		if (m_buf->height==m_image->height)
		{
			m_buf=m_image;
		} 
		else
		{
			UpdateMbuf();
		}
//		UpdateMbuf();// 水平滚动条改变时，更新m_buf
		

		int t=m_image->width-m_dsprect.Width();

		if (t>0)
		{
			m_SBar.SetScrollRange(0,100);//水平滑动条范围设置为 0 到100
			//m_SBar.SetScrollPos(0); //水平滑动条初始位置设置为 0 
			int position=m_SBar.GetScrollPos();

			switch (nSBCode)
			{
			case SB_LINELEFT://向左滚动一行 
				position--; 
				break; 
			case SB_LINERIGHT://向右滚动一行 
				position++; 
				break; 
			case SB_PAGELEFT: //向左滚动一页 
				position-=10; 
				break; 
			case SB_PAGERIGHT: //向右滚动一页 
				position+=10; 
				break; 
			case SB_THUMBTRACK://拖动滑动块 
				position=nPos;//这里的nPos 就是函数 OnHScroll()的第 2 个参数 
				break; 
			case SB_LEFT://移动到最左边 
				position=0; 
				break; 
			case SB_RIGHT://移动到最右边 
				position=100; 
				break; 
			} 
			if(position>100) position=100; 
			if(position<0) position=0; 
			m_SBar.SetScrollPos(position);//根据position 的值来设定滑动块的位置

			m_hpos=position*t/100;
			if (m_holeedgeFlag==1)
			{
				GetImageDisplayData(m_dspbuf,m_t1,m_hpos,m_vpos);
				Invalidate(FALSE);
			} 
			else
			{
				GetImageDisplayData(m_dspbuf, m_image, m_hpos,m_vpos );
				Invalidate(FALSE);
			}			

		} 
	} 
	
	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}




//垂直滚动条

void CRightImage_ManualRefinementDlg::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	if (m_image)
	{
		// 垂直滚动条改变时，更新m_buf
		if (m_buf->height==m_image->height)
		{
			m_buf=m_image;
		} 
		else
		{
			UpdateMbuf();
		}

		int t=m_image->height-m_dsprect.Height();
		if (t>0)
		{
			m_SvBar.SetScrollRange(0,100);//水平滑动条范围设置为 0 到100
			//m_SBar.SetScrollPos(0); //水平滑动条初始位置设置为 0 
			int position=m_SvBar.GetScrollPos();

			switch (nSBCode)
			{
			case SB_LINELEFT://向左滚动一行 
				position--; 
				break; 
			case SB_LINERIGHT://向右滚动一行 
				position++; 
				break; 
			case SB_PAGELEFT: //向左滚动一页 
				position-=10; 
				break; 
			case SB_PAGERIGHT: //向右滚动一页 
				position+=10; 
				break; 
			case SB_THUMBTRACK://拖动滑动块 
				position=nPos;//这里的nPos 就是函数 OnHScroll()的第 2 个参数 
				break; 
			case SB_LEFT://移动到最左边 
				position=0; 
				break; 
			case SB_RIGHT://移动到最右边 
				position=100; 
				break; 

			} 
			if(position>100) position=100; 
			if(position<0) position=0; 
			m_SvBar.SetScrollPos(position);//根据position 的值来设定滑动块的位置
			//		UpdateData(FALSE);
			m_vpos=position*t/100;

			if (m_holeedgeFlag==1)
			{
				GetImageDisplayData(m_dspbuf,m_t1,m_hpos,m_vpos);
				Invalidate(FALSE);
			} 
			else
			{
				GetImageDisplayData(m_dspbuf, m_image, m_hpos, m_vpos);
				Invalidate(FALSE);
			}
		} 
	} 
	

	CDialog::OnVScroll(nSBCode, nPos, pScrollBar);
}




void CRightImage_ManualRefinementDlg::ReleaseVector()
{
// 		SaveImageData.clear();//容器清空
// 		vector<copyData>().swap(SaveImageData);
// 		SaveMaskData.clear();//容器清空
// 		vector<BYTE>().swap(SaveMaskData);
		num.clear();
		vector<int>().swap(num);
}

// 保存图片

void CRightImage_ManualRefinementDlg::OnSave()
{
	if (m_image)
	{
		CopyImage(m_image,m_t,1);

		ReleaseVector();////////////////////////////////   释放容器占有的内存

		m_regionflag=0;

//		fclose(fp);

		IplImage *dst;
		CvSize size; 
		size.height =m_h, size.width =m_w;
		dst=  cvCreateImage(size, m_image->depth, 4);
		cvResize(m_image,dst,CV_INTER_NN);

		CString p,p1;//p1用来保存 mask output的路径
		/*int l=m_pathname.GetLength();
		int l1=m_name.GetLength();

		int l2=l1-15;

		l-=l1;
		p=m_pathname.Left(l);

		p1=m_name.Left(l2);
		p1=p+p1;
		p+=m_name;*/

		p=m_pathname;
		cvSaveImage(p,dst);

		CString s="已经保存 ";
		s+=p;
		GetDlgItem(IDC_EDIT1)->SetWindowText(s);

		if ((m_name_begin=="ProcImg")&&(m_name_end=="_RightImageFilled.jpg"))//只有右图修复时才输出output文件
		{
			//////////////////////////////////////////////////////////   保存 mask 图
			cvReleaseImage(&dst);
			dst=  cvCreateImage(size, m_MaskImage->depth, 3);
			cvResize(m_MaskImage,dst,CV_INTER_NN);
			p1=p.Left(p.GetLength()-15)+"MaskManualOutput.bmp";

			cvSaveImage(p1,dst);
		}
		
/////////////////////////////////////////////////////////////////////////////////////////////
		cvReleaseImage(&dst);

		SaveFlag=TRUE;
	} 
	else
	{
		MessageBox("非法操作！！！");
	}
}

void CRightImage_ManualRefinementDlg::OnSelectScoop()
{
	if (m_image)
	{
		if (BST_CHECKED==((CButton*)GetDlgItem(IDC_CHECK6))->GetCheck())
		{
			m_Tools=1;
			((CButton *)GetDlgItem(IDC_CHECK5))->SetCheck(FALSE);
			((CButton *)GetDlgItem(IDC_CHECK7))->SetCheck(FALSE);
			((CButton *)GetDlgItem(IDC_CHECK8))->SetCheck(FALSE);
			((CButton *)GetDlgItem(IDC_CHECK9))->SetCheck(FALSE);
			((CButton *)GetDlgItem(IDC_CHECK10))->SetCheck(FALSE);
		}
		else 
		{
			m_Tools=0;
		}
		GetImageDisplayData(m_dspbuf,m_image,m_hpos,m_vpos);
		Invalidate(FALSE);

		HoleEdge();
	} 
	else
	{
		((CButton *)GetDlgItem(IDC_CHECK6))->SetCheck(FALSE);
		MessageBox(" 请选择一张图片！！！");
	}
}

void CRightImage_ManualRefinementDlg::OnCircle()
{
	if (m_image)
	{
		if (BST_CHECKED==((CButton*)GetDlgItem(IDC_CHECK5))->GetCheck())
		{
			m_Tools=6;
			((CButton *)GetDlgItem(IDC_CHECK6))->SetCheck(FALSE);
			((CButton *)GetDlgItem(IDC_CHECK7))->SetCheck(FALSE);
			((CButton *)GetDlgItem(IDC_CHECK8))->SetCheck(FALSE);
			((CButton *)GetDlgItem(IDC_CHECK9))->SetCheck(FALSE);
			((CButton *)GetDlgItem(IDC_CHECK10))->SetCheck(FALSE);
		}
		else 
		{
			m_Tools=0;
		}
		GetImageDisplayData(m_dspbuf,m_image,m_hpos,m_vpos);
		Invalidate(FALSE);
		
		HoleEdge();
	} 
	else
	{
		((CButton *)GetDlgItem(IDC_CHECK5))->SetCheck(FALSE);
		MessageBox(" 请选择一张图片！！！");
	}
}

void CRightImage_ManualRefinementDlg::OnRegion()
{
	if (m_image)
	{
		while (m_image->height>m_h)
		{
			Reduce();
		}
	
		if (BST_CHECKED==((CButton*)GetDlgItem(IDC_CHECK7))->GetCheck())
		{
			m_Tools=2;
			((CButton *)GetDlgItem(IDC_CHECK6))->SetCheck(FALSE);
			((CButton *)GetDlgItem(IDC_CHECK5))->SetCheck(FALSE);
			((CButton *)GetDlgItem(IDC_CHECK8))->SetCheck(FALSE);
			((CButton *)GetDlgItem(IDC_CHECK9))->SetCheck(FALSE);
			((CButton *)GetDlgItem(IDC_CHECK10))->SetCheck(FALSE);

			cvReleaseImage(&m_square);

//			((CButton *)GetDlgItem(IDC_CHECK3))->SetCheck(FALSE);//hole edge

			//		delete []flag;
			flag=new BYTE[m_h*m_w];
			memset(flag, 0, m_w * m_h* sizeof(BYTE));//每次选择区域工具时初始化flag
		}
		else if(BST_UNCHECKED==((CButton*)GetDlgItem(IDC_CHECK7))->GetCheck())
		{
			m_Tools=0;
			m_regionflag=0;
		}

		GetImageDisplayData(m_dspbuf,m_image,m_hpos,m_vpos);
		Invalidate(FALSE);

		HoleEdge();
	} 
	else
	{
		((CButton *)GetDlgItem(IDC_CHECK7))->SetCheck(FALSE);
		MessageBox(" 请选择一张图片！！！");
	}
}

void CRightImage_ManualRefinementDlg::OnBrush()
{
	if (m_image)
	{
		while (m_image->height>m_h)
		{
			Reduce();
		}
	
		if (BST_CHECKED==((CButton*)GetDlgItem(IDC_CHECK8))->GetCheck())
		{
			m_Tools=3;

			((CButton *)GetDlgItem(IDC_CHECK6))->SetCheck(FALSE);
			((CButton *)GetDlgItem(IDC_CHECK5))->SetCheck(FALSE);
			((CButton *)GetDlgItem(IDC_CHECK7))->SetCheck(FALSE);
			((CButton *)GetDlgItem(IDC_CHECK9))->SetCheck(FALSE);
			((CButton *)GetDlgItem(IDC_CHECK10))->SetCheck(FALSE);

			cvReleaseImage(&m_square);
//			((CButton *)GetDlgItem(IDC_CHECK3))->SetCheck(FALSE);//hole edge

			Brushflag=new BYTE[m_h*m_w];
			memset(Brushflag, 0, m_w * m_h* sizeof(BYTE));

			CopyImage(m_image,m_t1,1);
			cvReleaseImage(&m_t1);//m_t1专门保存Brush 右键画的点 图
		}
		else if(BST_UNCHECKED==((CButton*)GetDlgItem(IDC_CHECK8))->GetCheck())
		{
			m_Tools=0;
			m_regionflag=0;
		}
	

		GetImageDisplayData(m_dspbuf,m_image,m_hpos,m_vpos);
		Invalidate(FALSE);
		
		HoleEdge();
	} 
	else
	{
		((CButton *)GetDlgItem(IDC_CHECK8))->SetCheck(FALSE);
		MessageBox(" 请选择一张图片！！！");
	}
}

void CRightImage_ManualRefinementDlg::OnHolePixel()
{
	if (m_image)
	{
		while (m_image->height>m_h)
		{
			Reduce();
		}
	
		if (BST_CHECKED==((CButton*)GetDlgItem(IDC_CHECK9))->GetCheck())
		{
			m_Tools=4;

			((CButton *)GetDlgItem(IDC_CHECK6))->SetCheck(FALSE);
			((CButton *)GetDlgItem(IDC_CHECK5))->SetCheck(FALSE);
			((CButton *)GetDlgItem(IDC_CHECK7))->SetCheck(FALSE);
			((CButton *)GetDlgItem(IDC_CHECK8))->SetCheck(FALSE);
			((CButton *)GetDlgItem(IDC_CHECK10))->SetCheck(FALSE);

			cvReleaseImage(&m_square);
//			((CButton *)GetDlgItem(IDC_CHECK3))->SetCheck(FALSE);//hole edge

			Brushflag=new BYTE[m_h*m_w];
			memset(Brushflag, 0, m_w * m_h* sizeof(BYTE));
			flag=new BYTE[m_h*m_w];
			memset(flag, 0, m_w * m_h* sizeof(BYTE));

			CopyImage(m_image,m_t1,1);//m_t1专门保存Brush 右键画的点 图
		}
		else if(BST_UNCHECKED==((CButton*)GetDlgItem(IDC_CHECK9))->GetCheck())
		{
			m_Tools=0;
			m_regionflag=0;
		}
		
		GetImageDisplayData(m_dspbuf,m_image,m_hpos,m_vpos);
		Invalidate(FALSE);

		HoleEdge();
	} 
	else
	{
		((CButton *)GetDlgItem(IDC_CHECK9))->SetCheck(FALSE);
		MessageBox(" 请选择一张图片！！！");
	}
	
	
}

void CRightImage_ManualRefinementDlg::OnNormalPixel()
{
	if (m_image)
	{
		while (m_image->height>m_h)
		{
			Reduce();
		}
	
		if (BST_CHECKED==((CButton*)GetDlgItem(IDC_CHECK10))->GetCheck())
		{
			m_Tools=5;

			((CButton *)GetDlgItem(IDC_CHECK6))->SetCheck(FALSE);
			((CButton *)GetDlgItem(IDC_CHECK5))->SetCheck(FALSE);
			((CButton *)GetDlgItem(IDC_CHECK7))->SetCheck(FALSE);
			((CButton *)GetDlgItem(IDC_CHECK8))->SetCheck(FALSE);
			((CButton *)GetDlgItem(IDC_CHECK9))->SetCheck(FALSE);

			cvReleaseImage(&m_square);
//			((CButton *)GetDlgItem(IDC_CHECK3))->SetCheck(FALSE);//hole edge

			Brushflag=new BYTE[m_h*m_w];
			memset(Brushflag, 0, m_w * m_h* sizeof(BYTE));
			flag=new BYTE[m_h*m_w];
			memset(flag, 0, m_w * m_h* sizeof(BYTE));

			CopyImage(m_image,m_t1,1);//m_t1专门保存Brush 右键画的点 图
		}
		else if(BST_UNCHECKED==((CButton*)GetDlgItem(IDC_CHECK10))->GetCheck())
		{
			m_Tools=0;
			m_regionflag=0;
		}
		

		GetImageDisplayData(m_dspbuf,m_image,m_hpos,m_vpos);
		Invalidate(FALSE);
		
		HoleEdge();
	} 
	else
	{
		((CButton *)GetDlgItem(IDC_CHECK10))->SetCheck(FALSE);
		MessageBox(" 请选择一张图片！！！");
	}
}

void CRightImage_ManualRefinementDlg::Edge()
{
	CString p,text;
	int i7 = m_lbox.GetCurSel();
	m_lbox.GetText(i7, text);
	if ((m_name_begin=="ProcImg")&&(m_name_end=="_RightImageFilled.jpg"))
	{
		p=m_path+"\\"+m_name_begin+text+"_RightMask.bmp";
	}
	else if ((m_name_begin=="origin")||((m_name_begin=="ProcImg")&&(m_name_end==".jpg")))
	{
		p=m_path+"\\"+"refinement"+text+".bmp";
	}
	else
	{
		p=m_path+"\\"+text.Left(text.GetLength()-3)+"bmp";
	}
// 	int l=m_pathname.GetLength();
// 	l-=15;
// 	p=m_pathname.Left(l);
// 	p+="Mask.bmp";
	CString name=m_name.Left(11);
	if (name=="Background_")
	{
		p=m_path+"\\"+"Background_"+text+"_Filled.bmp";
	}

	IplImage *t1=cvLoadImage(p,CV_LOAD_IMAGE_COLOR);//mask图

	CopyImage(m_image,m_t1,1);
	/*CvSize dst_cvsize;
	cvReleaseImage(&m_t1);
	dst_cvsize.width=(int)(m_image->width);
	dst_cvsize.height=(int)(m_image->height);
	m_t1=cvCreateImage(dst_cvsize,m_image->depth,m_image->nChannels);//m_t1专门保存hole edge 图
	cvResize(m_image,m_t1,CV_INTER_NN);*/

	int i,j;
	int i1,i2,i3,i4;
	for(i=1;i<t1->width-1;i++)
		for (j=1;j<t1->height-1;j++)
		{
			BYTE g = t1->imageData[j*t1->widthStep+t1->nChannels*i];

			i1=t1->imageData[(j-1)*t1->widthStep+t1->nChannels*i+2];
			i2=t1->imageData[(j+1)*t1->widthStep+t1->nChannels*i+2];
			i3=t1->imageData[j*t1->widthStep+t1->nChannels*(i-1)+2];
			i4=t1->imageData[j*t1->widthStep+t1->nChannels*(i+1)+2];

			if ( g && ((i1==0)||(i2==0)||(i3==0)||(i4==0)) )
			{
				m_t1->imageData[ j* m_t1->widthStep + m_t1->nChannels * i+ 2 ] = (BYTE)0;
				m_t1->imageData[ j* m_t1->widthStep + m_t1->nChannels * i+ 1 ] = (BYTE)255;
				m_t1->imageData[ j* m_t1->widthStep + m_t1->nChannels * i+ 0 ] = (BYTE)0;
			} 
		}
		GetImageDisplayData(m_dspbuf,m_t1,m_hpos,m_vpos);
		Invalidate(FALSE);

		UpdateMbuf();//将m_buf更新为hole edge 图m_t1
}

void CRightImage_ManualRefinementDlg::RegionGrow(IplImage *image,vector<CPoint> &growpnts, CPoint point, int w,int h,BYTE *flag)
{
	CPoint nb;

	int l = point.x - 1 <  0 ? 0   : point.x - 1;
	int r = point.x + 1 >= w ? w-1 : point.x + 1;
	int t = point.y - 1 <  0 ? 0   : point.y - 1;
	int b = point.y + 1 >= h ? h-1 : point.y + 1;

	
	BYTE g = image->imageData[point.y*image->widthStep+image->nChannels*point.x];


	nb.x = l,nb.y = t;
	if( !flag[ t * w + l ] && (!g))
	{
		CPoint tmp(l , t);
		growpnts.push_back(tmp);
		flag[ t * w + l ] = 1;
	}

	nb.x = point.x,nb.y = t;
	if( !flag[ t * w + point.x ] && (!g) )
	{
		CPoint tmp(point.x , t);
		growpnts.push_back(tmp);
		flag[ t * w + point.x ] = 1;
	}

	nb.x = r,nb.y = t;
	if( !flag[ t * w + r ] && (!g))
	{
		CPoint tmp(r , t);
		growpnts.push_back(tmp);
		flag[ t * w + r ] = 1;
	}

	///////////////////////////////////////////////////////////////

	nb.x = l,nb.y = point.y;
	if( !flag[ point.y * w + l ] &&(!g) )
	{
		CPoint tmp(l , point.y);
		growpnts.push_back(tmp);
		flag[ point.y * w + l ] = 1;
	}

	nb.x = r,nb.y = point.y;
	if( !flag[ point.y * w + r ] && (!g))
	{
		CPoint tmp(r , point.y);
		growpnts.push_back(tmp);
		flag[ point.y * w + r ] = 1;
	}

	///////////////////////////////////////////////////////////////

	nb.x = l,nb.y = b;
	if( !flag[ b * w + l ] &&(!g) )
	{
		CPoint tmp(l , b);
		growpnts.push_back(tmp);
		flag[ b * w + l ] = 1;
	}

	nb.x = point.x,nb.y = b;
	if( !flag[ b * w + point.x ] && (!g) )
	{
		CPoint tmp(point.x , b);
		growpnts.push_back(tmp);
		flag[ b * w + point.x ] = 1;
	}

	nb.x = r,nb.y = b;
	if( !flag[ b * w + r ] && (!g))
	{
		CPoint tmp(r , b);
		growpnts.push_back(tmp);
		flag[ b * w + r ] = 1;
	}
}



inline void CRightImage_ManualRefinementDlg::SaveVector(IplImage * &imagePoint,int j,int i)
{
 	if (imagePoint==m_image)
 	{
		copyData tmp;

		tmp.point.y=j;
		tmp.point.x=i;

		tmp.colour[0]=imagePoint->imageData[ j * imagePoint->widthStep + imagePoint->nChannels * i + 2 ] ;
		tmp.colour[1]=imagePoint->imageData[ j* imagePoint->widthStep + imagePoint->nChannels * i + 1 ] ;
		tmp.colour[2]=imagePoint->imageData[ j * imagePoint->widthStep + imagePoint->nChannels * i + 0 ] ;

//		SaveImageData.push_back(tmp);

		fwrite(&tmp,sizeof(copyData),1,m_ImageData);

		


	}
 	else if (imagePoint==m_MaskImage)
	{
		if ((m_name_begin=="ProcImg")&&(m_name_end=="_RightImageFilled.jpg"))
		{
			BYTE tmp_byte=imagePoint->imageData[ j * imagePoint->widthStep + imagePoint->nChannels * i] ;
			fwrite(&tmp_byte,sizeof(BYTE),1,m_MaskData);
		}
		
// 		BYTE tmp_byte=imagePoint->imageData[ j * imagePoint->widthStep + imagePoint->nChannels * i] ;
// 		SaveMaskData.push_back(tmp_byte);
 	}
}

inline void CRightImage_ManualRefinementDlg::PushVector()
{
//	if (!SaveImageData.empty())
//	{
		///////////////////////////////////////////////////////////        撤销 m_image
//		copyData tmp=SaveImageData.back();//传回最后一个数据

		copyData tmp;
//		long  s=sizeof(copyData);
		fseek(m_ImageData,-12L,1);
		fread(&tmp,sizeof(copyData),1,m_ImageData);
		fseek(m_ImageData,-12L,1);
	
//根据 num 弹出所有点
		m_image->imageData[ tmp.point.y * m_image->widthStep + m_image->nChannels * tmp.point.x + 2 ] = tmp.colour[0];
		m_image->imageData[ tmp.point.y* m_image->widthStep + m_image->nChannels * tmp.point.x + 1 ] = tmp.colour[1];
		m_image->imageData[ tmp.point.y * m_image->widthStep + m_image->nChannels * tmp.point.x + 0 ] = tmp.colour[2];

		if ((m_name_begin=="ProcImg")&&(m_name_end=="_RightImageFilled.jpg"))
		{
			BYTE tmp_byte;
			fseek(m_MaskData,-1L,1);
			fread(&tmp_byte,sizeof(BYTE),1,m_MaskData);
			fseek(m_MaskData,-1L,1);
			m_MaskImage->imageData[ tmp.point.y * m_MaskImage->widthStep + m_MaskImage->nChannels * tmp.point.x + 2 ] = tmp_byte;
			m_MaskImage->imageData[ tmp.point.y* m_MaskImage->widthStep + m_MaskImage->nChannels * tmp.point.x + 1 ] = tmp_byte;
			m_MaskImage->imageData[ tmp.point.y * m_MaskImage->widthStep + m_MaskImage->nChannels * tmp.point.x + 0 ] = tmp_byte;
		}
		

	//	SaveImageData.pop_back();//删除最后一个数据

		///////////////////////////////////////////////////////////        撤销 mask

//		BYTE tmp_byte=SaveMaskData.back();//传回最后一个数据
// 		if (m_MaskImage)
// 		{
// 			m_MaskImage->imageData[ tmp.point.y * m_MaskImage->widthStep + m_MaskImage->nChannels * tmp.point.x + 2 ] = tmp_byte;
// 			m_MaskImage->imageData[ tmp.point.y* m_MaskImage->widthStep + m_MaskImage->nChannels * tmp.point.x + 1 ] = tmp_byte;
// 			m_MaskImage->imageData[ tmp.point.y * m_MaskImage->widthStep + m_MaskImage->nChannels * tmp.point.x + 0 ] = tmp_byte;
// 		} 
// 		else
// 		{
// 			MessageBox("没有Mask文件！！");
// 		}
// 
// 		SaveMaskData.pop_back();//删除最后一个数据
/////////////////////////////////////////////////////////////////////////////////////
		


//	}
	
}

void CRightImage_ManualRefinementDlg::Cancle()
{
	if (m_image)
	{
		if (!num.empty())
		{
			int i=num.back();
			int j;
			while (i<0)//仅仅放大缩小，没有对图片内容作出修改
			{
				if (i==-1)
				{
					//			Reduce();
					float scale=0.5;
					zoom(scale);
				} 
				else if (i==-2)
				{
					//			OnBig();
					float scale=2;
					zoom(scale);
				}
				num.pop_back();

				if (!num.empty())
				{
					i=num.back();
				}
				else
				{
					i=0;
				}
			}

			for (j=0;j<i;j++)
			{
				PushVector();
			}

			if (!num.empty())
			{
				num.pop_back();
			}

			int k=(int)num.size();
			CString s=" ";
			s.Format("%d", k);

			s="单步撤销成功！！！         已经撤销到了第 "+s+" 步";
			GetDlgItem(IDC_EDIT1)->SetWindowText(s);


			if (m_holeedgeFlag==1)    //hole edge 下显示的其实是m_t1，但是m_t1与m_image被同步操作
			{
				Edge();
			}
			else if (m_holeedgeFlag==0)  //非hole edge 下，显示的是m_image
			{
				GetImageDisplayData(m_dspbuf, m_image, m_hpos, m_vpos);
				Invalidate(FALSE);
			}

			UpdateMt1();
			UpdateMbuf();
		}
		else
		{
			CString s="————————————已经撤销到了最初始状态！！！———————————— ";
			GetDlgItem(IDC_EDIT1)->SetWindowText(s);
		}
	}
	else
	{
		MessageBox("图片不存在，请重新加载图片！！");
	}
}
//listbox 单击打开一张图片
void CRightImage_ManualRefinementDlg::OnLopen()
{
	

	int i = m_lbox.GetCurSel();

	if (m_NewPicture==0)
	{
		colour=-1;
		m_regionflag=0;//默认未选中区域
		m_scoop=3;//小方块初始化,双击打开新图片时初始化小方块
		//m_dspbuf=NULL;
//		cvReleaseImage(&m_dspbuf);

		m_holeedgeFlag=0;//每次新打开的图片默认关闭 hole边缘显示
		((CButton *)GetDlgItem(IDC_CHECK3))->SetCheck(FALSE);
		((CButton *)GetDlgItem(IDC_CHECK2))->SetCheck(FALSE);

		//m_square=NULL;//每打开一张新图清空小方块
		cvReleaseImage(&m_square);

		m_preNum=i-1;
	}
	
	if (i >= 0)
	{     
		CString text;     
		m_lbox.GetText(i, text);

		if (m_NewPicture==0)
		{
			if (SaveFlag==FALSE)
			{
//				MessageBox("请先保存图片！！");
//				CString s1="此帧尚未保存，要保存吗？";
//				PMsg.showMessage(s1);
	//			PMsg->ShowWindow(SW_SHOW);
				PMsg.DoModal();
				CString s=" ";
				GetDlgItem(IDC_EDIT1)->SetWindowText(s);

			} 
//			else
//			{
// 				SaveImageData.clear();//每次打开新图容器清空
// 				SaveMaskData.clear();//每次打开新图容器清空

				


				num.clear();
				ReleaseVector();

				m_name=m_name_begin+text+m_name_end;

//				SetWindowTextA(m_name);//对话框标题栏显示 文件名

				m_pathname=m_path+"\\";
				//			CString MaskPath=m_pathname+"ProcImg"+text+"_RightMask.bmp";//m_image对应的mask图路径

				m_pathname+=imagename[i];

				m_new=1;
				cvReleaseImage(&m_image);
				ReadImage();
				m_new=0;

				//			SaveVector(ImageReback,m_image);

				if (m_image)
				{
					GetImageDisplayData(m_dspbuf, m_image, m_hpos, m_vpos);
					Invalidate(FALSE);
					///////////////////////////////////////////////////////////////////////////////////// 打开对应output mask图，而不是mask图，否则每点一次之前做的output图就没原本的mask覆盖

					CString tmp_maskPath;
					//			tmp_maskPath=m_pathname.Left(m_pathname.GetLength()-15);
					//			tmp_maskPath+="MaskManualOutput.bmp";////////////// 读取MaskManualOutput.bmp的路径

					if (m_name_begin=="ProcImg")
					{
						tmp_maskPath=m_path+"\\"+m_name_begin+text+"_RightMaskManualOutput.bmp";
					}
					else if (m_name_begin=="origin")
					{
						tmp_maskPath=m_path+"\\"+"refinement"+text+".bmp";
					}

					IplImage* tmp_Mask = cvLoadImage(tmp_maskPath,CV_LOAD_IMAGE_COLOR);////////////// 读取MaskManualOutput.bmp文件

					if (!tmp_Mask)////////////// 如果打开的是区域划分文件，则改加载refinementxxxxx.bmp文件
					{
						if (m_name_begin=="ProcImg")
						{
							tmp_maskPath=m_path+"\\"+"refinement"+text+".bmp";
						}
						tmp_Mask = cvLoadImage(tmp_maskPath,CV_LOAD_IMAGE_COLOR);
					}

					if (!tmp_Mask)////////////// 如果没有MaskManualOutput.bmp文件，则改加载Mask.bmp文件
					{
						if (m_name_begin=="ProcImg")
						{
							tmp_maskPath=m_path+"\\"+m_name_begin+text+"_RightMask.bmp";
						}
						tmp_Mask = cvLoadImage(tmp_maskPath,CV_LOAD_IMAGE_COLOR);
					}


					if (!tmp_Mask)
					{
						cvReleaseImage(&tmp_Mask);
						CvSize dst_cvsize;
						dst_cvsize.width=(int)(m_image->width);
						dst_cvsize.height=(int)(m_image->height);
						tmp_Mask=cvCreateImage(dst_cvsize,m_image->depth,3);

						int i=0,j=0;
						for (j=0;j<m_image->height;j++)
							for (i=0;i<m_image->width;i++)
							{
								tmp_Mask->imageData[ j * tmp_Mask->widthStep + tmp_Mask->nChannels * i + 2 ] = m_image->imageData[ j * m_image->widthStep + m_image->nChannels * i + 1 ];
								tmp_Mask->imageData[ j* tmp_Mask->widthStep + tmp_Mask->nChannels * i + 1 ] = m_image->imageData[ j * m_image->widthStep + m_image->nChannels * i+ 1 ];
								tmp_Mask->imageData[ j * tmp_Mask->widthStep + tmp_Mask->nChannels * i + 0 ] =m_image->imageData[ j * m_image->widthStep + m_image->nChannels * i + 1 ];
							}

							if (m_name_begin=="")
							{
								tmp_maskPath=m_path+"\\"+text.Left(text.GetLength()-4)+".bmp";
							}

							cvSaveImage(tmp_maskPath,tmp_Mask);
					}

					if (tmp_Mask)
					{
						CopyImage(tmp_Mask,m_MaskImage,1);

						cvReleaseImage(&tmp_Mask);
						//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						//这段防止点击New Picture 却没有选择图片就在当选了图片的情况下使用复制新打开的图片这个功能
						CopyImage(m_image,m_t2,1);
						//////////////////////////////////////////////对专门保存新打开的mask图操作
						CopyImage(m_MaskImage,m_NewMask,1);

					} 
					else
					{
						MessageBox("由于该文件没有对应的Mask文件，使用本软件的部分功能对该文件进行操作可能不会达到您预期的效果！");
					}
				} 
				else
				{
					MessageBox("您需要的文件不存在，或者您加载的目录已改变未及时更新文件列表！");

					
				}
				
//			}
			
			
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		}
		else if (m_NewPicture==1)
		{
			CString tmp_path=m_path+"\\";
			tmp_path+=imagename[i];

			IplImage* tmp_img = cvLoadImage( tmp_path,1);  

	//		CopyImage(tmp_img,m_t2,1);//tmp_img 通道为3
			cvReleaseImage( &m_t2 );  

			CvSize dst_cvsize;
			dst_cvsize.width=(int)(tmp_img->width);
			dst_cvsize.height=(int)(tmp_img->height);

			m_t2=cvCreateImage(dst_cvsize,tmp_img->depth,4);//m_t2保存新打开的图

			int i,j;
			for(i = 0; i < tmp_img->height; i++)
				for(j = 0; j < tmp_img->width; j++)
				{
					SetImageData(m_t2,i,j,tmp_img,i,j);
				}
			
//////////////////////////////////////////////对专门保存新打开的mask图操作
			CString tmp_maskPath;
			/*tmp_maskPath=tmp_path.Left(tmp_path.GetLength()-15);
			tmp_maskPath+="MaskManualOutput.bmp";////////////// 读取MaskManualOutput.bmp的路径
			IplImage* tmp_Mask = cvLoadImage(tmp_maskPath,CV_LOAD_IMAGE_COLOR);////////////// 读取MaskManualOutput.bmp文件
			if (!tmp_Mask)////////////// 如果没有MaskManualOutput.bmp文件，则改加载Mask.bmp文件
			{
				tmp_maskPath=tmp_path.Left(tmp_path.GetLength()-15);
				tmp_maskPath+="Mask.bmp";
				tmp_Mask = cvLoadImage(tmp_maskPath,CV_LOAD_IMAGE_COLOR);
			}*/



			if (m_name_begin=="ProcImg")
			{
				tmp_maskPath=m_path+"\\"+m_name_begin+text+"_RightMaskManualOutput.bmp";
			}
			else if (m_name_begin="origin")
			{
				tmp_maskPath=m_path+"\\"+"refinment"+text+".bmp";
			}

			IplImage* tmp_Mask = cvLoadImage(tmp_maskPath,CV_LOAD_IMAGE_COLOR);////////////// 读取MaskManualOutput.bmp文件
			if (!tmp_Mask)////////////// 如果没有MaskManualOutput.bmp文件，则改加载Mask.bmp文件
			{
				if (m_name_begin=="ProcImg")
				{
					tmp_maskPath=m_path+"\\"+m_name_begin+text+"_RightMask.bmp";
				}
				tmp_Mask = cvLoadImage(tmp_maskPath,CV_LOAD_IMAGE_COLOR);
			}
			if (!tmp_Mask)////////////// 如果打开的是区域划分文件，则改加载refinementxxxxx.bmp文件
			{
				if (m_name_begin=="ProcImg")
				{
					tmp_maskPath=m_path+"\\"+"refinement"+text+".bmp";
				}
				tmp_Mask = cvLoadImage(tmp_maskPath,CV_LOAD_IMAGE_COLOR);
			}

			if (!tmp_Mask)
			{
				cvReleaseImage(&tmp_Mask);
				CvSize dst_cvsize;
				dst_cvsize.width=(int)(m_image->width);
				dst_cvsize.height=(int)(m_image->height);
				tmp_Mask=cvCreateImage(dst_cvsize,m_image->depth,3);

				int i=0,j=0;
				for (j=0;j<m_image->height;j++)
					for (i=0;i<m_image->width;i++)
					{
						tmp_Mask->imageData[ j * tmp_Mask->widthStep + tmp_Mask->nChannels * i + 2 ] = m_image->imageData[ j * m_image->widthStep + m_image->nChannels * i + 1 ];
						tmp_Mask->imageData[ j* tmp_Mask->widthStep + tmp_Mask->nChannels * i + 1 ] = m_image->imageData[ j * m_image->widthStep + m_image->nChannels * i+ 1 ];
						tmp_Mask->imageData[ j * tmp_Mask->widthStep + tmp_Mask->nChannels * i + 0 ] =m_image->imageData[ j * m_image->widthStep + m_image->nChannels * i + 1 ];
					}

					/*if (m_name_begin="")
					{
						tmp_maskPath=m_path+"\\"+text.Left(text.GetLength()-4)+".bmp";
					}
					cvSaveImage(tmp_maskPath,tmp_Mask);*/
			}

			if (tmp_Mask)
			{
				CopyImage(tmp_Mask,m_NewMask,1);
				cvReleaseImage(&tmp_Mask);

				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				cvReleaseImage(&tmp_img);
				//////////////////////////////////////////////////////////////////////显示新窗口
				if (Pnewdailog)
				{
					Pnewdailog->DestroyWindow();
				}
				Pnewdailog=new CnewDialog;
				Pnewdailog->Create(IDD_NEWDIALOG);
				Pnewdailog->showImage(m_t2,tmp_path);
				//			Pnewdailog->ShowWindow(SW_SHOW);
			} 
			else
			{
				MessageBox("没有Mask文件！");
			}


			SetListNum();
		}
	}
	else
	{     
		AfxMessageBox("未选中。");
	}
}

void CRightImage_ManualRefinementDlg::OnBleft()
{
	// 镜像 左边
	m_regiontool=0;
	Region();
}

void CRightImage_ManualRefinementDlg::Onleft()
{
	// 非 镜像 左边 
	m_regiontool=1;
	Region();
}

void CRightImage_ManualRefinementDlg::Region()
{
	

	if (m_regionflag==0)
	{
//		MessageBox("先选择区域！！");
		if (m_regiontool==5)
		{
			OnCopyAllforward();
		}
		if (m_regiontool==6)
		{
			OnCopyAllback();
		}

	}
	else if (m_Tools==REGION/*&&(m_regionflag)*/)
		{
				RegionTool();
			
				GetImageDisplayData(m_dspbuf,m_image,m_hpos,m_vpos);
				Invalidate(FALSE);

				UpdateMbuf();
		}
		else if (m_Tools==BOTH)
		{
			flag=Brushflag;
			RegionTool();

			GetImageDisplayData(m_dspbuf,m_image,m_hpos,m_vpos);
			Invalidate(FALSE);
		}
		else if ((m_Tools==HOLE_PIXEL)||(m_Tools==NORMAL_PIXEL))
		{
			RegionTool();

			GetImageDisplayData(m_dspbuf,m_image,m_hpos,m_vpos);
			Invalidate(FALSE);
		}
}

void CRightImage_ManualRefinementDlg::OnBoth()
{
	m_regiontool=4;
	Region();
}

void CRightImage_ManualRefinementDlg::OnBright()
{
	// 镜像 右 边
	m_regiontool=2;
	Region();
}

void CRightImage_ManualRefinementDlg::OnRight()
{
	// 非镜像 右边
	m_regiontool=3;
	Region();
}

void CRightImage_ManualRefinementDlg::OnCopy()
{
	if (BST_CHECKED==((CButton*)GetDlgItem(IDC_CHECK3))->GetCheck())// 选中时
	{
		((CButton *)GetDlgItem(IDC_CHECK3))->SetCheck(FALSE);
		HoleEdge();
	}
	// 区域复制
	m_regiontool=5;
	Region();
}

void CRightImage_ManualRefinementDlg::OnBackCopy()
{
	if (BST_CHECKED==((CButton*)GetDlgItem(IDC_CHECK3))->GetCheck())// 选中时
	{
		((CButton *)GetDlgItem(IDC_CHECK3))->SetCheck(FALSE);
		HoleEdge();
	}
	// 复制下一帧信息到当前帧
	m_regiontool=6;
	Region();
}

void CRightImage_ManualRefinementDlg::UpdateMbuf()
{
	if (m_buf->height==m_image->height)
	{
		m_buf=m_image;
	} 
	else
	{
		CopyImage(m_dspbuf,m_buf,1);
		/*cvReleaseImage(&m_buf);
		CvSize dst_cvsize;
		dst_cvsize.width=(int)(m_dspbuf->width);
		dst_cvsize.height=(int)(m_dspbuf->height);
		m_buf=cvCreateImage(dst_cvsize,m_dspbuf->depth,m_dspbuf->nChannels);
		cvResize(m_dspbuf,m_buf,CV_INTER_NN);*/
	}

}

void CRightImage_ManualRefinementDlg::SetRegionFlag(int x,int y,IplImage *t1)
{
	vector<CPoint> growpnts;
	vector<CPoint> growtmp;
	CPoint point1;
	point1.x=x;
	point1.y=y;
	int w1=m_t1->width;
	int h1=m_t1->height;

	if (!t1->imageData[y*t1->widthStep+t1->nChannels*x])//是黑洞区域就将其标记为一
	{
		growpnts.push_back(point1);//仅当该点是黑洞点时才将其压入容器

		if (flag[y*w1+x]==0)//等于0说明是要选中黑洞区域
		{
			flag[y*w1+x]=1;

			CString s="*****   选中一个区域  *****";
			GetDlgItem(IDC_EDIT1)->SetWindowText(s);

			while( growpnts.size() )   //以该点为种子点选中黑洞区域所有点
			{
				int k,size = (int)growpnts.size();
				growtmp.clear();
				for( k = 0; k < size; k++ )
				{
					RegionGrow(t1,growtmp, growpnts[k],w1 , h1, flag);
					//					rgnpnts.push_back(growpnts[k]);
				}
				growpnts = growtmp;
			}
		} 
		else  if (flag[y*w1+x]==1) //不等于0说明是 取消黑洞区域
		{
			CString s="*****   取消一个区域  *****";
			GetDlgItem(IDC_EDIT1)->SetWindowText(s);

			while( growpnts.size() )   //以该点为种子点选中黑洞区域所有点
			{
				int k,size = (int)growpnts.size();
				growtmp.clear();
				for( k = 0; k < size; k++ )
				{
					CancleRegion(t1,growtmp, growpnts[k],w1 , h1, flag);
				}
				growpnts = growtmp;
			}
		}
	}
}

void CRightImage_ManualRefinementDlg::CancleRegion(IplImage *image,vector<CPoint> &growpnts, CPoint point, int w,int h,BYTE *flag)
{
CPoint nb;

	int l = point.x - 1 <  0 ? 0   : point.x - 1;
	int r = point.x + 1 >= w ? w-1 : point.x + 1;
	int t = point.y - 1 <  0 ? 0   : point.y - 1;
	int b = point.y + 1 >= h ? h-1 : point.y + 1;

	nb.x = l,nb.y = t;
	if( flag[ t * w + l ] )
	{
		CPoint tmp(l , t);
		growpnts.push_back(tmp);
		flag[ t * w + l ] = 0;
	}

	nb.x = point.x,nb.y = t;
	if( flag[ t * w + point.x ]  )
	{
		CPoint tmp(point.x , t);
		growpnts.push_back(tmp);
		flag[ t * w + point.x ] =0;
	}

	nb.x = r,nb.y = t;
	if( flag[ t * w + r ])
	{
		CPoint tmp(r , t);
		growpnts.push_back(tmp);
		flag[ t * w + r ] = 0;

	}

	///////////////////////////////////////////////////////////////

	nb.x = l,nb.y = point.y;
	if( flag[ point.y * w + l ]  )
	{
		CPoint tmp(l , point.y);
		growpnts.push_back(tmp);
		flag[ point.y * w + l ] = 0;
	}

	nb.x = r,nb.y = point.y;
	if( flag[ point.y * w + r ] )
	{
		CPoint tmp(r , point.y);
		growpnts.push_back(tmp);
		flag[ point.y * w + r ] = 0;
	}

	///////////////////////////////////////////////////////////////

	nb.x = l,nb.y = b;
	if( flag[ b * w + l ]  )
	{
		CPoint tmp(l , b);
		growpnts.push_back(tmp);
		flag[ b * w + l ] = 0;

	}

	nb.x = point.x,nb.y = b;
	if( flag[ b * w + point.x ]  )
	{
		CPoint tmp(point.x , b);
		growpnts.push_back(tmp);
		flag[ b * w + point.x ] = 0;
	}

	nb.x = r,nb.y = b;
	if( flag[ b * w + r ] )
	{
		CPoint tmp(r , b);
		growpnts.push_back(tmp);
		flag[ b * w + r ] = 0;
	}
}

void CRightImage_ManualRefinementDlg::SetDisplayPoint(int &x,int &y)
{
// 	GetDlgItem(IDC_STATIC_IMG)->GetWindowRect(m_dsprect);
// 	ScreenToClient(m_dsprect);

	x=x-m_hpos+W-m_dsprect.TopLeft().x;
	y=y-m_vpos+H-m_dsprect.TopLeft().y;
	if (x<0)
	{
		x=0;
	}
	if (x>=m_dspbuf->width)
	{
		x=m_dspbuf->width-1;
	}
	if (y<0)
	{
		y=0;
	}
	if (y>=m_dspbuf->height)
	{
		y=m_dspbuf->height-1;
	}

}
void CRightImage_ManualRefinementDlg::SetPicturePoint(int &x,int &y,CPoint &point)
{
//	int H,W;//屏幕显示的图片左上角坐标
//	int h,w;//有效区长宽
	GetDlgItem(IDC_STATIC_IMG)->GetWindowRect(m_dsprect);
	ScreenToClient(m_dsprect);

	if (m_image->height<m_dsprect.Height())
	{
		if (m_image->width<m_dsprect.Width())
		{
			H=m_dsprect.TopLeft().y+(m_dsprect.Height()-m_image->height)/2;
			W=m_dsprect.TopLeft().x+(m_dsprect.Width()-m_image->width)/2;

			h=m_image->height;
			w=m_image->width;
			x=point.x-W;
			y=point.y-H;
		} 
		else
		{
			H=m_dsprect.TopLeft().y+(m_dsprect.Height()-m_image->height)/2;
			W=m_dsprect.TopLeft().x;

			h=m_image->height;
			w=m_dsprect.Width();
			x=point.x-W+m_hpos;
			y=point.y-H;
		}
	} 
	else
	{
		if (m_image->width<m_dsprect.Width())
		{
			H=m_dsprect.TopLeft().y;
			W=m_dsprect.TopLeft().x+(m_dsprect.Width()-m_image->width)/2;

			h=m_dsprect.Height();
			w=m_image->width;

			x=point.x-W;
			y=point.y-H+m_vpos;
		} 
		else
		{
			H=m_dsprect.TopLeft().y;
			W=m_dsprect.TopLeft().x;

			h=m_dsprect.Height();
			w=m_dsprect.Width();

			x=point.x-W+m_hpos;
			y=point.y-H+m_vpos;
		}
	}

}


inline void CRightImage_ManualRefinementDlg::OnMouseMove(UINT nFlags, CPoint point)
{
	CString s,s1;
	s1.Format("%d",point.x);
	s="坐标（"+s1+"，";
	s1.Format("%d",point.y);
	s+=s1+"）";

	GetDlgItem(IDC_EDIT2)->SetWindowText(s);

	if (m_image)
	{
		int x=0,y=0;
		SetPicturePoint(x,y,point);
		if (m_NewPicture==1)
		{
			x=point.x;
			y=point.y;
		}

		s1.Format("%d",x);
		s="图片中坐标（"+s1+"，";
		s1.Format("%d",y);
		s+=s1+"）";
		GetDlgItem(IDC_EDIT3)->SetWindowText(s);
	}
	
	if ((m_mouseSelect==TRUE)&&m_image)
	{
		int tmp1=m_image->width-m_dsprect.Width();
		int tmp2=m_image->height-m_dsprect.Height();
		if((nFlags==MK_LBUTTON)&&(tmp1>0)&&(tmp2>0))
		{
			m_hpos-=point.x-m_point.x;
			m_vpos-=point.y-m_point.y;
			if (m_hpos<0)
			{
				m_hpos=0;
		//		m_SBar.SetFocus();
			}
			else if (m_hpos>tmp1)
			{
				m_hpos=tmp1;
	//			m_SBar.SetFocus();
			}

			if (m_vpos<0)
			{
				m_vpos=0;
	//			m_SvBar.SetFocus();
			}
			else if (m_vpos>tmp2)
			{
				m_vpos=tmp2;
	//			m_SvBar.SetFocus();
			}

			int position=100*m_hpos/tmp1;
			m_SBar.SetScrollPos(position);
			position=100*m_vpos/tmp2;
			m_SvBar.SetScrollPos(position);//根据position 的值来设定滑动块的位置
			UpdateData(FALSE);

			GetImageDisplayData(m_dspbuf,m_image,m_hpos,m_vpos);
			Invalidate(FALSE);

		}

		m_point=point;
	}

	if ((m_Tools==SQUARE)||(m_Tools==CIRCLE))
	{
		m_point=point;
	}
	
	if (m_mouseSelect==FALSE)
	{
		if(nFlags==MK_LBUTTON)
		{
			OnLButtonDown(nFlags, point);
		}

		if(nFlags==MK_RBUTTON)
		{
			if ((m_Tools==SQUARE)||(m_Tools==CIRCLE))
			{
				OnRButtonDown(nFlags, point);

				
			}
			if ((m_Tools==BOTH)||(m_Tools==HOLE_PIXEL)||(m_Tools==NORMAL_PIXEL))
			{
				DrawLine(m_t1, m_point, point);
				m_point=point;
			}
		}
	}
	

	CDialog::OnMouseMove(nFlags, point);
}


void CRightImage_ManualRefinementDlg::BrushPoint(CPoint point)
{
	overFlag=0;

	int x=0,y=0;

	SetPicturePoint(x,y,point);

	if (x<0)
	{
		x=0;
	}
	if (x>(m_t1->width-1))
	{
		x=(m_t1->width-1);
	}
	if (y<0)
	{
		y=0;
	}
	if (y>(m_h-1))
	{
		y=(m_h-1);
	}

	Brushflag[y*m_w+x]=1;  //将所画的点标记

	if (x<m_HoleEdgePoint[1])
	{
		m_HoleEdgePoint[1]=x;
	}
	if (y<m_HoleEdgePoint[0])
	{
		m_HoleEdgePoint[0]=y;
	}

	if (m_HoleEdgePoint[2]<x)
	{
		m_HoleEdgePoint[2]=x;
	}

	if (m_HoleEdgePoint[3]<y)
	{
		m_HoleEdgePoint[3]=y;
	}

	int i=0,j=0;
	for(i=x-1;i<(x+2);i++)
		for (j=y-1;j<(y+2);j++)
		{
			if (i>0&&i<m_t1->width&&j>0&&j<m_t1->height)
			{
				m_t1->imageData[ j* m_t1->widthStep + m_t1->nChannels * i+ 2 ] = 0;
				m_t1->imageData[ j* m_t1->widthStep + m_t1->nChannels * i+ 1 ] = 0;
				m_t1->imageData[ j* m_t1->widthStep + m_t1->nChannels * i+ 0 ] = (BYTE)255;
			}
		}

// 	GetImageDisplayData(m_dspbuf,m_t1,m_hpos,m_vpos);
// 	Invalidate(FALSE);

}
void CRightImage_ManualRefinementDlg::OnRButtonUp(UINT nFlags, CPoint point)	//记录brush右键释放的坐标
{
 
		m_ButtonUpFlag=1;

		m_ButtonUp=point;

		m_pnt.clear();

		preBrushRightflag=BrushRightflag;

		if (m_Tools==REGION)
		{
			m_HoleEdgePoint[0]=m_h;
			m_HoleEdgePoint[1]=m_w;
			m_HoleEdgePoint[2]=0;
			m_HoleEdgePoint[3]=0;

			int i=0,j=0;
			for(j=0;j<m_h;j++)
				for(i=0;i<m_w;i++)
				{
					if (flag[ j * m_w +i ])
					{
						if (i<m_HoleEdgePoint[1])
						{
							m_HoleEdgePoint[1]=i;
						}
						if (j<m_HoleEdgePoint[0])
						{
							m_HoleEdgePoint[0]=j;
						}
						if (m_HoleEdgePoint[3]<j)
						{
							m_HoleEdgePoint[3]=j;
						}
						if (m_HoleEdgePoint[2]<i)
						{
							m_HoleEdgePoint[2]=i;
						}
					}
					
				}
			
		}

		if ((m_Tools==BOTH)||(m_Tools==HOLE_PIXEL)||(m_Tools==NORMAL_PIXEL))
		{
			if (m_ButtonDown!=m_ButtonUp)//只点一个点不算
			{
				DrawLine(m_t1, m_ButtonDown, m_ButtonUp);

				GetAllBrushPoint();

				UpdateMt1();
				m_regionflag=1;//画完brush区域再立即标记区域
			}
			else
			{
				m_regionflag=0;
				
				UpdateMt1();
				GetImageDisplayData(m_dspbuf,m_t1,m_hpos,m_vpos);
			}
			
			Invalidate(FALSE);

		}

		if ((m_Tools==HOLE_PIXEL)||(m_Tools==NORMAL_PIXEL))    //只复制brush区域内的黑洞部分，在hole pixel和 normal pixel工具下将所有的黑洞点标记
		{
			delete []flag;

			flag=new BYTE[m_w*m_h];

			memset(flag, 0, m_w * m_h* sizeof(BYTE));

			CString p,text;
			int i7 = m_lbox.GetCurSel();
			m_lbox.GetText(i7, text);
			if ((m_name_begin=="ProcImg")&&(m_name_end=="_RightImageFilled.jpg"))
			{
				p=m_path+"\\"+m_name_begin+text+"_RightMask.bmp";
			}
			else if ((m_name_begin=="origin")||((m_name_begin=="ProcImg")&&(m_name_end==".jpg")))
			{
				p=m_path+"\\"+"refinement"+text+".bmp";
			}
			else
			{
				p=m_path+"\\"+text.Left(text.GetLength()-3)+"bmp";
			}
// 			CString p;
// 			int l=m_pathname.GetLength();
// 			l-=15;
// 			p=m_pathname.Left(l);
// 			p+="Mask.bmp";
			CString name=m_name.Left(11);
			if (name=="Background_")
			{
				p=m_path+"\\"+"Background_"+text+"_Filled.bmp";
			}

			IplImage* temp = cvLoadImage(p,CV_LOAD_IMAGE_COLOR);

			if (temp)
			{
				int i=0,j=0;
				for(i=0;i<m_w;i++)
					for (j=0;j<m_h;j++)
					{
						BYTE g = temp->imageData[j*temp->widthStep+temp->nChannels*i];

						if (g==0)
						{
							flag[j*m_image->width+i]=1;
						}
					}
			} 
			else
			{
				MessageBox("没有对应的Mask文件！！");
			}
			

			cvReleaseImage(&temp);
		}
		
		
		m_ButtonUp=0;
		m_ButtonDown=0;

		BrushRightflag=0;




//		HoleEdge();

		CDialog::OnRButtonUp(nFlags, point);
}

/*vector<CPoint>*/void CRightImage_ManualRefinementDlg::DrawLine(IplImage *image, CPoint begin, CPoint end /*, COLORREF rgb*/)
{
	int i,j;
	CPoint /*pnt1,pnt2,*/tmp;
//	vector<CPoint> points;

	int dx = end.x - begin.x;
	int dy = end.y - begin.y;

	int w = image->width, h = image->height;

	if( begin.x == end.x )
	{
		if( dy > 0 )
		{
			for(i = begin.y; i <= end.y; i++)
			{
//				if( begin.x < 0 || begin.x >= w || i < 0 || i >= h )
//					continue;

				if( image )

//					SetPixel(image, begin.x, i, rgb);
				tmp.x = begin.x, tmp.y = i;
//				points.push_back(tmp);

				BrushPoint(tmp);
			}
		}
		else
		{
			for(i = begin.y; i >= end.y; i--)
			{
//				if( begin.x < 0 || begin.x >= w || i < 0 || i >= h )
//					continue;

				if( image )
//					SetPixel(image, begin.x, i, rgb);
				tmp.x = begin.x, tmp.y = i;
//				points.push_back(tmp);
				BrushPoint(tmp);
			}
		}
	}
	else
	{
		if( abs(dx) >= abs(dy) )
		{
			double k = (double)dy / (double)dx;

			if( dx >= 0 )
			{
				for(j = begin.x; j <= end.x; j++)
				{
					tmp.x = j;
					tmp.y = (int)( begin.y + (j-begin.x)*k + 0.5 );

//					if( tmp.x < 0 || tmp.x >= w || tmp.y < 0 || tmp.y >= h )
//						continue;

					if( image )
						BrushPoint(tmp);
//						SetPixel(image, tmp.x, tmp.y, rgb);
//					points.push_back(tmp);
				}
			}
			else
			{
				for(j = begin.x; j >= end.x; j--)
				{
					tmp.x = j;
					tmp.y = (int)( begin.y + (j-begin.x)*k + 0.5 );

//					if( tmp.x < 0 || tmp.x >= w || tmp.y < 0 || tmp.y >= h )
//						continue;

					if( image )
						BrushPoint(tmp);
//						SetPixel(image, tmp.x, tmp.y, rgb);
//					points.push_back(tmp);
				}
			}
		}
		else
		{
			double k = (double)dx / (double)dy;
//			int x1,x2,y1,y2;

			if( dy >= 0 )
			{
				for(i = begin.y; i <= end.y; i++)
				{
					tmp.x = (int)( begin.x + (i-begin.y)*k + 0.5 );
					tmp.y = i;

//					if( tmp.x < 0 || tmp.x >= w || tmp.y < 0 || tmp.y >= h )
//						continue;

					if( image )
						BrushPoint(tmp);
//						SetPixel(image, tmp.x, tmp.y, rgb);
//					points.push_back(tmp);
				}
			}
			else
			{
				for(i = begin.y; i >= end.y; i--)
				{
					tmp.x = (int)( begin.x + (i-begin.y)*k + 0.5 );
					tmp.y = i;

//					if( tmp.x < 0 || tmp.x >= w || tmp.y < 0 || tmp.y >= h )
//						continue;

					if( image )
						BrushPoint(tmp);
//						SetPixel(image, tmp.x, tmp.y, rgb);
//					points.push_back(tmp);
				}
			}
		}
	}

//	return points;
GetImageDisplayData(m_dspbuf,m_t1,m_hpos,m_vpos);
Invalidate(FALSE);
}


void CRightImage_ManualRefinementDlg::GetAllBrushPoint()//由brush画出的边界线将区域内的所有点标记为1
{
	int i=0,j=0,k=0;	

	/*for(j=m_HoleEdgePoint[0];j<=m_HoleEdgePoint[3];j++)
	{
		for(i=m_HoleEdgePoint[1];i<=m_HoleEdgePoint[2];i++)
		{
			k=1;

			if ((Brushflag[j*m_image->width+i]==1)&&(Brushflag[j*m_image->width+i-1]==0))
			{
				while ((Brushflag[j*m_image->width+i+k]==0)||(Brushflag[j*m_image->width+i+k]==Brushflag[j*m_image->width+i+k-1]))
				{
					k++;

					if (k>(m_HoleEdgePoint[2]-i))
					{
						k=1;
						break;
					}
				}
			}
			while (k>1)
			{
				Brushflag[j*m_image->width+i+k]=1;
				k--;
			}
		}
	}*/

	i=m_HoleEdgePoint[1];
	for(j=m_HoleEdgePoint[0];j<=m_HoleEdgePoint[3];j++)
	{
		while (Brushflag[j*m_image->width+i+k]!=1)
		{
			Brushflag[j*m_image->width+i+k]=2;
			k++;
		}
		k=0;
	}

	i=m_HoleEdgePoint[2];
	k=0;
	for(j=m_HoleEdgePoint[0];j<=m_HoleEdgePoint[3];j++)
	{
		while (Brushflag[j*m_image->width+i-k]!=1)
		{
			Brushflag[j*m_image->width+i-k]=2;
			k++;
		}
		k=0;
	}

	j=m_HoleEdgePoint[0];
	k=0;
	for(i=m_HoleEdgePoint[1];i<=m_HoleEdgePoint[2];i++)
	{
		while (Brushflag[(j+k)*m_image->width+i]!=1)
		{
			Brushflag[(j+k)*m_image->width+i]=2;
			k++;
		}
		k=0;
	}

	j=m_HoleEdgePoint[3];
	k=0;
	for(i=m_HoleEdgePoint[1];i<=m_HoleEdgePoint[2];i++)
	{
		while (Brushflag[(j-k)*m_image->width+i]!=1)
		{
			Brushflag[(j-k)*m_image->width+i]=2;
			k++;
		}
		k=0;
	}

	for(j=m_HoleEdgePoint[0];j<=m_HoleEdgePoint[3];j++)
		for(i=m_HoleEdgePoint[1];i<=m_HoleEdgePoint[2];i++)
		{
			if (Brushflag[j*m_image->width+i]!=2)
			{
				Brushflag[j*m_image->width+i]=1;
			}
		}

		for(j=m_HoleEdgePoint[0];j<=m_HoleEdgePoint[3];j++)
			for(i=m_HoleEdgePoint[1];i<=m_HoleEdgePoint[2];i++)
			{
				if (Brushflag[j*m_image->width+i]!=1)
				{
					Brushflag[j*m_image->width+i]=0;
				}
			}
}

void CRightImage_ManualRefinementDlg::OnCopyAllforward()//区域工具之将黑洞部分全部从前一帧copy
{
	if (BST_CHECKED==((CButton*)GetDlgItem(IDC_CHECK3))->GetCheck())// 选中时
	{
		((CButton *)GetDlgItem(IDC_CHECK3))->SetCheck(FALSE);
	}
	if (m_image)
	{
		if (m_image->height>m_h)
		{
			MessageBox("请先将图片恢复至原始尺寸！！");
		}
		else
		{
			CString text;
//			text=m_path+"\\";

			CString tmp_maskPath,tmp_CurrentimageMaskPath;
//			CString tmp_CurrentimageMaskPath=m_path+"\\"+imagename[m_preNum+1];//当前图片对应的mask；
//			tmp_CurrentimageMaskPath=tmp_CurrentimageMaskPath.Left(tmp_CurrentimageMaskPath.GetLength()-15);

			if (m_preNum>=0)//listbox第一个编号是0
			{
//				text+=imagename[m_preNum];
//				tmp_maskPath=text.Left(text.GetLength()-15);
//				CopyAllHole(text,tmp_maskPath,tmp_CurrentimageMaskPath);

				m_lbox.GetText((m_preNum), text);//前一帧序号
				tmp_maskPath=text;
				m_lbox.GetText((m_preNum+1), tmp_CurrentimageMaskPath);//当前帧

				CopyAllHole(text,tmp_maskPath,tmp_CurrentimageMaskPath);
			}
			else
			{
				MessageBox("这是第一帧，不能从前一帧复制！！");
			}
			
		}
		
	}
	else 
	{
		MessageBox("先打开一张图片！！");
	}
	
}

void CRightImage_ManualRefinementDlg::OnCopyAllback()//区域工具之将黑洞部分全部从后一帧copy
{
	if (BST_CHECKED==((CButton*)GetDlgItem(IDC_CHECK3))->GetCheck())// 选中时
	{
		((CButton *)GetDlgItem(IDC_CHECK3))->SetCheck(FALSE);
	}

	if (m_image)
	{
		if (m_image->height>m_h)
		{
			MessageBox("请先将图片恢复至原始尺寸！！");
		}
		else
		{
			CString text;
//			text=m_path+"\\";

			CString tmp_maskPath,tmp_CurrentimageMaskPath;
//			CString tmp_CurrentimageMaskPath=m_path+"\\"+imagename[m_preNum+1];//当前图片对应的mask；
//			tmp_CurrentimageMaskPath=tmp_CurrentimageMaskPath.Left(tmp_CurrentimageMaskPath.GetLength()-15);

			if ((m_preNum+2)<(int)imagename.size())
			{
//				text+=imagename[m_preNum+2];
				//////////////////////////////////////////////////////////////////////////////////////// 读取MaskManualOutput.bmp的路径
//				tmp_maskPath=text.Left(text.GetLength()-15);//后一帧的mask
				m_lbox.GetText((m_preNum+2), text);//后一帧序号
				tmp_maskPath=text;
				m_lbox.GetText((m_preNum+1), tmp_CurrentimageMaskPath);//当前帧

				CopyAllHole(text,tmp_maskPath,tmp_CurrentimageMaskPath);
			}
			else
			{
				MessageBox("这是最后一帧，不能从后一帧复制！！");
			}
			
		}
		
	}
	else 
	{
		MessageBox("先打开一张图片！！");
	}
	
}
void CRightImage_ManualRefinementDlg::CopyAllHole(CString &text,CString &tmp_maskPath,CString &tmp_CurrentimageMaskPath)
{
	int tmp_num=0;
//	tmp_CurrentimageMaskPath+="Mask.bmp";

//	CString text;
// 	int i7 = m_lbox.GetCurSel();
// 	m_lbox.GetText(i7, text);
	CString name=m_name.Left(11);
	
	if ((m_name_begin=="ProcImg")&&(m_name_end=="_RightImageFilled.jpg"))
	{
		text=m_path+"\\"+m_name_begin+text+m_name_end;
		tmp_maskPath=m_path+"\\"+m_name_begin+tmp_maskPath+"_RightMask.bmp";

		tmp_CurrentimageMaskPath=m_path+"\\"+m_name_begin+tmp_CurrentimageMaskPath+"_RightMask.bmp";
	}
	else if ((m_name_begin=="origin")||((m_name_begin=="ProcImg")&&(m_name_end==".jpg")))
	{
		text=m_path+"\\"+m_name_begin+text+m_name_end;
		tmp_maskPath=m_path+"\\"+"refinement"+tmp_maskPath+".bmp";

		if (name=="Background_")
		{
			tmp_CurrentimageMaskPath=m_path+"\\"+"Background_"+tmp_CurrentimageMaskPath+"_Filled.bmp";
		}
		else
		{
			tmp_CurrentimageMaskPath=m_path+"\\"+"refinement"+tmp_CurrentimageMaskPath+".bmp";
		}
		
	}
	else
	{
		text=m_path+"\\"+text;
		tmp_maskPath=m_path+"\\"+tmp_maskPath.Left(tmp_maskPath.GetLength()-3)+"bmp";;
		
		tmp_CurrentimageMaskPath=m_path+"\\"+tmp_CurrentimageMaskPath.Left(tmp_CurrentimageMaskPath.GetLength()-3)+"bmp";
	}

	IplImage* temp = cvLoadImage(text,CV_LOAD_IMAGE_COLOR);//前一张或者后一张
	IplImage* tempCurrent = cvLoadImage(tmp_CurrentimageMaskPath,CV_LOAD_IMAGE_COLOR);

//	tmp_maskPath+="Mask.bmp";////////////// 读取MaskManualOutput.bmp的路径
	IplImage* tmp_Mask = cvLoadImage(tmp_maskPath,CV_LOAD_IMAGE_COLOR);////////////// 读取Mask.bmp文件

	if (tmp_Mask)
	{
		int i=0,j=0;
		for(j=0;j<m_image->height;j++)
			for (i=0;i<m_image->width;i++)
			{
				BYTE g=tempCurrent->imageData[j*tempCurrent->widthStep+tempCurrent->nChannels*i];//判断黑洞点
				if (!g||((name=="Background_")&&((i==m_image->width/3)||(i==2*m_image->width/3)||(j==m_image->height/2))))
				{
					tmp_num++;
					SetImageData(m_image,j,i,temp,j,i);
					/////////////////////////////////////////////////////////////////////////////////////////   对 mask 操作
					if ((m_name_begin=="ProcImg")&&(m_name_end=="_RightImageFilled.jpg"))
					{
						SetImageData(m_MaskImage,j,i,tmp_Mask,j,i);
					}
					
				}

			}
			GetImageDisplayData(m_dspbuf,m_image,m_hpos,m_vpos);
			Invalidate(FALSE);
	} 
	else
	{
		MessageBox("没有Mask文件！");
	}
	cvReleaseImage(&temp);
	cvReleaseImage(&tmp_Mask);

	if (tmp_num>0)
	{
		num.push_back(tmp_num);
		int i=(int)num.size();
		CString s=" ";
		s.Format("%d", i);

		s="       已经操作 "+s+" 步";
		GetDlgItem(IDC_EDIT1)->SetWindowText(s);
		SaveFlag=FALSE;
		Message(i);
	}
	
}
void CRightImage_ManualRefinementDlg::RegionTool()
{

	int tmp_num=0;

	BYTE *tmp_flag;
	BYTE *tmp_flag1;

		if ((m_Tools==HOLE_PIXEL)||(m_Tools==NORMAL_PIXEL))
		{
			tmp_flag=flag;
			tmp_flag1=flag;

			flag=Brushflag;
		}

		CString text;
		text=m_path+"\\";

		CString tmp_maskPath;

		if (m_regiontool==6)
			{
				if ((m_preNum+2)<(int)imagename.size())
				{
					text+=imagename[m_preNum+2];
					//////////////////////////////////////////////////////////////////////////////////////// 读取MaskManualOutput.bmp的路径
					tmp_maskPath=text.Left(text.GetLength()-15);
				}
				
			}
		else 
		{	
			if (m_preNum>=0)//listbox第一个编号是0
			{
				text+=imagename[m_preNum];
				tmp_maskPath=text.Left(text.GetLength()-15);
			}
		}
		
		IplImage* temp = cvLoadImage(text,CV_LOAD_IMAGE_COLOR);

		tmp_maskPath+="MaskManualOutput.bmp";////////////// 读取MaskManualOutput.bmp的路径
		IplImage* tmp_Mask = cvLoadImage(tmp_maskPath,CV_LOAD_IMAGE_COLOR);////////////// 读取MaskManualOutput.bmp文件
		if (!tmp_Mask)////////////// 如果没有MaskManualOutput.bmp文件，则改加载Mask.bmp文件
		{
			tmp_maskPath=text.Left(text.GetLength()-15);
			tmp_maskPath+="Mask.bmp";
			tmp_Mask = cvLoadImage(tmp_maskPath,CV_LOAD_IMAGE_COLOR);
		}
	
		int i=0,j=0;
		for (j=0;j<m_image->height;j++)
		{
			for(i=1;i<m_image->width;i++)//  i 是从1开始的
				{
					int k=1;
//					k=0;
					if (((flag[j*m_image->width+i-1]==0)&&(flag[j*m_image->width+i]==1))||(((i-1)==0)&&(flag[j*m_image->width+i]==1))/*||(Brushflag[j*m_image->width+i-1]==0)&&(Brushflag[j*m_image->width+i]==2)*/)
					{
						
						while ((flag[j*m_image->width+i+k]==1))
						{
							k++;
						}

						int l;
						for( l=0;l<=(k+1);l++)
						{
							if (((m_Tools==HOLE_PIXEL)&&(tmp_flag[j*m_image->width+i+l]==1))||((m_Tools==NORMAL_PIXEL)&&(tmp_flag[j*m_image->width+i+l]==0))||(m_Tools==REGION)||(m_Tools==BOTH))
							{
								tmp_num++;
								//非镜像  右边
								if (m_regiontool==3)
								{
									SetImageData(m_image,j,(i+l-1),m_image,j,( i+k+l+1));
/////////////////////////////////////////////////////////////////////////////////////////   对 mask 操作
									SetImageData(m_MaskImage,j,(i+l-1),m_MaskImage,j,( i+k+l+1));
								}
//镜像   右边
								if (m_regiontool==2)
								{
									SetImageData(m_image,j,(i+l-1),m_image,j,( i+k+k-l+2));
/////////////////////////////////////////////////////////////////////////////////////////   对 mask 操作
									SetImageData(m_MaskImage,j,(i+l-1),m_MaskImage,j,( i+k+k-l+2));
								}
//镜像   左边
								if (m_regiontool==0)
								{
									SetImageData(m_image,j,(i+l-1),m_image,j,( i-l-2));
/////////////////////////////////////////////////////////////////////////////////////////   对 mask 操作
									SetImageData(m_MaskImage,j,(i+l-1),m_MaskImage,j,( i-l-2));
								}
							
//非镜像  左边
								if (m_regiontool==1)
								{
									SetImageData(m_image,j,(i+l-1),m_image,j,( i-k+l-3));
/////////////////////////////////////////////////////////////////////////////////////////   对 mask 操作
									SetImageData(m_MaskImage,j,(i+l-1),m_MaskImage,j,( i-k+l-3));
								}
//非镜像  both
							/*m_image->imageData[ j* m_image->widthStep + m_image->nChannels *( i+l)+ 2 ] =(m_image->imageData[ j* m_image->widthStep + m_image->nChannels *( i-k+l)+ 2 ]+m_image->imageData[ j* m_image->widthStep + m_image->nChannels *( i+k+l)+ 2 ])/2;
							m_image->imageData[ j* m_image->widthStep + m_image->nChannels * (i+ l)+1 ] = (m_image->imageData[ j* m_image->widthStep + m_image->nChannels *( i-k+l)+ 1 ]+m_image->imageData[ j* m_image->widthStep + m_image->nChannels *( i+k+l)+ 1 ])/2;
							m_image->imageData[ j* m_image->widthStep + m_image->nChannels * (i+l)+ 0 ] = (m_image->imageData[ j* m_image->widthStep + m_image->nChannels *( i-k+l)+ 0 ]+m_image->imageData[ j* m_image->widthStep + m_image->nChannels *( i+k+l)+ 0 ])/2;
*/
//镜像  both
								if (m_regiontool==4)
								{
									if (j>=0&&j<m_image->height&&( i+l-1)>=0&&( i+l-1)<m_image->width&&( i-l-2)>=0&&( i-l-2)<m_image->width&&(i+k+k-l+2)>=0&&( i+k+k-l+2)<m_image->width)
									{
										SaveVector(m_image,j,( i+l-1));//先保存点，在改变
										BYTE d4=m_image->imageData[ j* m_image->widthStep + m_image->nChannels *( i-l-2)+ 2 ];
										BYTE d5=m_image->imageData[ j* m_image->widthStep + m_image->nChannels *( i+k+k-l+2)+ 2 ];
										int d1=d4+d5;
										d4=m_image->imageData[ j* m_image->widthStep + m_image->nChannels *( i-l-2)+ 1 ];
										d5=m_image->imageData[ j* m_image->widthStep + m_image->nChannels *( i+k+k-l+2)+ 1 ];

										int d2=d4+d5;
										d4=m_image->imageData[ j* m_image->widthStep + m_image->nChannels *( i-l-2)+ 1 ];
										d5=m_image->imageData[ j* m_image->widthStep + m_image->nChannels *( i+k+k-l+2)+ 1 ];

										int d3=d4+d5;

										m_image->imageData[ j* m_image->widthStep + m_image->nChannels *( i+l-1)+ 2 ] =d1/2;
										m_image->imageData[ j* m_image->widthStep + m_image->nChannels * (i+l-1)+1 ] =d2/2;
										m_image->imageData[ j* m_image->widthStep + m_image->nChannels * (i+l-1)+ 0 ] =d3/2;


										/////////////////////////////////////////////////////////////////////////////////////////   对 mask 操作
										if ((m_name_begin=="ProcImg")&&(m_name_end=="_RightImageFilled.jpg"))
										{
											SaveVector(m_MaskImage,j,( i+l-1));
										}
										
										if (m_MaskImage)
										{
											d4=m_MaskImage->imageData[ j* m_MaskImage->widthStep + m_MaskImage->nChannels *( i-l-2)+ 2 ];
											d5=m_MaskImage->imageData[ j* m_MaskImage->widthStep + m_MaskImage->nChannels *( i+k+k-l+2)+ 2 ];
											d1=d4+d5;
											d4=m_MaskImage->imageData[ j* m_MaskImage->widthStep + m_MaskImage->nChannels *( i-l-2)+ 1 ];
											d5=m_MaskImage->imageData[ j* m_MaskImage->widthStep + m_MaskImage->nChannels *( i+k+k-l+2)+ 1 ];

											d2=d4+d5;
											d4=m_MaskImage->imageData[ j* m_MaskImage->widthStep + m_MaskImage->nChannels *( i-l-2)+ 1 ];
											d5=m_MaskImage->imageData[ j* m_MaskImage->widthStep + m_MaskImage->nChannels *( i+k+k-l+2)+ 1 ];

											d3=d4+d5;

											m_MaskImage->imageData[ j* m_MaskImage->widthStep + m_MaskImage->nChannels *( i+l-1)+ 2 ] =d1/2;
											m_MaskImage->imageData[ j* m_MaskImage->widthStep + m_MaskImage->nChannels * (i+l-1)+1 ] =d2/2;
											m_MaskImage->imageData[ j* m_MaskImage->widthStep + m_MaskImage->nChannels * (i+l-1)+ 0 ] =d3/2;
										}
									}
									
									

									
								}	

								if (((m_regiontool==5)&&(m_preNum>=0))||((m_regiontool==6)&&((m_preNum+2)<(int)imagename.size())))
								{
									SetImageData(m_image,j,(i+l-1),temp,j,( i+l-1));
/////////////////////////////////////////////////////////////////////////////////////////   对 mask 操作
									if ((m_name_begin=="ProcImg")&&(m_name_end=="_RightImageFilled.jpg"))
									{
										SetImageData(m_MaskImage,j,(i+l-1),tmp_Mask,j,( i+l-1));
									}
									
									
								}
								else
								{
									tmp_num=-1;
									MessageBox("不能对第一帧从前一帧复制，不能对最后一帧从后一帧复制！！");
									break;
								}
							}
						}
					}
					if (tmp_num==-1)
					{
						break;
					}
				}
				if (tmp_num==-1)
				{
					break;
				}
			}

				cvReleaseImage(&temp);
				cvReleaseImage(&tmp_Mask);

				UpdateMt1();

				if ((m_Tools==HOLE_PIXEL)||(m_Tools==NORMAL_PIXEL))
				{
					flag=tmp_flag1;
				}

				if (tmp_num>0)
				{
					num.push_back(tmp_num);
					int i=(int)num.size();
					CString s=" ";
					s.Format("%d", i);

					s="       已经操作 "+s+" 步";
					GetDlgItem(IDC_EDIT1)->SetWindowText(s);
					SaveFlag=FALSE;
					Message(i);
				}
}



void CRightImage_ManualRefinementDlg::UpdateMt1()
{
	CopyImage(m_image,m_t1,1);
	/*cvReleaseImage(&m_t1);
	CvSize dst_cvsize;
	dst_cvsize.width=(int)(m_image->width);
	dst_cvsize.height=(int)(m_image->height);
	m_t1=cvCreateImage(dst_cvsize,m_image->depth,m_image->nChannels);
	cvResize(m_image,m_t1,CV_INTER_NN);*/
}

void CRightImage_ManualRefinementDlg::CloseNewPicture()
{
	((CButton*)GetDlgItem(IDC_CHECK1))->SetCheck(FALSE);
	OnNewPicture();
	if(BST_CHECKED==((CButton*)GetDlgItem(IDC_CHECK6))->GetCheck())
	{
		GetDlgItem(IDC_CHECK6)->SetFocus();

	}
	if(BST_CHECKED==((CButton*)GetDlgItem(IDC_CHECK5))->GetCheck())
	{
		GetDlgItem(IDC_CHECK5)->SetFocus();

	}
	

}
void CRightImage_ManualRefinementDlg::OnNewPicture()
{
	if (m_image)
	{
		if(BST_CHECKED==((CButton*)GetDlgItem(IDC_CHECK1))->GetCheck())
		{
			m_NewPicture=1;
		}
		else if (BST_UNCHECKED==((CButton*)GetDlgItem(IDC_CHECK1))->GetCheck())
		{
			m_NewPicture=0;
			if (Pnewdailog)
			{
				Pnewdailog->DestroyWindow();
				//			m_lbox.SetCurSel(m_preNum+1);
			}
		}
	} 
	else
	{
		((CButton *)GetDlgItem(IDC_CHECK1))->SetCheck(FALSE);
		MessageBox("请重新选择图片！！");
	}
	
}

void CRightImage_ManualRefinementDlg::SetListNum()
{
	m_lbox.SetCurSel(m_preNum+1);
}

void CRightImage_ManualRefinementDlg::OnBigORLittle()
{
	if(BST_CHECKED==((CButton*)GetDlgItem(IDC_CHECK2))->GetCheck())
	{
		m_ChangeSize=1;
	}
	else if (BST_UNCHECKED==((CButton*)GetDlgItem(IDC_CHECK2))->GetCheck())
	{
		m_ChangeSize=0;
	}
}

void CRightImage_ManualRefinementDlg::HoleEdge()
{
	if (m_image)
	{
		if (BST_CHECKED==((CButton*)GetDlgItem(IDC_CHECK3))->GetCheck())// 选中时
			{
				if (m_image->height!=m_h)
				{
					MessageBox("hole edge 只能用在原始尺寸大小图片！！");
					((CButton *)GetDlgItem(IDC_CHECK3))->SetCheck(FALSE);
				} 
				else
				{
					m_holeedgeFlag=1;

				//保存hole edge 前的dspimage，为在hole edge 下取小方块而不会取中边界
					CopyImage(m_buf,m_buf1,1);
					Edge();
				}
				
			} 
			else if (BST_UNCHECKED==((CButton*)GetDlgItem(IDC_CHECK3))->GetCheck()) //未选中
			{
				m_holeedgeFlag=0;
				((CButton *)GetDlgItem(IDC_CHECK3))->SetCheck(FALSE);
				m_holeedgeFlag=0;

				GetImageDisplayData(m_dspbuf,m_image,m_hpos,m_vpos);
				Invalidate(FALSE);

				UpdateMbuf();

			}
				
	} 
	else
	{
		MessageBox("请重新选择图片！！！");
		((CButton *)GetDlgItem(IDC_CHECK3))->SetCheck(FALSE);

	}
}

void CRightImage_ManualRefinementDlg::OnShowEdge()
{
	if(BST_CHECKED==((CButton*)GetDlgItem(IDC_CHECK4))->GetCheck())
	{
		m_ShowEdge=1;
	}
	else if (BST_UNCHECKED==((CButton*)GetDlgItem(IDC_CHECK4))->GetCheck())
	{
		m_ShowEdge=0;
	}
}

/*
void CRightImage_ManualRefinementDlg::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	CDialog::OnKeyDown(nChar, nRepCnt, nFlags);
}*/



void CRightImage_ManualRefinementDlg::OnOpendir()  //与按钮打开目录功能相同
{
	WriteDir();//保存上一次目录
	OldDirFlag=FALSE;
	((CButton *)GetDlgItem(IDC_RADIO1))->SetCheck(TRUE);
	((CButton *)GetDlgItem(IDC_RADIO2))->SetCheck(FALSE);
	m_lbox.ResetContent();
	CString s=" ";
	GetDlgItem(IDC_EDIT4)->SetWindowText(s);

	ReadDir();
	if(((m_name_begin=="ProcImg")||(m_name_begin=="origin"))&&(m_name_end==".jpg"))//////////////////////////////////////////////////默认打开目录后打开第一张
	{
		int k=(int)imagename.size()/2;
		m_lbox.SetCurSel(k);
	}
	else
	{
		m_lbox.SetCurSel(0);
	}

	continueNum=-1;//第一次用完后，恢复，否则打开另一目录不会默认选择第一帧或中间帧
	OnLopen();
}

void CRightImage_ManualRefinementDlg::Save()
{
	OnSave();
}

void CRightImage_ManualRefinementDlg::OnReback()
{
	Cancle();
}

void CRightImage_ManualRefinementDlg::OnCopyforward()
{
	OnCopy();
}

void CRightImage_ManualRefinementDlg::OnCopybackward()
{
	OnBackCopy();
}

void CRightImage_ManualRefinementDlg::OnScoopSquare()
{
	GetDlgItem(IDC_CHECK6)->SetFocus();
	((CButton *)GetDlgItem(IDC_CHECK6))->SetCheck(TRUE);
	OnSelectScoop();
}

void CRightImage_ManualRefinementDlg::OnScoopCircle()
{
	GetDlgItem(IDC_CHECK5)->SetFocus();
	((CButton *)GetDlgItem(IDC_CHECK5))->SetCheck(TRUE);
	OnCircle();
}

void CRightImage_ManualRefinementDlg::OnRegionSelect()
{
	((CButton *)GetDlgItem(IDC_CHECK7))->SetCheck(TRUE);
	OnRegion();
}

void CRightImage_ManualRefinementDlg::OnBrushBoth()
{
	((CButton *)GetDlgItem(IDC_CHECK8))->SetCheck(TRUE);
	OnBrush();
}

void CRightImage_ManualRefinementDlg::OnBrushHolepixel()
{
	((CButton *)GetDlgItem(IDC_CHECK9))->SetCheck(TRUE);
	OnHolePixel();
}

void CRightImage_ManualRefinementDlg::OnBrushNormalpixel()
{
	((CButton *)GetDlgItem(IDC_CHECK10))->SetCheck(TRUE);
	OnNormalPixel();
}

void CRightImage_ManualRefinementDlg::Onright()
{
	OnBright();
}

void CRightImage_ManualRefinementDlg::OnSelectleft()
{
	OnBleft();
}

void CRightImage_ManualRefinementDlg::OnSelectboth()
{
	OnBoth();
}

void CRightImage_ManualRefinementDlg::OnSright()
{
	OnRight();
}

void CRightImage_ManualRefinementDlg::OnSleft()
{
	Onleft();
}

void CRightImage_ManualRefinementDlg::OnImageBiger()
{
	OnBig();
}

void CRightImage_ManualRefinementDlg::OnImageReduce()
{
	Reduce();
}

void CRightImage_ManualRefinementDlg::OnMousePoint()
{
	m_mouseSelect=FALSE;
	SetClassLong(this->GetSafeHwnd(),GCL_HCURSOR , (LONG)LoadCursor(NULL , IDC_ARROW));//标准箭头
}

void CRightImage_ManualRefinementDlg::OnMouseHand()
{
	m_mouseSelect=TRUE;
	SetClassLong(this->GetSafeHwnd(),GCL_HCURSOR , (LONG)LoadCursor(AfxGetInstanceHandle() , MAKEINTRESOURCE(IDC_CURSOR1)));
}



void CRightImage_ManualRefinementDlg::OnTools()
{
	
//	PTool->ShowWindow(SW_SHOWNA);
}


BOOL CRightImage_ManualRefinementDlg::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Add your specialized code here and/or call the base class
	if(WM_RBUTTONDOWN==pMsg->message)
		if((pMsg->hwnd==m_lbox.m_hWnd)&&(m_image))
		{
			DWORD dwPos=GetMessagePos();
			CPoint point(LOWORD(dwPos),HIWORD(dwPos));
			CMenu menu;
			VERIFY(menu.LoadMenu(IDR_MENU2));
			CMenu *popup=menu.GetSubMenu(0);
			ASSERT(popup!=NULL);
			popup->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,point.x,point.y,this);
			
		}
		return CDialog::PreTranslateMessage(pMsg);
}


void CRightImage_ManualRefinementDlg::OnUndo()
{
	int i=num.empty();
	if (m_image&&m_t&&(!num.empty()))
	{
		if (m_image->height>m_t->height)
		{
			cvReleaseImage(&m_dspbuf);
			CvSize size; //缩小后重新分配m_dspbuf，否则m_dspbuf会保留前一张图片的信息，如果这次图片尺寸小于前一张，则这张图片尺寸之外将显示前一张图片的信息
			size.height =(int) m_dsprect.Height(), size.width = (int)m_dsprect.Width();
			m_dspbuf=  cvCreateImage(size, m_image->depth, 4);
			while (m_image->height>m_h)
			{
				Reduce();
			}
		}
		
		CopyImage(m_t,m_image,1);


		m_hpos=0;
		m_vpos=0;
		m_buf=m_image;//防止update mbuf时由于m_image被释放，而m_buf=m_image也被是释放，导致m_buf->height！=m_image->height,从而更新m_buf为m_dspbuf出现内存错误
		GetImageDisplayData(m_dspbuf,m_image,m_hpos,m_vpos);
		Invalidate(FALSE);

		ReleaseVector();////////////////////////////////   释放容器占有的内存

		CString s;

		s="                          执行 UNDO  操作成功！  ";
		GetDlgItem(IDC_EDIT1)->SetWindowText(s);

		HoleEdge();
	}
	else if(!m_image)
	{
		MessageBox("加载图片失败，请重新选择图片！！");
	}	
}

void CRightImage_ManualRefinementDlg::OnExit()
{
	if (SaveFlag==FALSE)
	{
		PMsg.DoModal();
	}
	WriteDir();
	DelTXT();
	if (Ppro->CancleFlag==FALSE)
	{
		Ppro->OnClose();
	}
	this->DestroyWindow();
}
void CRightImage_ManualRefinementDlg::OnClose()
{
	if (SaveFlag==FALSE)
	{
		PMsg.DoModal();
	} 
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////写入本次目录
		 WriteDir();
		 DelTXT();

		 if (Ppro->CancleFlag==FALSE)
		 {
			 Ppro->OnClose();
		 }
		CDialog::OnClose();
	
}
void CRightImage_ManualRefinementDlg::OnDestroy()//任务栏右键关闭
{
	if (SaveFlag==FALSE)
	{
		PMsg.DoModal();
	} 
	WriteDir();
	DelTXT();

	if (Ppro->CancleFlag==FALSE)
	{
		Ppro->OnClose();
	}
	CDialog::OnDestroy();
}

void CRightImage_ManualRefinementDlg::DelTXT()
{
	fclose(m_ImageData);
	fclose(m_MaskData);
	CString t="del "+ImageDataPath;
// 	char *p1;
// 	p1=t.GetBuffer(t.GetLength());//保存 SaveImageData.txt的路径
// 	system(p1);//关闭软件是会调用cmd框
	CString cmd=_T("cmd /c ");
	cmd+=t; 
	WinExec((LPCTSTR)cmd,SW_HIDE);

//	char *p2;
	CString t1="del "+MaskDataPath;
// 	p2=t1.GetBuffer(t1.GetLength());
// 	system(p2);
	cmd=_T("cmd /c ");
	cmd+=t1;
	WinExec((LPCTSTR)cmd,SW_HIDE);
	
}

void CRightImage_ManualRefinementDlg::WriteDir()
{
	CString path; 
	GetModuleFileName(NULL,path.GetBufferSetLength(MAX_PATH+1),MAX_PATH); 
	path.ReleaseBuffer(); 
	int pos = path.ReverseFind('\\'); 
	path = path.Left(pos);
	path=path+"\\"+"system.ini";


	char *p1;
	p1=path.GetBuffer(path.GetLength());//保存 1.txt的路径
	//		fp=fopen(p1,"w+");

	char *p=m_path.GetBuffer(m_path.GetLength());//保存本次打开的目录
	int l=(int)strlen(p);
	if (l>0)
	{
		FILE * fp;
		fopen_s(&fp,p1,"w+");
		fwrite(p,1,l,fp);
		fclose(fp);
	}

	path = path.Left(pos);
	path=path+"\\"+"system_continue.ini";
//	char *p1;
	p1=path.GetBuffer(path.GetLength());//保存 1.txt的路径
	
	int p2;
	if (continueFlag==TRUE)
	{
		p2=m_lbox.GetCurSel();//保存本次未做完的帧序号
	}
	else
	{
		p2=-1;
	}

	FILE * fp;
	fopen_s(&fp,p1,"w+");
	l=sizeof(int);
	fwrite(&p2,l,1,fp);
	fclose(fp);
	
}
void CRightImage_ManualRefinementDlg::OnLastDir()
{

	DirFlag=FALSE;
	((CButton *)GetDlgItem(IDC_RADIO2))->SetCheck(TRUE);
	((CButton *)GetDlgItem(IDC_RADIO1))->SetCheck(FALSE);
	OpenDir() ;
//WriteDir();//放在这个位置，会在读出上次目录之前将上次目录覆盖为本次目录，这样打开的还是本次目录

	/*CString path; 
	GetModuleFileName(NULL,path.GetBufferSetLength(MAX_PATH+1),MAX_PATH); 
	path.ReleaseBuffer(); 
	int pos = path.ReverseFind('\\'); 
	path = path.Left(pos);
	path=path+"\\"+"1.txt";
	FILE * fp;

	char *p1;
	p1=path.GetBuffer(path.GetLength());//保存 1.txt的路径
//	fp=fopen(p1,"r+");
	fopen_s(&fp,p1,"r+");

	char p[100];//保存本次打开的目录
	if (fp)
	{
		fgets(p,100,fp);
		fclose(fp);

		WriteDir();//现将本次路径写入，在将读出的路径给m_path
		m_path=p;

		if (m_path)
		{
			OldDirFlag=TRUE;//设置打开上一次目录
			
			ReadDir();

			OldDirFlag=FALSE;//打开结束还原
		} 
	} 
	else
	{
		MessageBox("上次目录为空");
	}
	CString s=m_path;
	GetDlgItem(IDC_EDIT4)->SetWindowText(s);*/
}

void CRightImage_ManualRefinementDlg::OnBUNDO()
{
	OnUndo();
}

void CRightImage_ManualRefinementDlg::OnHandMouse()
{
	OnMouseHand();
}

void CRightImage_ManualRefinementDlg::OnArrowMouse()
{
	OnMousePoint();
}

void CRightImage_ManualRefinementDlg::OnUseLastDir()
{
//	WriteDir();//放在这个位置，会在读出上次目录之前将上次目录覆盖为本次目录，这样打开的还是本次目录

	CString path; 
	GetModuleFileName(NULL,path.GetBufferSetLength(MAX_PATH+1),MAX_PATH); 
	path.ReleaseBuffer(); 
	int pos = path.ReverseFind('\\'); 
	path = path.Left(pos);
	path=path+"\\"+"system.ini";
	FILE * fp;

	char *p1;
	p1=path.GetBuffer(path.GetLength());//保存 1.txt的路径
	//	fp=fopen(p1,"r+");
	fopen_s(&fp,p1,"r+");

	char p[100];//保存本次打开的目录
	if (fp)
	{
		fgets(p,100,fp);
		fclose(fp);

		WriteDir();//先将本次路径写入，在将读出的路径给m_path
		m_path=p;

//		if (m_path)
//		{
			OldDirFlag=TRUE;//设置打开上一次目录

//			ReadDir();

//			OldDirFlag=FALSE;//打开结束还原
//		} 
	} 
	else
	{
		MessageBox("上次目录为空");
	}

}

void CRightImage_ManualRefinementDlg::OnUseNewDir()//读目录时根据单选框选择状态调用
{
	WriteDir();
	OldDirFlag=FALSE;
}
void CRightImage_ManualRefinementDlg::UseLastDir()//单选框按钮
{
	m_lbox.ResetContent( );
	cvReleaseImage(&m_image);
	((CButton *)GetDlgItem(IDC_CHECK6))->SetCheck(FALSE);
	((CButton *)GetDlgItem(IDC_CHECK5))->SetCheck(FALSE);
	((CButton *)GetDlgItem(IDC_CHECK7))->SetCheck(FALSE);
	((CButton *)GetDlgItem(IDC_CHECK8))->SetCheck(FALSE);
	((CButton *)GetDlgItem(IDC_CHECK9))->SetCheck(FALSE);
	((CButton *)GetDlgItem(IDC_CHECK10))->SetCheck(FALSE);
	OnUseLastDir();
	CString s=m_path;
	GetDlgItem(IDC_EDIT4)->SetWindowText(s);
	DirFlag=TRUE;
}

void CRightImage_ManualRefinementDlg::UseNewDir()//单选框按钮
{
	m_lbox.ResetContent();
	cvReleaseImage(&m_image);
	((CButton *)GetDlgItem(IDC_CHECK6))->SetCheck(FALSE);
	((CButton *)GetDlgItem(IDC_CHECK5))->SetCheck(FALSE);
	((CButton *)GetDlgItem(IDC_CHECK7))->SetCheck(FALSE);
	((CButton *)GetDlgItem(IDC_CHECK8))->SetCheck(FALSE);
	((CButton *)GetDlgItem(IDC_CHECK9))->SetCheck(FALSE);
	((CButton *)GetDlgItem(IDC_CHECK10))->SetCheck(FALSE);
	OnUseNewDir();
	CString s=" ";
	GetDlgItem(IDC_EDIT4)->SetWindowText(s);
	DirFlag=TRUE;
}
void CRightImage_ManualRefinementDlg::OnMutibackground()
{
	colour=BackgroundClour;
	if ((colour>=0)&&(colour<=256))
	{
		FullBKFlag=FALSE;
		char *argv[4];
		CString s1,s2,s3;

		BOOL Flag=FALSE;
		Ppro->CancleFlag=FALSE;
		backgroundProcess(Flag);
		if (Flag==FALSE)
		{
			MessageBox("单帧背景视角转换结束！");

			CString text;  
			int i=m_lbox.GetCurSel();
			m_lbox.GetText(i, text);

			s1=m_path+"\\";
			argv[1]=s1.GetBuffer(s1.GetLength()); //强制转换一下
			s2=pNewMsg.pictureName.Left(5);
//			s2=s2;
			if (s2==text)
			{
				s2=pNewMsg.pictureName.Left(11);
				s2=s2.Right(5);
			}
			argv[2]=s2.GetBuffer(s2.GetLength());
			s3=pNewMsg.pictureName.Right(5);
			argv[3]=s3.GetBuffer(s3.GetLength());


			int k=BackGroundSec(argv,m_path,text);

			if (k==0)
			{
				MessageBox("多帧背景拼接成功！");
			}
			else if (k==-1)
			{
				MessageBox("已经取消本次操作！");
			}
		}
	} 
	else
	{
		MessageBox("请先左键点击图片背景区域");
	}
}

void CRightImage_ManualRefinementDlg::backgroundProcess(BOOL &Flag)
{
	pNewMsg.DoModal();
	CString Msg=pNewMsg.pictureName;
	vector<int> num;

	int i=m_lbox.GetCurSel();

	if (pNewMsg.Flag==TRUE)//输入文件名时取消操作
	{
		Flag=TRUE;
		MessageBox("已经取消本次操作");
	}
	else	if (((m_name_begin=="ProcImg")||(m_name_begin=="origin"))&&(m_name_end==".jpg"))
	{
		char *argv[6];

		CString s1,s2,s3,s4,s5;
		s1=m_path+"\\"+imagename[i];
		argv[1]=s1.GetBuffer(s1.GetLength()); //强制转换一下     ，当前帧     
		s2=imagename[i].Right(9);
		s2="refinement"+s2.Left(5)+".bmp";
		s2=m_path+"\\"+s2;
		argv[3]=s2.GetBuffer(s2.GetLength()); //强制转换一下，当前帧mask图

		int k=Msg.GetLength();
		if (k==5)//只有一个文件时
		{
			Msg+=",";
		}
		while (k>0)
		{
			CString tmp=Msg.Left(6); 
			tmp=tmp.Right(1);
			CString tmp1=Msg.Left(5);
			int l=0,m=0;//l记录序号从哪一帧开始
			CString tmp2;
			m_lbox.GetText(l, tmp2);

			while (tmp1!=tmp2)
			{
				l++;
				if (l<(int)imagename.size())
				{
					m_lbox.GetText(l, tmp2);
				}
				else
				{
					Flag=TRUE;
					break;
				}
				
			}
			if (tmp=="-")
			{
				Msg=Msg.Right(k-6);
				k=Msg.GetLength();
				tmp1=Msg.Left(5);
				m_lbox.GetText(m, tmp2);

				while (tmp1!=tmp2)
				{
					m++;//m记录结束帧的序号
					
					if (m<(int)imagename.size())
					{
						m_lbox.GetText(m, tmp2);
					}
					else
					{
						Flag=TRUE;
						break;
					}
				}
			} 
			else
			{
				m=l;
			}

			if (k>6)
			{
				Msg=Msg.Right(k-6);
			}
			else if (k==5)
			{
				Msg="";
			}
			k=Msg.GetLength();

			int j=0;
			for (j=l;j<=m/*(int)imagename.size()*/;j++)
			{
				if (j!=i)
				{
					num.push_back(j);
				}
			}
		}

		if (Flag==TRUE)
		{
			MessageBox("输入的文件序号有误！");
		} 
		else
		{
			MSG msg;
			while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				::TranslateMessage(&msg);
				::DispatchMessage(&msg);
			}

			float t1=(int)num.size();
			int i3=0;
			for (i3=0;i3<5;i3++)
			{
				
				if (Ppro->CancleFlag==FALSE)
				{
					Ppro->show(0);
				}
				MSG msg;
				while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
				{
					::TranslateMessage(&msg);
					::DispatchMessage(&msg);
				}
				if (Ppro->CancleFlag==TRUE)
				{
					Flag=TRUE;//表示取消，本次不再往下执行
					break;
				}
			}

			if (Ppro->CancleFlag==FALSE)
			{
				int i1=0;
				for (i1=0;i1<t1;i1++)
				{
					MSG msg;
					while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
					{
						::TranslateMessage(&msg);
						::DispatchMessage(&msg);
					}

					int j=num[i1];
					s3=m_path+"\\"+imagename[j];
					argv[0]=s3.GetBuffer(s3.GetLength()); //强制转换一下          
					s4=imagename[j].Right(9);
					s4="refinement"+s4.Left(5)+".bmp";;
					s4=m_path+"\\"+s4;
					argv[2]=s4.GetBuffer(s4.GetLength()); //强制转换一下

					s5=imagename[j].Right(9);
					s5=s5.Left(5);
					if (FullBKFlag==TRUE)
					{
						s5="Background_"+s5+"_Filled.png";
						s5=m_path+"\\"+s5;
						argv[5]=s5.GetBuffer(s5.GetLength()); //强制转换一下
						CString s6=imagename[i].Right(9);
						s6=s6.Left(5);
						s6="Background_"+s6+"_Filled.png";
						s6=m_path+"\\"+s6;
						argv[4]=s6.GetBuffer(s6.GetLength()); //强制转换一下
						int k=BackGroundThird(argv,colour,BRIGHTNESS_COMPENSATION);
						if(-1==	k)
						{
							Flag=TRUE;
							Ppro->OnClose();
						}
					}
					else
					{
						s5="Background_tmp_"+s5+".png";
						s5=m_path+"\\"+s5;
						argv[4]=s5.GetBuffer(s5.GetLength()); //强制转换一下
						backGround(argv,colour);
					}

					if (Flag==TRUE)//
					{
						break;
					}
					//			MSG msg;
					while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
					{
						::TranslateMessage(&msg);
						::DispatchMessage(&msg);
					}

					float t=(i1+1)/t1*100;
					//		float t2=i1/t1*100;
					//		int x=0;
					int  i7;
					for (i7=0;i7<100;i7++)
					{
						if (Ppro->CancleFlag==FALSE)
						{
							Ppro->show(t);
						}
						//			Sleep(1000);
						while(::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
						{
							::TranslateMessage(&msg);
							::DispatchMessage(&msg);
						}

						if (Ppro->CancleFlag==TRUE&&t<100)//中途关闭进度条，取消本次操作
						{
							Flag=TRUE;//表示取消，本次不再往下执行
							break;
						}
					}

					if (Ppro->CancleFlag==TRUE)
					{
						Flag=TRUE;//表示取消，本次不再往下执行
						break;
					}

					if (abs(t-100)<0.00001)
					{
						Sleep(1000);
						Ppro->OnClose();
					}
				}
			}
			
			if (!num.empty())
			{
				num.clear();
				vector<int>().swap(num);
			}
		}
	} 
	else
	{
		MessageBox("该文件名或者格式不正确，请重新选择文件");
		Flag=TRUE;
	}

}
void CRightImage_ManualRefinementDlg::OnFull_bk()//默认有亮度和颜色补偿 BRIGHTNESS_COMPENSATION=0；
{
	if (colour==-2||colour==-1)//没有进行多帧背景拼接，直接进行全背景转换，或者上次多帧背景拼接后退出软件，本次开始全背景转换。
	{
		colour=BackgroundClour;
	}
	if (m_name.Left(10)=="Background")
	{
		int c=colour;
		OnLopen();
		colour=c;
	}

	if ((colour>=0)&&(colour<=256))
	{
		FullBKFlag=TRUE;
		BOOL Flag=FALSE;
		Ppro->CancleFlag=FALSE;
		backgroundProcess(Flag);
		if (Flag==FALSE)
		{
			MessageBox("全背景拼接结束！");
		}
	} 
	else
	{
		MessageBox("请先左键点击图片背景区域\n\n确保选择的背景区域与多帧背景区域相一致。");
		colour=-2;//这种情况说明没有进行多帧背景拼接，直接进行全背景转换，或者上次多帧背景拼接后退出软件，本次开始全背景转换。
	}
}

void CRightImage_ManualRefinementDlg::FullBKCancleLight()//无亮度无颜色补偿
{
	BRIGHTNESS_COMPENSATION=1;
	OnFull_bk();
	BRIGHTNESS_COMPENSATION=0;

}
void CRightImage_ManualRefinementDlg::LightNoColour()//有亮度无颜色补偿
{
	BRIGHTNESS_COMPENSATION=2;
	OnFull_bk();
	BRIGHTNESS_COMPENSATION=0;
}

void CRightImage_ManualRefinementDlg::NoLoseBK()//不采用亮度颜色补偿，直接复制原图
{
	BRIGHTNESS_COMPENSATION=3;
	OnFull_bk();
	BRIGHTNESS_COMPENSATION=0;
}
void CRightImage_ManualRefinementDlg::OnOpenMutiBK()//打开多帧拼接后的png图片
{
	CString s;
	CString s1;
	int i=m_lbox.GetCurSel();
	m_lbox.GetText(i, s1);
	s=m_path+"\\"+"Background_"+s1+"_Filled.png";
	CString s2=m_pathname;
	m_pathname=s;
	m_new=1;
	cvReleaseImage(&m_image);
	ReadImage();//如果打开成功，在给m_image复制前会将其Release，如果打开失败，m_image仍然是当前帧
	m_new=0;

	if (!m_image)
	{
		s=m_path+"\\"+"Background_extended_"+s1+".png";
		m_new=1;
		m_pathname=s;
		cvReleaseImage(&m_image);
		ReadImage();//如果打开成功，在给m_image复制前会将其Release，如果打开失败，m_image仍然是当前帧
		m_new=0;
	}
	num.clear();
	ReleaseVector();

	m_name="Background_"+s1+"_Filled.png";

// 	m_name_begin="Background_";
// 	m_name_end="_Filled.bmp";

	m_pathname=m_path+"\\"+m_name;//保存时以这个路径

	if (m_image)
	{
		GetImageDisplayData(m_dspbuf, m_image, m_hpos, m_vpos);
		Invalidate(FALSE);
		///////////////////////////////////////////////////////////////////////////////////// 

		IplImage* tmp_Mask=NULL;
		cvReleaseImage(&tmp_Mask);
		CvSize dst_cvsize;
		dst_cvsize.width=(int)(m_image->width);
		dst_cvsize.height=(int)(m_image->height);
		tmp_Mask=cvCreateImage(dst_cvsize,m_image->depth,3);

		int j=0;
		for (j=0;j<m_image->height;j++)
			for (i=0;i<m_image->width;i++)
			{
				
				BYTE s=m_image->imageData[ j * m_image->widthStep + m_image->nChannels * i]+m_image->imageData[ j * m_image->widthStep + m_image->nChannels * i +1] +m_image->imageData[ j * m_image->widthStep + m_image->nChannels * i +2];
				if (s==0&&(i!=m_image->width/3)&&(i!=2*m_image->width/3)&&(j!=m_image->height/2))
				{
					tmp_Mask->imageData[ j * tmp_Mask->widthStep + tmp_Mask->nChannels * i ] =(BYTE)0;
					tmp_Mask->imageData[ j * tmp_Mask->widthStep + tmp_Mask->nChannels * i +1] =(BYTE)0;
					tmp_Mask->imageData[ j * tmp_Mask->widthStep + tmp_Mask->nChannels * i +2] =(BYTE)0;
				}
				else
				{
					tmp_Mask->imageData[ j * tmp_Mask->widthStep + tmp_Mask->nChannels * i ] =(BYTE)255;
					tmp_Mask->imageData[ j * tmp_Mask->widthStep + tmp_Mask->nChannels * i +1] =(BYTE)255;
					tmp_Mask->imageData[ j * tmp_Mask->widthStep + tmp_Mask->nChannels * i +2] =(BYTE)255;
				}
	
				m_image->imageData[ j * m_image->widthStep + m_image->nChannels * i + 3 ] =(BYTE)255;//设置透明度
			}

			if (tmp_Mask)
			{
				CopyImage(tmp_Mask,m_MaskImage,1);

				cvReleaseImage(&tmp_Mask);
				//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				//这段防止点击New Picture 却没有选择图片就在当选了图片的情况下使用复制新打开的图片这个功能
				CopyImage(m_image,m_t2,1);
				//////////////////////////////////////////////对专门保存新打开的mask图操作
				CopyImage(m_MaskImage,m_NewMask,1);

				CString p=m_path+"\\"+"Background_"+s1+"_Filled.bmp";
				cvSaveImage(p,m_MaskImage);

			} 
	} 
	else
	{
		MessageBox("没有对应的多帧背景拼接图！");
		int c=colour;
		OnLopen();
		colour=c;
//		Invalidate(FALSE);
	}
}

void CRightImage_ManualRefinementDlg::ContinueThisShot()
{
	continueFlag=TRUE;
}

 void CRightImage_ManualRefinementDlg::ReadLastpicture()
 {
	 CString path; 
	 GetModuleFileName(NULL,path.GetBufferSetLength(MAX_PATH+1),MAX_PATH); 
	 path.ReleaseBuffer(); 
	 int pos = path.ReverseFind('\\'); 
	 path = path.Left(pos);
	 path=path+"\\"+"system_continue.ini";
	 FILE * fp;

	 char *p1;
	 p1=path.GetBuffer(path.GetLength());//保存 1.txt的路径
	 //	fp=fopen(p1,"r+");
	 fopen_s(&fp,p1,"rb+");

	 if (fp)
	 {
		 int a[1];
		 fread(a,4,1,fp);
		 continueNum=a[0];

		 fclose(fp);
	 }
	 else
	 {
		continueNum=-1;
	 }
 }

 
