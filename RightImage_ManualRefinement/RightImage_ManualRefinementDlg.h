// newDlg.h : 头文件
//


#pragma once
#include <opencv2\opencv.hpp> 

#include "Windows.h"

#include "newDialog.h"//使用新对话框

#include "Tools.h"//使用工具栏

#include "MyMessage.h"//自定义信息提示
#include "MyNewMessage.h"
#include "Progress.h"

#include "resource.h"
#include <vector>
#include "afxwin.h"

using namespace std;

typedef struct copyImageData
{
	CPoint point;
	BYTE colour[3];
} copyData;

// CnewDlg 对话框
class CRightImage_ManualRefinementDlg : public CDialog
{
// 构造
public:
	CRightImage_ManualRefinementDlg(CWnd* pParent = NULL);	// 标准构造函数

	void SetImageData(IplImage* &image,int j,int i,IplImage* &temp,int j1,int i1);

    void ReadImage();

	void GetImageDisplayData(IplImage* dspbuf, IplImage*& imagescale, int hpos, int vpos);

	void SetListNum();

	void CloseNewPicture();

// 对话框数据
	enum { IDD = IDD_NEW_DIALOG };

	CString m_pathname;
	BOOL m_mouseSelect;
	BYTE SaveFlag;
	int m_regionflag;


	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持
	IplImage *m_image;

	IplImage *m_MaskImage;//用来存储m_image对应的mask图

	IplImage *m_NewMask;//用来存储新打开的m_image对应的mask图

	CRect       m_dsprect;
	IplImage *m_dspbuf;//图片显示的区域的大小，初始设为和picture control控件一样大
	IplImage *m_buf;

	IplImage *m_square;//保存小方块图像

	IplImage *m_Masksquare;//保存Mask小方块图像


	IplImage * m_t;//保存 hole边缘前的m_image

	IplImage * m_t1;//保存 hole边缘后的m_image

	IplImage * m_t2;//保存新打开的图片

	IplImage *m_buf1;//保存hole edge 前的dspimage

//	vector <copyData> SaveImageData;//保存每一步操作后的m_image改变了的点的rgb值；
//	vector <BYTE> SaveMaskData;//保存每一步操作后的m_MaskImage改变了的点的rgb值,由于坐标与m_image相同，只需要记录rgb一个分量
	FILE * m_ImageData;
	FILE * m_MaskData;
	CString m_CurPath;
	CString ImageDataPath;
	CString MaskDataPath;

	vector <int> num;//记录每次操作了多少个点

	int m_preNum;

	vector <CString> imagename;

	int imagenum,n;

	

	CString m_path;

	CString m_name;
	CString m_name_begin;
	CString m_name_end;

	int m_Tools;//scoop 区域 brush 工具选择标志

	int m_scoop;
	int m_hpos;
	int m_vpos;
	
	int m_h;// 记录每次新打开的图片的尺寸
	int m_w;

	int m_flag;//  scoop没左点击一次，置为1，每右击一置为0；

	int m_holeedgeFlag;//标记hole边缘的选中，默认为0，第一次选为1，再点击选为0；

	int m_new;//标记打开新图片；

	BYTE *flag;

	BYTE *Brushflag;



	int m_regiontool;//区域工具标记

	CPoint m_point;

	CPoint m_point1;//记录scoop最后一次点击的坐标

	CPoint m_ButtonUp;
	CPoint m_ButtonDown;

	CPoint m_BrushLastPoint;

	int m_ButtonUpFlag;

	int overFlag;

	int BrushRightflag;
	int preBrushRightflag;

	BYTE LeftButtonUp;

	BYTE firstFlag;//标注第一次左键按下；


	int m_HoleEdgePoint[4];

	int H,W;//屏幕显示的图片左上角坐标
	int h,w;//有效区长宽

	vector <CPoint> m_pnt;//记录brush右键第一个点

	int m_NewPicture;//控制是否新打开图片

	int m_ChangeSize;//控制滑轮是改变scoop还是图片

	int m_ShowEdge;//控制区域工具使用时是否显示绿色边界


	static const int SQUARE=1;
	static const int REGION=2;
	static const int BOTH=3;
	static const int HOLE_PIXEL=4;
	static const int NORMAL_PIXEL=5;
	static const int CIRCLE=6;

	CnewDialog  *Pnewdailog;//打开新窗口

	Tools *PTool;//打开工具栏

	CMyMessage PMsg;
	CMyNewMessage pNewMsg;
	CProgress *Ppro;

	CToolBar m_toolbar;//添加工具栏
	
	BYTE OldDirFlag;

	BYTE ScaleFlag;//记录图片是否是最大尺寸或最小尺寸，=0表示最小尺寸，=10表示最大尺寸

	BOOL FullBKFlag;//标记全背景转换

	BOOL DirFlag;

	int BackgroundClour;
	int colour;

	int BRIGHTNESS_COMPENSATION;
	BOOL continueFlag;
	int continueNum;

	void Scoop(CPoint point,int &tmp_num);
	void Manual(CPoint point,int &tmp_num);
	void ReadDir();

	void  Edge();

	void RegionGrow(IplImage *image,vector<CPoint> &growpnts, CPoint point, int w,int h,BYTE *flag);

	void CancleRegion(IplImage *image,vector<CPoint> &growpnts, CPoint point, int w,int h,BYTE *flag);

	void Region();

	void UpdateMbuf();//更新 m_buf

	void SetRegionFlag(int x,int y,IplImage *t1);

	void SetPicturePoint(int &x,int &y,CPoint &point);

	void BrushPoint(CPoint point);

	void DrawLine(IplImage *image, CPoint begin, CPoint end );

	void GetAllBrushPoint();

	void RegionTool();

	void UpdateMt1();

	void SaveVector(IplImage * &imagePoint,int j,int i);//将m_image或者mask暂时保存以备撤销时用

	void PushVector();

	void ReleaseVector();//释放容器内存，清空容器

	void zoom(float scale);

	void CopyImage(IplImage* &src,IplImage* &dst,float scale);

	BOOL PreTranslateMessage(MSG* pMsg);

	void CopyAllHole(CString &text,CString &tmp_maskPath,CString &tmp_CurrentimageMaskPath);

	
void WriteDir();

void backgroundProcess(BOOL &Flag);

void Message(int &i);

void DelTXT();

void SetDisplayPoint(int &x,int &y);
void ReadLastpicture();//读取上一次未做完的帧序号



// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	afx_msg BOOL OnToolTipNotify( UINT id, NMHDR * pNMHDR, LRESULT * pResult );//声明工具栏的显示提示功能
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
public:
	afx_msg void OpenDir();
public:
	afx_msg void OnBig();
public:
	afx_msg void Reduce();
public:
	afx_msg void OnLbnSelchangeList2();
public:
	afx_msg void OpenListPicture();
public:
	CListBox m_lbox;
public:
	afx_msg void Cancle();
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
public:
	afx_msg void OnRButtonDblClk(UINT nFlags, CPoint point);
public:
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
public:
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
public:
	CScrollBar m_SBar;
public:
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
public:
	CScrollBar m_SvBar;
public:
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
public:
	afx_msg void OnSave();
public:
//	afx_msg void OnEdge();
public:
	afx_msg void OnRegion();
public:
	afx_msg void OnBrush();
public:
	afx_msg void OnLopen();
public:
	afx_msg void OnBleft();
public:
	afx_msg void Onleft();
public:
	afx_msg void OnBoth();
public:
	afx_msg void OnBright();
public:
	afx_msg void OnRight();
public:
	afx_msg void OnCopy();
public:
	afx_msg void OnSelectScoop();
public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
public:
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
public:
	afx_msg void OnHolePixel();
public:
	afx_msg void OnNormalPixel();
public:
	afx_msg void OnCircle();
public:
	afx_msg void OnBackCopy();
//public:
//	afx_msg void OnNew();
public:
	afx_msg void OnNewPicture();
public:
	afx_msg void OnBnClickedRadi11();
public:
	afx_msg void OnBigORLittle();
public:
	afx_msg void HoleEdge();
public:
	afx_msg void OnShowEdge();
/*
public:
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);*/
public:
	afx_msg void OnOpendir();
public:
	afx_msg void Save();
public:
	afx_msg void OnReback();
public:
	afx_msg void OnCopyforward();
public:
	afx_msg void OnCopybackward();
public:
	afx_msg void OnScoopSquare();
public:
	afx_msg void OnScoopCircle();
public:
	afx_msg void OnRegionSelect();
public:
	afx_msg void OnBrushBoth();
public:
	afx_msg void OnBrushHolepixel();
public:
	afx_msg void OnBrushNormalpixel();
public:
	afx_msg void Onright();
public:
	afx_msg void OnSelectleft();
public:
	afx_msg void OnSelectboth();
public:
	afx_msg void OnSright();
public:
	afx_msg void OnSleft();
public:
	afx_msg void OnImageBiger();
public:
	afx_msg void OnImageReduce();
public:
	afx_msg void OnMousePoint();
public:
	afx_msg void OnMouseHand();
public:
	afx_msg void OnExit();
public:
	afx_msg void OnTools();
public:
	afx_msg void OnUndo();
public:
	afx_msg void OnCopyAllforward();
public:
	afx_msg void OnCopyAllback();
public:
	afx_msg void OnClose();
public:
	afx_msg void OnLastDir();
public:
	afx_msg void OnBUNDO();
public:
	afx_msg void OnHandMouse();
public:
	afx_msg void OnArrowMouse();
public:
	afx_msg void OnUseLastDir();
public:
	afx_msg void OnUseNewDir();
public:
	afx_msg void OnMutibackground();
public:
	afx_msg void OnFull_bk();//声明
public:
	afx_msg void OnOpenMutiBK();
public:
	afx_msg void UseLastDir();
public:
	afx_msg void UseNewDir();
public:
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
public:
	afx_msg void OnDestroy();
public:
	afx_msg void FullBKCancleLight();
public:
	afx_msg void ContinueThisShot();
public:
	afx_msg void LightNoColour();
public:
	afx_msg void NoLoseBK();
};



//void Mouse(int Event,int x,int y,int flags,void* param);

void backGround(char **argv,int x);
int BackGroundSec(char** argv,CString s,CString s1);
int BackGroundThird(char** argv,int x,BOOL s);