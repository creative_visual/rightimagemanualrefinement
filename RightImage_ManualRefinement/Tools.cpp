// Tools.cpp : 实现文件
//

#include "stdafx.h"
#include "RightImage_ManualRefinement.h"
#include "Tools.h"


// Tools 对话框

IMPLEMENT_DYNAMIC(Tools, CDialog)

Tools::Tools(CWnd* pParent /*=NULL*/)
	: CDialog(Tools::IDD, pParent)
{

}

Tools::~Tools()
{
}

void Tools::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(Tools, CDialog)
	ON_BN_CLICKED(IDC_RADIO1, &Tools::OnTBoth)
	ON_BN_CLICKED(IDC_RADIO7, &Tools::OnNormalPixel)
	ON_BN_CLICKED(IDC_RADIO8, &Tools::OnHolePixel)
	ON_BN_CLICKED(IDC_RADIO5, &Tools::OnRegion)
	ON_BN_CLICKED(IDC_RADIO6, &Tools::OnSquare)
	ON_BN_CLICKED(IDC_RADIO9, &Tools::OnCircle)
	ON_BN_CLICKED(IDC_BUTTON11, &Tools::OnCopyForward)
	ON_BN_CLICKED(IDC_BUTTON12, &Tools::OnCopyBack)
	ON_BN_CLICKED(IDC_BUTTON6, &Tools::Onleft)
	ON_BN_CLICKED(IDC_BUTTON9, &Tools::Onright)
	ON_BN_CLICKED(IDC_BUTTON8, &Tools::OnSBoth)
	ON_BN_CLICKED(IDC_BUTTON7, &Tools::OnBleft)
	ON_BN_CLICKED(IDC_BUTTON10, &Tools::OnBright)
	ON_BN_CLICKED(IDC_BUTTON1, &Tools::Onbig)
	ON_BN_CLICKED(IDC_BUTTON3, &Tools::OnReduce)
	ON_WM_MOUSEWHEEL()
END_MESSAGE_MAP()


// Tools 消息处理程序

void Tools::OnTBoth()
{
	theApp.pRight->OnBrushBoth();
}

void Tools::OnNormalPixel()
{
	theApp.pRight->OnBrushNormalpixel();
}

void Tools::OnHolePixel()
{
	theApp.pRight->OnBrushHolepixel();
}

void Tools::OnRegion()
{
	theApp.pRight->OnRegionSelect();
}

void Tools::OnSquare()
{
	theApp.pRight->OnScoopSquare();
}

void Tools::OnCircle()
{
	theApp.pRight->OnScoopCircle();
}

void Tools::OnCopyForward()
{
	theApp.pRight->OnCopy();
}

void Tools::OnCopyBack()
{
	theApp.pRight->OnBackCopy();
}

void Tools::Onleft()
{
	theApp.pRight->OnBleft();//镜像 左边
}

void Tools::Onright()
{
	theApp.pRight->OnBright();// 镜像 右 边
}

void Tools::OnSBoth()
{
	theApp.pRight->OnBoth();
}

void Tools::OnBleft()
{
	theApp.pRight->Onleft();
}

void Tools::OnBright()
{
	theApp.pRight->OnRight();
}

void Tools::Onbig()
{
	theApp.pRight->OnBig();
}

void Tools::OnReduce()
{
	theApp.pRight->Reduce();
}

BOOL Tools::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt)
{
	theApp.pRight->OnMouseWheel(nFlags,  zDelta, pt);

	return CDialog::OnMouseWheel(nFlags, zDelta, pt);
}
