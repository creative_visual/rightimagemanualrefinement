#pragma once


// Tools 对话框

class Tools : public CDialog
{
	DECLARE_DYNAMIC(Tools)

public:
	Tools(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~Tools();

// 对话框数据
	enum { IDD = IDD_DIALOG1 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnTBoth();
public:
	afx_msg void OnNormalPixel();
public:
	afx_msg void OnHolePixel();
public:
	afx_msg void OnRegion();
public:
	afx_msg void OnSquare();
public:
	afx_msg void OnCircle();
public:
	afx_msg void OnCopyForward();
public:
	afx_msg void OnCopyBack();
public:
	afx_msg void Onleft();
public:
	afx_msg void Onright();
public:
	afx_msg void OnSBoth();
public:
	afx_msg void OnBleft();
public:
	afx_msg void OnBright();
public:
	afx_msg void Onbig();
public:
	afx_msg void OnReduce();
public:
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
};
