//#include "stdafx.h"// newDialog.cpp : 实现文件
//

#include "stdafx.h"
#include "RightImage_ManualRefinement.h"
#include "newDialog.h"


// CnewDialog 对话框

IMPLEMENT_DYNAMIC(CnewDialog, CDialog)

CnewDialog::CnewDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CnewDialog::IDD, pParent)
{
//	pRight=NULL;
	m_dspbuf=NULL;
	m_image=NULL;
	m_Cpoint=0;

	m_hpos=0;
	m_vpos=0;

}

CnewDialog::~CnewDialog()
{
}

void CnewDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_SCROLLBAR2, m_SvBar);
	DDX_Control(pDX, IDC_SCROLLBAR1, m_SBar);
}


BEGIN_MESSAGE_MAP(CnewDialog, CDialog)
	ON_WM_RBUTTONDOWN()
	ON_WM_MOUSEMOVE()
	ON_WM_LBUTTONDOWN()
//	ON_NOTIFY(NM_THEMECHANGED, IDC_SCROLLBAR2, &CnewDialog::OnNMThemeChangedScrollbar2)
ON_WM_HSCROLL()
ON_WM_VSCROLL()
ON_WM_PAINT()
ON_WM_CLOSE()
ON_WM_LBUTTONUP()
END_MESSAGE_MAP()


// CnewDialog 消息处理程序

void CnewDialog::OnMouseMove(UINT nFlags, CPoint point)
{
	m_Cpoint=point;
	SetPicturePoint(m_Cpoint);
	theApp.pRight->OnMouseMove(nFlags,m_Cpoint);


	if ((theApp.pRight->m_mouseSelect==TRUE)&&m_image)
	{
		int tmp1=m_image->width-m_dsprect.Width();
		int tmp2=m_image->height-m_dsprect.Height();
		if((nFlags==MK_RBUTTON)&&(tmp1>0)&&(tmp2>0))
		{
			m_hpos-=point.x-m_point.x;
			m_vpos-=point.y-m_point.y;
			if (m_hpos<0)
			{
				m_hpos=0;
			}
			else if (m_hpos>tmp1)
			{
				m_hpos=tmp1;
			}

			if (m_vpos<0)
			{
				m_vpos=0;
			}
			else if (m_vpos>tmp2)
			{
				m_vpos=tmp2;
			}

			int position=100*m_hpos/tmp1;
			m_SBar.SetScrollPos(position);
			position=100*m_vpos/tmp2;
			m_SvBar.SetScrollPos(position);//根据position 的值来设定滑动块的位置
			UpdateData(FALSE);

			GetImageDisplayData(m_dspbuf,m_image,m_hpos,m_vpos);
			Invalidate(FALSE);

		}
	}
	m_point=point;
	CDialog::OnMouseMove(nFlags, point);
}
void CnewDialog::OnLButtonDown(UINT nFlags, CPoint point)
{
	m_Cpoint=point;
	SetPicturePoint(m_Cpoint);
	if (theApp.pRight->m_regionflag==1)
	{
		theApp.pRight->OnLButtonDown(nFlags,m_Cpoint);
	}
	else
	{
		MessageBox("请先右键选定区域！！");
	}
	

	CDialog::OnLButtonDown(nFlags, point);
}
void CnewDialog::OnLButtonUp(UINT nFlags, CPoint point)
{
	SetPicturePoint(m_Cpoint);
	theApp.pRight->OnLButtonUp(nFlags,m_Cpoint);

	CDialog::OnLButtonUp(nFlags, point);
}
void CnewDialog::OnRButtonDown(UINT nFlags, CPoint point)
{
	
	/*m_Cpoint=point;
	SetPicturePoint(m_Cpoint);
	theApp.pRight->OnRButtonDown(nFlags,m_Cpoint);*/

	CDialog::OnRButtonDown(nFlags, point);
}

/*
void CnewDialog::OnPaint(IplImage *image)
{
//	CDialog::OnPaint();

	if(image)
	{
		CPaintDC dc(GetDlgItem(IDC_STATIC));
		CDC dcMem;
		CBitmap bmpMem;
		dcMem.CreateCompatibleDC(&dc);
		bmpMem.CreateCompatibleBitmap(&dc, m_dsprect.Width(), m_dsprect.Height());
		dcMem.SelectObject(&bmpMem);

		int linebyte;
		int w = m_dsprect.Width();
		int h = m_dsprect.Height();
		linebyte = m_dspbuf->widthStep;

		bmpMem.SetBitmapBits(linebyte * h, m_dspbuf->imageData);
		dc.BitBlt(0, 0, w,h, &dcMem, 0, 0, SRCCOPY);

		bmpMem.DeleteObject();
		dcMem.DeleteDC(); 
	}
	
}*/
void CnewDialog::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: 在此处添加消息处理程序代码
	// 不为绘图消息调用 CDialog::OnPaint()
//	CDialog::OnPaint();

	if(m_image)
	{
		CPaintDC dc(GetDlgItem(IDC_STATIC));
		CDC dcMem;
		CBitmap bmpMem;
		dcMem.CreateCompatibleDC(&dc);
		bmpMem.CreateCompatibleBitmap(&dc, m_dsprect.Width(), m_dsprect.Height());
		dcMem.SelectObject(&bmpMem);

		int linebyte;
		int w = m_dsprect.Width();
		int h = m_dsprect.Height();
		linebyte = m_dspbuf->widthStep;

		bmpMem.SetBitmapBits(linebyte * h, m_dspbuf->imageData);
		dc.BitBlt(0, 0, w,h, &dcMem, 0, 0, SRCCOPY);

		bmpMem.DeleteObject();
		dcMem.DeleteDC(); 
	}
}
void CnewDialog::showImage(IplImage *&image,CString &tmp_path)
{
	text=tmp_path;
	SetWindowTextA(text);
	IplImage *tmp=image;
	GetDlgItem(IDC_STATIC)->GetWindowRect(m_dsprect);

	cvReleaseImage(&m_dspbuf);
	CvSize size;
	size.height = m_dsprect.Height(), size.width = m_dsprect.Width();
	m_dspbuf=  cvCreateImage(size, image->depth, 4);

	GetImageDisplayData(m_dspbuf,image,m_hpos,m_vpos);

	ShowWindow(SW_SHOWNA);

	if (tmp!=m_image)
	{
		CvSize dst_cvsize;
		dst_cvsize.width=(int)(image->width);
		dst_cvsize.height=(int)(image->height);
		cvReleaseImage(&m_image);
		m_image=cvCreateImage(dst_cvsize,image->depth,image->nChannels);
		cvResize(image,m_image,CV_INTER_NN);
	}
	
//	OnPaint();
	Invalidate(FALSE);
}

inline void CnewDialog::GetImageDisplayData(IplImage* dspbuf, IplImage* &imagescale, int hpos, int vpos)
{
	int i,j,i2,j2;
	int linebyte = dspbuf->widthStep;
	int linebytesclae = imagescale->widthStep;
	char *temp_dsp = dspbuf->imageData, *temp_scale = imagescale->imageData;
	UINT *dsp = (UINT *)temp_dsp, *scale = (UINT *)temp_scale;

	if( imagescale->width <= dspbuf->width && imagescale->height <= dspbuf->height )
	{
		int x = (dspbuf->width-imagescale->width)/2, y = (dspbuf->height-imagescale->height)/2;
		for( i = y, i2=0; i < imagescale->height+y, i2<imagescale->height; i++,i2++ )
			for( j = x, j2=0; j < imagescale->width+x, j2<imagescale->width; j++,j2++ )
			{
				//if( i < imagescale->height && j < imagescale->width )
				{
					dsp[i * dspbuf->width + j] = scale[(i2+vpos) * imagescale->width + (j2+hpos)];
				}
			}
	}

	if( imagescale->width <= dspbuf->width && imagescale->height > dspbuf->height )
	{
		int x = (dspbuf->width-imagescale->width)/2, y = 0;
		for( i = 0; i < dspbuf->height; i++)
			for( j = x, j2=0; j < imagescale->width+x, j2<imagescale->width; j++,j2++ )
			{
				//if( i < imagescale->height && j < imagescale->width )
				{
					dsp[i * dspbuf->width + j] = scale[(i+vpos) * imagescale->width + (j2+hpos)];
				}
			}
	}

	if( imagescale->width > dspbuf->width && imagescale->height <= dspbuf->height )
	{
		int x = 0, y = (dspbuf->height-imagescale->height)/2;
		for( i = y, i2=0; i < imagescale->height+y, i2<imagescale->height; i++,i2++ )
			for( j = 0; j < dspbuf->width; j++ )
			{
				//if( i < imagescale->height && j < imagescale->width )
				{
					dsp[i * dspbuf->width + j] = scale[(i2+vpos) * imagescale->width + (j+hpos)];
				}
			}
	}

	if( imagescale->width > dspbuf->width && imagescale->height > dspbuf->height )
	{
		DWORD oldtime = GetTickCount();
		for( i = 0; i < dspbuf->height; i++)
			for( j = 0; j < dspbuf->width; j++ )
			{
				if( i < imagescale->height && j < imagescale->width )
				{
					dsp[i * dspbuf->width + j] = scale[(i+vpos) * imagescale->width + (j+hpos)];
				}
			}
			DWORD time = GetTickCount() - oldtime;
			int kkk = 0;
	}
	temp_dsp = NULL, temp_scale = NULL;
	dsp = NULL,scale = NULL;
}


void CnewDialog::SetPicturePoint(CPoint &point)
{
	int H,W;//屏幕显示的图片左上角坐标
	int h,w;//有效区长宽
	int x=0,y=0;

	GetDlgItem(IDC_STATIC)->GetWindowRect(m_dsprect);
	ScreenToClient(m_dsprect);

	if (m_image->height<m_dsprect.Height())
	{
		if (m_image->width<m_dsprect.Width())
		{
			H=m_dsprect.TopLeft().y+(m_dsprect.Height()-m_image->height)/2;
			W=m_dsprect.TopLeft().x+(m_dsprect.Width()-m_image->width)/2;

			h=m_image->height;
			w=m_image->width;
			x=point.x-W;
			y=point.y-H;
		} 
		else
		{
			H=m_dsprect.TopLeft().y+(m_dsprect.Height()-m_image->height)/2;
			W=m_dsprect.TopLeft().x;

			h=m_image->height;
			w=m_dsprect.Width();
			x=point.x-W+m_hpos;
			y=point.y-H;
		}
	} 
	else
	{
		if (m_image->width<m_dsprect.Width())
		{
			H=m_dsprect.TopLeft().y;
			W=m_dsprect.TopLeft().x+(m_dsprect.Width()-m_image->width)/2;

			h=m_dsprect.Height();
			w=m_image->width;

			x=point.x-W;
			y=point.y-H+m_vpos;
		} 
		else
		{
			H=m_dsprect.TopLeft().y;
			W=m_dsprect.TopLeft().x;

			h=m_dsprect.Height();
			w=m_dsprect.Width();

			x=point.x-W+m_hpos;
			y=point.y-H+m_vpos;
		}
	}

	point.x=x;
	point.y=y;

}


void CnewDialog::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	if (m_image)
	{
		int t=m_image->width-m_dsprect.Width();

		if (t>0)
		{
			m_SBar.SetScrollRange(0,100);//水平滑动条范围设置为 0 到100
			//m_SBar.SetScrollPos(0); //水平滑动条初始位置设置为 0 
			int position=m_SBar.GetScrollPos();

			switch (nSBCode)
			{
			case SB_LINELEFT://向左滚动一行 
				position--; 
				break; 
			case SB_LINERIGHT://向右滚动一行 
				position++; 
				break; 
			case SB_PAGELEFT: //向左滚动一页 
				position-=10; 
				break; 
			case SB_PAGERIGHT: //向右滚动一页 
				position+=10; 
				break; 
			case SB_THUMBTRACK://拖动滑动块 
				position=nPos;//这里的nPos 就是函数 OnHScroll()的第 2 个参数 
				break; 
			case SB_LEFT://移动到最左边 
				position=0; 
				break; 
			case SB_RIGHT://移动到最右边 
				position=100; 
				break; 
			} 
			if(position>100) position=100; 
			if(position<0) position=0; 
			m_SBar.SetScrollPos(position);//根据position 的值来设定滑动块的位置

			m_hpos=position*t/100;

			showImage(m_image,text);
		} 
	} 

	CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
}

void CnewDialog::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar)
{
	if (m_image)
	{
		// 垂直滚动条改变时，更新m_buf

		int t=m_image->height-m_dsprect.Height();
		if (t>0)
		{
			m_SvBar.SetScrollRange(0,100);//水平滑动条范围设置为 0 到100
			//m_SBar.SetScrollPos(0); //水平滑动条初始位置设置为 0 
			int position=m_SvBar.GetScrollPos();

			switch (nSBCode)
			{
			case SB_LINELEFT://向左滚动一行 
				position--; 
				break; 
			case SB_LINERIGHT://向右滚动一行 
				position++; 
				break; 
			case SB_PAGELEFT: //向左滚动一页 
				position-=10; 
				break; 
			case SB_PAGERIGHT: //向右滚动一页 
				position+=10; 
				break; 
			case SB_THUMBTRACK://拖动滑动块 
				position=nPos;//这里的nPos 就是函数 OnHScroll()的第 2 个参数 
				break; 
			case SB_LEFT://移动到最左边 
				position=0; 
				break; 
			case SB_RIGHT://移动到最右边 
				position=100; 
				break; 

			} 
			if(position>100) position=100; 
			if(position<0) position=0; 
			m_SvBar.SetScrollPos(position);//根据position 的值来设定滑动块的位置

			m_vpos=position*t/100;

			showImage(m_image,text);
		} 
	} 

	CDialog::OnVScroll(nSBCode, nPos, pScrollBar);
}



void CnewDialog::OnClose()
{
	
    theApp.pRight->CloseNewPicture();

	CDialog::OnClose();
}


