#pragma once
#include "resource.h"
#include "afxwin.h"
// CnewDialog 对话框

class CnewDialog : public CDialog
{
	DECLARE_DYNAMIC(CnewDialog)

public:
	CnewDialog(CWnd* pParent = NULL);   // 标准构造函数
	virtual ~CnewDialog();

// 对话框数据
	enum { IDD = IDD_NEWDIALOG };
	
	IplImage *m_image;
	CRect       m_dsprect;
	IplImage *m_dspbuf;//图片显示的区域的大小，初始设为和picture control控件一样大
	CPoint m_Cpoint;
	int m_hpos;
	int m_vpos;
	CPoint m_point;
	CString text;

//	CDialog *pRight;

//	void OnPaint(IplImage *image);
	void showImage(IplImage *&image,CString &tmp_path);
	inline void GetImageDisplayData(IplImage* dspbuf, IplImage* &imagescale, int hpos, int vpos);
	void SetPicturePoint(CPoint &point);




protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
public:
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
public:
	CScrollBar m_SvBar;
public:
	CScrollBar m_SBar;
public:
//	afx_msg void OnNMThemeChangedScrollbar2(NMHDR *pNMHDR, LRESULT *pResult);
public:
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
public:
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
public:
	afx_msg void OnPaint();
public:
	afx_msg void OnClose();
public:
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
};
